var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss', './public/css/app.css');
      //  .sass('default.theme.scss', './public/css/default.theme.css');
        
    //mix.less('semantic.less', './public/vendor/semantic/semantic.css');
    
    mix.styles([
        //'public/vendor/semantic/semantic.css',
        //'public/css/default.theme.css',
        //'resources/assets/semantic-ui-calendar/dist/calendar.min.css',
        'resources/assets/css/animate.min.css',
        'resources/assets/css/bistro-icons.css',
        'resources/assets/css/bootstrap-datetimepicker.css',
        'resources/assets/css/bootstrap.min.css',
        'resources/assets/css/font-awesome.min.css',
        'resources/assets/css/jquery.fancybox.css',
        'resources/assets/css/loader.css',
        'resources/assets/css/navigation.css',
        'resources/assets/css/owl.carousel.css',
        'resources/assets/css/owl.transitions.css',
        'resources/assets/css/settings.css',
        'resources/assets/css/style.css',
        'resources/assets/css/zerogrid.css',
        'public/css/app.css'
    ], 'public/css/all.css', './');

    mix.styles([
            'resources/assets/css/lib/button.css'
        ], 'public/css/button.css');
    
    mix.scripts([
        'resources/assets/js/jquery-2.2.3.js',
        'resources/assets/js/jquery.geocomplete.min.js',
        'resources/assets/js/bootstrap.min.js',
        'resources/assets/js/jquery.themepunch.tools.min.js',
        'resources/assets/js/jquery.themepunch.revolution.min.js',
        'resources/assets/js/revolution.extension.layeranimation.min.js',
        'resources/assets/js/revolution.extension.navigation.min.js',
        'resources/assets/js/revolution.extension.parallax.min.js',
        'resources/assets/js/revolution.extension.slideanims.min.js',
        'resources/assets/js/revolution.extension.video.min.js',
        'resources/assets/js/slider.js',
        'resources/assets/js/owl.carousel.min.js',
        'resources/assets/js/jquery.parallax-1.1.3.js',
        'resources/assets/js/jquery.mixitup.min.js',
        'resources/assets/js/jquery-countTo.js',
        'resources/assets/js/jquery.appear.js',
        'resources/assets/js/jquery.fancybox.js',
        'resources/assets/js/jquery.fancybox-media.js',
        'resources/assets/js/map.js',
        'resources/assets/js/functions.js',
        'resources/assets/js/custom.js',
    ], 'public/js/all.js', './');

    mix.scripts(['resources/assets/js/lib/core.js'], 'public/js/core.js', './');
    mix.scripts([
        'resources/assets/js/lib/tinymce.min.js'
        ], 'public/js/tinymce.min.js', './');
    
    mix.version(['public/css/all.css', 'public/js/all.js', 'public/css/button.css', 'public/js/core.js', 'public/js/tinymce.min.js']);
    
    mix.copy('resources/assets/fonts', './public/fonts')
    	.copy('resources/assets/fonts', './public/build/fonts')
        .copy('public/build-images', './public/build/images')
        .copy('resources/assets/langs', './public/build/js/langs')
        .copy('resources/assets/plugins', './public/build/js/plugins')
        .copy('resources/assets/skins', './public/build/js/skins')
        .copy('resources/assets/themes', './public/build/js/themes');

});
