$(document).ready(function(){
	$(".fileUpload").on('change', function () {
		//Get count of selected files
		var parent = $(this).parent();
		var countFiles = $(this)[0].files.length;
	
		var imgPath = $(this)[0].value;
		var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
		var image_holder = $(this).parent().find('.image-preview');
		image_holder.empty();
	
		if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
			if (typeof (FileReader) != "undefined") {
	
				//loop for each file selected for uploaded.
				for (var i = 0; i < countFiles; i++) {
	
					var reader = new FileReader();
					reader.onload = function (e) {
						$("<img />", {
							"min-width":300,
							"max-width":'100%',
							"min-height":300,
							"max-height":'auto',
							
							"src": e.target.result,
								"class": "thumb-image img-rounded img-responsive"
						}).appendTo(image_holder);
					}
	
					image_holder.show();
					reader.readAsDataURL($(this)[0].files[i]);
				}
	
			} else {
				alert("This browser does not support FileReader.");
			}
		} else {
			alert("Pls select only images");
		}
	});
	
	$('#flash-overlay-modal').modal();
	$('div.alert').not('.form-errors').delay(5000).slideUp(300);

	$("#modalStatus, #editModal, #editSliderModal, #deleteModal").on('show.bs.modal', function(event){
        var button = $(event.relatedTarget);  // Button that triggered the modal
        var titleData = button.data('title');

        if(titleData.trim() == 'EDIT' || titleData.trim() == 'EDIT SLIDER'){
        	$(this).find('.modal-footer a').attr('href', button.data('url'));
        }else{
	        $(this).find('.modal-title').text(titleData);
	        $(this).find('.modal-body span.status').text(titleData);
	        $(this).find('form button').removeClass().addClass(button.data('class')).text(titleData);
	        $(this).find('form').attr('action', button.data('url'));
        }

    });

    $(".collapse.in").each(function(){
        	$(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-minus").removeClass("glyphicon-plus");
        });
        
    // Toggle plus minus icon on show hide of collapse element
    $(".collapse").on('show.bs.collapse', function(){
    	$(this).parent().find(".glyphicon").removeClass("glyphicon-plus").addClass("glyphicon-minus");
    }).on('hide.bs.collapse', function(){
    	$(this).parent().find(".glyphicon").removeClass("glyphicon-minus").addClass("glyphicon-plus");
    });

    $(document.body).on('click','.multiple-select-button', function(e){

    	var $this = $(this),
    		input = $this.find('input');


    	if($this.hasClass('active')){
    		$this.removeClass().addClass('multiple-select-button disable-select');
    		input.attr("checked",false);
    	}else{
    		$this.removeClass().addClass('multiple-select-button disable-select active');
    		input.attr("checked",true);
    	}
    });

    

	$(document.body).on('click','.single-select-button', function(e){

	var $this = $(this);
	if($(this).attr('data-filename') == 'image'){
		$('input[type=file].filesUpload').removeAttr('name');
		$('input[type=file].fileUpload').attr('name', 'image');

	}else if($(this).attr('data-filename') == 'slider'){
		$('input[type=file].filesUpload').attr('name', 'slider[]');
		$('input[type=file].fileUpload').removeAttr('name');
	}

	$this.siblings('input[type=hidden]').val($this.find('input').val());

	allSingleSelectBtn = $this.siblings('.single-select-button');

	$.each(allSingleSelectBtn, function(index, elem){

		$(elem).removeClass().addClass('single-select-button disable-select');

	});

	$this.removeClass().addClass('single-select-button disable-select active');


  });

   $(document.body).on("change",".filesUpload", handleFileSelect);
        
	selDiv = $(".multiple-image-preview"); 
	removeImages = [];
	$("body").on("click", ".selFile", removeFile);

	$(document.body).on('click', 'button.multiple', function(){
		$('input[type=file].filesUpload').val(null);
    	$('input[type=file].filesUpload').click();
    });



    $('.image-wrapper img').on('click', function(e){
    	var $this = $(this),
    	    imgWrapper = $this.closest('.image-wrapper'),
    	    removeImageHandler = $('.slider .hidden input[name=removeImages]');

    	 if(imgWrapper.hasClass('selected')){
    	 	imgWrapper.removeClass('selected');
    	 	removeImages.splice($.inArray($this.attr('data-href'),removeImages), 1);
    	 }else{
    	 	imgWrapper.addClass('selected');
    	 	removeImages.push($this.attr('data-href'));
    	 }

    	 console.log(removeImages);

    	 removeImageHandler.val(removeImages);

    });

    $(".media-list .media-body a.reply").on('click', function(e) {
        e.preventDefault();
        var commentId = $(this).data('comment-id');
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top - 83
        }, 1000, function(e){
            $('form#reply-form input[name=parent_id]').val(commentId);
        });
    });
	
});

function handleFileSelect(e) {
        var files = e.target.files;
        var filesArr = Array.prototype.slice.call(files);
        removeImages = [];
        selDiv.empty();
        filesArr.forEach(function(f) {          

            if(!f.type.match("image.*")) {
                return;
            }
            
            var reader = new FileReader();
            reader.onload = function (e) {
                var html = "<img src=\"" + e.target.result + "\" data-file='"+f.name+"' class='selFile img-responsive img-rounded' title='Click to remove'>";
                selDiv.append(html);
                
            }
            reader.readAsDataURL(f); 
        });
        if(filesArr.length == 1){
        	$('button.multiple').text((files.length - removeImages.length)+' file selected (Choose Files)');
        }else{
        	$('button.multiple').text((files.length - removeImages.length)+' files selected (Choose Files)');
        }
        
    }

function removeFile(e) {
	var files = Array.prototype.slice.call($(".filesUpload")[0].files);
    var file = $(this).attr("data-file");
    for(var i=0;i<files.length;i++) {
        if(files[i].name === file) {
        	// files.splice(i,1);
            removeImages.push(file);
            break;
        }
    }
    $('.slider .hidden input[name=removeImages]').val(removeImages);
    $(this).remove();
    if((files.length - removeImages.length) == 1){
    	$('button.multiple').text((files.length - removeImages.length)+' file selected (Choose Files)');
    }else{
    	$('button.multiple').text((files.length - removeImages.length)+' files selected (Choose Files)');
    }
}