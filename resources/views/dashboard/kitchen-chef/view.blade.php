@extends('dashboard.layouts.main')

@section('dashboard-content')
<table  class="table table-condensed table-bordered">
	<thead>
		<tr class="warning">
			<th>Kitchen Chef</th>
			<th>Kitchen Chef Position</th>
			<th>Kitchen Chef Rank</th>
			<th>Menu Thumbnail</th>
			<th>Kitchen Chef Description</th>
			<th colspan="2" class="text-center">ACTIONS</th>
		</tr>
	</thead>
	<tbody>
		@forelse($kitchen_chefs as $kitchen_chef)
		<tr>
			<td>{{$kitchen_chef->name}}
			</td>
			<td>
				{{$kitchen_chef->kitchen_position->name}}
			</td>
			<td>
				{{$kitchen_chef->kitchen_rank->name}}
			</td>
			<td>
				<img src="{{url($kitchen_chef->thumbnail)}}" class="img-responsive img-rounded" style="max-width:150px; max-height: 120px">
			</td>
			<td>
				{{substr($kitchen_chef->excerpt, 0, 20)}}
			</td>
			<td>
				<button type="button" class="btn btn-primary" data-url="{{route('edit.kitchen.chef', ['id' => $kitchen_chef->id])}}" data-class="btn btn-primary" data-title="EDIT" data-toggle="modal" data-target="#editModal"> EDIT </button>
			</td>
			<td>
				<button type="button" class="btn btn-danger" data-url="{{route('delete.kitchen.chef', ['id' => $kitchen_chef->id])}}" data-class="btn btn-danger" data-title="DELETE" data-toggle="modal" data-target="#deleteModal"> DELETE </button>
			</td>			
		</tr>
		@empty
		<tr class="info">
			<td colspan="7">
				<span class="text-info"><i class="fa fa-fw fa-exclamation-triangle"></i> No Transactions</span>  
			</td>
		</tr>
		@endforelse
	</tbody>
	<tfoot>
		<tr class="warning">
			<td colspan="7">{{$kitchen_chefs->render()}}</td>
		</tr>
	</tfoot>
</table>
@endsection