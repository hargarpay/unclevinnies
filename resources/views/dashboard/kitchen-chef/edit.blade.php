@extends('dashboard.layouts.main')

@section('dashboard-content')
<div class="panel panel-warning margin-top">
	<div class="panel-heading"><h2>Edit Kitchen Chef</h2></div>
	<div class="panel-body">
		@include('partials.form-errors')
		<form action="" method="post" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<input type="hidden" name="_method" value="put">
			 <div class="row">
		        <div class="col-xs-12">
		    		<div class="form-group">
		    		<label style="display:block;">Kitchen Chef Position</label>
		    		<input type="hidden" name="kitchen_position" @if(!empty(old('kitchen_position')))
		    		 value="{{old('kitchen_position')}}"
		    		 @else
					value="{{$kitchen_chef->kitchen_position_id}}"
		    		 @endif>
			        		@foreach($kitchen_positions as $kitchen_position)
				        		<div class="single-select-button disable-select @if(!empty(old('kitchen_position'))) {{$kitchen_position->id == old('kitchen_position') ? 'active' : ''}} @else
								{{$kitchen_position->id == $kitchen_chef->kitchen_position_id? 'active' : ''}}
				        		 @endif">
				        			{{$kitchen_position->name}}
				        			<input type="radio" value="{{$kitchen_position->id}}">
				        		</div>
			        		@endforeach
		           </div>
		        </div>
		    </div>
		    <div class="row">
		        <div class="col-xs-12">
		    		<div class="form-group">
			        <label style="display:block;">Kitchen Chef Rank</label>
			       	<input type="hidden" name="kitchen_rank" @if(!empty(old('kitchen_rank')))
		    		 value="{{old('kitchen_rank')}}"
		    		 @else
					value="{{$kitchen_chef->kitchen_rank_id}}"
		    		 @endif>
			        		@foreach($kitchen_ranks as $kitchen_rank)
				        		<div class="single-select-button disable-select @if(!empty(old('kitchen_rank'))) {{$kitchen_rank->id == old('kitchen_rank') ? 'active' : ''}} @else
								{{$kitchen_rank->id == $kitchen_chef->kitchen_rank_id? 'active' : ''}}
				        		 @endif">
				        			{{$kitchen_rank->name}}
				        			<input type="radio" value="{{$kitchen_rank->id}}">
				        		</div>
			        		@endforeach
		           </div>
		        </div>
		    </div>
		    <div class="row">
		        <div class="col-xs-12">
			        <div class="form-group">
			        	<label for="">Kitchen Chef Fullname</label>
			            <input type="text" class="form-control" name="name" @if(!empty(old('name'))) value="{{old('name')}}" @else value="{{$kitchen_chef->name}}" @endif placeholder="Kitchen Chef Title">
			        </div>
		        </div>
		    </div>
		    <div class="row">
		    	<div class="col-xs-12">
		    	<div class="form-group">
		    		<label>Kitchen Chef Description</label>
		    		<textarea class="form-control" name="excerpt" placeholder="Kitchen Chef Description">@if(!empty(old('excerpt'))){{old('excerpt')}}@else{{$kitchen_chef->excerpt}}@endif</textarea>
		    	</div>
		    	
		    	</div>
		    </div>
		    <div class="row">
		        <div class="col-sm-6">
		        	<div class="form-group">
	                    <label for="">Thumbnail preview</label>
	                    <div class="help-block image-preview">
	                        <img src="{{$kitchen_chef->thumbnail}}" width="100%" alt="" class="img-resposive img-rounded" />
	                    </div>
	                     <input type="file" name="thumbnail" class="fileUpload">
	                 </div>
		        </div>
		    </div>
		    <div class="row">
		    	<div class="col-xs-12">
		    		<button type="submit" class="btn btn-orange btn-block" style="color: white; font-weight: bold;">Update Kitchen Chef</button>
		    	</div>
		    </div>
		</form>
	</div>
</div>
@endsection