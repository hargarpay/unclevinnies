@extends('dashboard.layouts.main')
@section('dashboard-content')
	<div class="panel panel-warning margin-top">
	<div class="panel-heading"><h2>Create Category Blog</h2></div>
	<div class="panel-body">
		@include('partials.form-errors')
		<form action="" method="post" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
	
		    <div class="row">
		        <div class="col-xs-12">
			        <div class="form-group">
			        	<label for="">Category</label>
			            <input type="text" class="form-control" name="category" value="{{old('category')}}">
			        </div>
		        </div>
		    </div>
		    <div class="row">
		        <div class="col-xs-12">
		        	<div class="form-group">
	                    <label for="">Banner Category preview</label>
	                    <div class="help-block image-preview">
	                        <img src="http://via.placeholder.com/1920x450" width="100%" alt="" class="img-resposive img-rounded" />
	                    </div>
	                     <input type="file" name="thumbnail" class="fileUpload">
	                 </div>
		        </div>
		    </div>
		    <div class="row">
		    	<div class="col-xs-12">
		    		<button type="submit" class="btn btn-orange btn-block" style="color: white; font-weight: bold;">Create Blog Category</button>
		    	</div>
		    </div>
		</form>
	</div>
</div>
@endsection