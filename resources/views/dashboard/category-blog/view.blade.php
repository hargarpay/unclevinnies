@extends('dashboard.layouts.main')

@section('dashboard-content')
<table  class="table table-condensed table-bordered">
	<thead>
		<tr class="warning">
			<th>Category Blog</th>
			<th>Category Blog Banner</th>
			<th>Blogs</th>
			<th colspan="3" class="text-center">ACTIONS</th>
		</tr>
	</thead>
	<tbody>
		@forelse($categories as $category)
		<tr>
			<td>
				{{$category->category}}
			</td>
			<td>
				<img src="{{url($category->banner)}}" class="img-responsive img-rounded" style="max-width:150px;">
			</td>
			<td>
				@forelse(getBlogByCategoryID($category->id) as $blog)
					<p class="{{$blog->published == 1 ? 'bg-warning' : 'bg-info'}}" style=""><strong>{{$blog->title}}</strong></p>
				@empty
					<p class="bg-danger"><strong><i class="fa fa-fw fa-exclamation-triangle"></i> Sorry, No Blog has been created for this category</strong></p>
				@endforelse
			</td>
			<td>
				@if($category->published == 0)
				<button type="button" class="btn btn-warning" data-url="{{route('publish.blog.category', ['id' => $category->id])}}" data-class="btn btn-warning" data-title="PUBLISH" data-toggle="modal" data-target="#modalStatus"> PUBLISH </button>
				@else
				<button type="button" class="btn" data-url="{{route('draft.blog.category', ['id' => $category->id])}}" data-class="btn" data-title="DRAFT" data-toggle="modal" data-target="#modalStatus"> DRAFT </button>
				@endif
			</td>
			<td>
				<button type="button" class="btn btn-primary" data-url="{{route('edit.blog.category', ['id' => $category->id])}}" data-class="btn btn-primary" data-title="EDIT" data-toggle="modal" data-target="#editModal"> EDIT </button>
			</td>
			<td>
				<button type="button" class="btn btn-danger" data-url="{{route('delete.blog.category', ['id' => $category->id])}}" data-class="btn btn-danger" data-title="DELETE" data-toggle="modal" data-target="#deleteModal"> DELETE </button>
			</td>			
		</tr>
		@empty
		<tr class="info">
			<td colspan="5">
				<span class="text-info"><i class="fa fa-fw fa-exclamation-triangle"></i> No Transactions</span>  
			</td>
		</tr>
		@endforelse
	</tbody>
	<tfoot>
		<tr class="warning">
			<td colspan="5">{{$categories->render()}}</td>
		</tr>
	</tfoot>
</table>
@endsection