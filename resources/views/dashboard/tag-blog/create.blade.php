@extends('dashboard.layouts.main')
@section('dashboard-content')
	<div class="panel panel-warning margin-top">
	<div class="panel-heading"><h2>Create Tag Blog</h2></div>
	<div class="panel-body">
		@include('partials.form-errors')
		<form action="" method="post" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
	
		    <div class="row">
		        <div class="col-xs-12">
			        <div class="form-group">
			        	<label for="">Tag</label>
			            <input type="text" class="form-control" name="tag" value="{{old('tag')}}">
			        </div>
		        </div>
		    </div>
		    <div class="row">
		    	<div class="col-xs-12">
		    		<button type="submit" class="btn btn-orange btn-block" style="color: white; font-weight: bold;">Create Blog Tag</button>
		    	</div>
		    </div>
		</form>
	</div>
</div>
@endsection