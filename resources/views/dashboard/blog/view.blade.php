@extends('dashboard.layouts.main')

@section('dashboard-content')
<table  class="table table-condensed table-bordered" style="max-width: 800px">
	<thead>
		<tr class="warning">
			<th width="5%">Title</th>
			<th width="45%">Description</th>
			<th width="5%">Blog Type</th>
			<th width="5%">Blog Category</th>
			<th width="5%">Blog Tags</th>
			<th width="15%">Blog Placeholder</th>
			<th colspan="4" width="20%" class="text-center">ACTIONS</th>
		</tr>
	</thead>
	<tbody>
		@forelse($blogs as $blog)
		<tr>
			<td width="5%">{{$blog->title}}
			</td>
			<td width="45%">
				{!!str_limit($blog->description, 25)!!}
			</td>
			<td width="5%">
				{{$blog->type}}
			</td>
			<td width="5%">
				@forelse($blog->categories as $category)
					<p class="{{$category->published == 1 ? 'bg-warning' : 'bg-info'}}" style=""><strong>{{$category->category}}</strong></p>
				@empty
					<p class="bg-danger"><strong><i class="fa fa-fw fa-exclamation-triangle"></i> Sorry, Blog does not have Categories</strong></p>
				@endforelse
			</td>
			<td width="5%">
				@forelse($blog->tags as $tag)
					<p class="{{$tag->published == 1 ? 'bg-warning' : 'bg-info'}}" style=""><strong>{{$tag->tag}}</strong></p>
				@empty
					<p class="bg-danger"><strong><i class="fa fa-fw fa-exclamation-triangle"></i> Sorry, Blog does not have tags</strong></p>
				@endforelse
			</td>
			<td width="15%">
				@if($blog->type != 'video_url')
				@forelse(json_decode($blog->placeholder, true) as $image_src)
				<p><img src="{{url($image_src)}}" class="img-responsive img-rounded" style="width:100%; max-height: 120px"></p>
				@empty
				 <p class="text-center text-danger"> No Image Found</p>
				@endforelse
				@else
					<p class="text-center">{{$blog->placeholder}}</p>
				@endif
			</td>
			<td width="5%">
				@if($blog->type == 'slider')
					<button type="button" class="btn btn-success" data-url="{{route('slider.edit.blog', ['id' => $blog->id])}}" data-class="btn btn-info" data-title="EDIT SLIDER" data-toggle="modal" data-target="#editSliderModal"> REMOVE IMAGE<br> SLIDER </button>
				@else
					<p class="text-center text-info">Not Slider</p>
				@endif
			</td>
			<td width="5%">
				@if($blog->published == 0)
				<button type="button" class="btn btn-warning" data-url="{{route('publish.blog', ['id' => $blog->id])}}" data-class="btn btn-warning" data-title="PUBLISH" data-toggle="modal" data-target="#modalStatus"> PUBLISH </button>
				@else
				<button type="button" class="btn" data-url="{{route('draft.blog', ['id' => $blog->id])}}" data-class="btn" data-title="DRAFT" data-toggle="modal" data-target="#modalStatus"> DRAFT </button>
				@endif
			</td>
			<td width="5%">
				<button type="button" class="btn btn-primary" data-url="{{route('edit.blog', ['id' => $blog->id])}}" data-class="btn btn-primary" data-title="EDIT" data-toggle="modal" data-target="#editModal"> EDIT </button>
			</td>
			<td width="5%">
				<button type="button" class="btn btn-danger" data-url="{{route('delete.blog', ['id' => $blog->id])}}" data-class="btn btn-danger" data-title="DELETE" data-toggle="modal" data-target="#deleteModal"> DELETE </button>
			</td>			
		</tr>
		@empty
		<tr class="info">
			<td colspan="10">
				<span class="text-info"><i class="fa fa-fw fa-exclamation-triangle"></i> No Blog has been created</span>  
			</td>
		</tr>
		@endforelse
	</tbody>
	<tfoot>
		<tr class="warning">
			<td colspan="10">{{$blogs->render()}}</td>
		</tr>
	</tfoot>
</table>
@endsection