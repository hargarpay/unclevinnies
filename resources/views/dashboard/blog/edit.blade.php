@extends('dashboard.layouts.main')

@section('dashboard-content')
<div class="panel panel-warning margin-top">
	<div class="panel-heading"><h2>Edit Blog</h2></div>
	<div class="panel-body">
		@include('partials.form-errors')
		<form action="" method="post" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<input type="hidden" name="_method" value="put">
		<input type="hidden" name="blog_id" value="{{$blog->id}}">
			 <!-- json_decode($blog->placeholder, true) -->
		    <div class="row">
		        <div class="col-xs-12">
		    		<div class="form-group">
		        		<label style="display:block;">Blog Tags</label>
		        		@forelse($tags as $tag)
			        		<div class="multiple-select-button disable-select @if(!empty(old('tags'))) {{in_array($tag->id, old('tags')) ? 'active' : ''}} @else  {{in_array($tag->id, $tag_select) ? 'active' : ''}} @endif">
			        			{{$tag->tag}}
			        			<input type="checkbox" name="tags[]" value="{{$tag->id}}" @if(!empty(old('tags'))) {{in_array($tag->id, old('tags')) ? 'checked' : ''}} @else {{in_array($tag->id, $tag_select) ? 'checked' : ''}} @endif>
			        		</div>
			        	@empty

		        		@endforelse
		            </div>
		        </div>
		    </div>
		    <div class="row">
		        <div class="col-xs-12">
		    		<div class="form-group">
		        		<label style="display:block;">Blog Category</label>
		        		@forelse($categories as $category)
			        		<div class="multiple-select-button disable-select @if(!empty(old('categories'))) {{in_array($category->id, old('categories')) ? 'active' : ''}} @else {{in_array($category->id, $category_select) ? 'active' : ''}} @endif">
			        			{{$category->category}}
			        			<input type="checkbox" name="categories[]" value="{{$category->id}}" @if(!empty(old('categories'))) {{in_array($category->id, old('categories')) ? 'checked' : ''}} @else {{in_array($category->id, $category_select) ? 'checked' : ''}} @endif>
			        		</div>
		        		@empty

		        		@endforelse
		            </div>
		        </div>
		    </div>
		    <div class="row">
		        <div class="col-xs-12">
			        <div class="form-group">
			        	<label for="">Blog Title</label>
			            <input type="text" class="form-control" name="name" @if(!empty(old('name'))) value="{{old('name')}}" @else value="{{$blog->title}}" @endif placeholder="Kitchen Chef Title">
			        </div>
		        </div>
		    </div>
		    <div class="row">
		    	<div class="col-xs-12">
		    	<div class="form-group">
		    	  <label>Description Description</label>
		    	  <textarea cols="80" rows="10" id="articleContent" name="articleContent">@if(!empty(old('articleContent'))){{e(old('articleContent'))}}@else{{$blog->description}}@endif</textarea>
			      <script type="text/javascript">
			      tinyMCE.init({
			        theme : "modern",
			        theme_advanced_toolbar_location : "top",
			        theme_advanced_toolbar_align : "left",
			        mode : "exact",
			        elements : "articleContent"
			      });
			      </script>		    	
      			</div>
		    	
		    	</div>
		    </div>
		    <div class="row  blog type">
		        <div class="col-xs-12">
		    		<div class="form-group">
		    		<label style="display:block;">Blog Type</label>
		    		<input type="hidden" name="blog_type" @if(!empty(old('blog_type')))
		    			value="{{old('blog_type')}}"
		    		@else
						value="{{$blog->type}}"
		    		@endif >
			        		@foreach($blog_types as $blog_type)
				        		<div class="single-select-button disable-select filter @if(!empty(old('blog_type'))) {{$blog_type == old('blog_type') ? 'active' : ''}}@else {{$blog_type == $blog->type ? 'active' : ''}}  @endif" data-filter=".{{$blog_type}}" data-filename="{{$blog_type}}">
				        			{{str_replace('_',' ',$blog_type)}}
				        			<input type="radio" value="{{$blog_type}}">
				        		</div>
			        		@endforeach
		           </div>
		        </div>
		    </div>
		    <div class="placeholder">
			    <div class="row mix image" style="width:100%">
			        <div class="col-xs-12">
			        	<div class="form-group">
		                    <label for="">Thumbnail preview</label>
		                    <div class="help-block image-preview">
		                    	@if('image' == trim($blog->type))
									<img src="{{url(head(json_decode($blog->placeholder, true)))}}" width="100%" alt="" class="img-resposive img-rounded" />
		                    	@else
		                        	<img src="http://via.placeholder.com/1542x784" width="100%" alt="" class="img-resposive img-rounded" />
		                    	@endif
		                    </div>
		                     <input type="file" 
		                     @if('image' == old('blog_type'))
		                      name="image" 
		                     @elseif('image' == trim($blog->type))
		                      name="image" 
		                     @endif
		                     class="fileUpload">
		                 </div>
			        </div>
			    </div>
			    <div class="row mix slider" style="width:100%">
			        <div class="col-xs-12">
			        	<div class="form-group">
		                    <label for="">Slider preview</label>
		                    <div class="help-block multiple-image-preview">
		                    @if('slider' == trim($blog->type))
		                    	 @foreach(json_decode($blog->placeholder, true) as $image_src)
									<img src="{{url($image_src)}}" width="25%" alt="" class="img-resposive img-rounded" />
							     @endforeach
	                    	@else
	                        	<img src="http://via.placeholder.com/1542x784" width="25%" alt="" class="img-resposive img-rounded" />
	                        	<img src="http://via.placeholder.com/1542x784" width="25%" alt="" class="img-resposive img-rounded" />
	                        	<img src="http://via.placeholder.com/1542x784" width="25%" alt="" class="img-resposive img-rounded" />
	                    	@endif
		                    </div>
		                     <input type="file" 
		                     @if('slider' == old('blog_type'))
		                      name="slider[]" 
		                     @elseif('slider' == trim($blog->type))
		                      name="slider[]"
		                     @endif
		                       class="filesUpload" multiple="" style="clear:both;display:none">
		                     <button type="button" class="btn btn-orange btn-block multiple" style="color: white; font-weight: bold;width:100%;clear: both;">Choose Files</button>
		                 </div>
			        </div>
			        <div class="hidden">
			        	<input type="hidden" name="removeImages">
			        </div>
			    </div>
			    <div class="row mix video_url" style="width:100%">
			        <div class="col-xs-12">
			        	<div class="form-group">
		                    <label for="">Video Url</label>
		                     <input type="url" name="video_url"
		                     @if(!empty(old('video_url')))
		                      value="{{old('video_url')}}"
		                     @elseif('video_url' == trim($blog->type))
							  value="{{trim($blog->placeholder)}}"
		                     @endif 
		                     class="form-control" placeholder="https://">
		                 </div>
			        </div>
			    </div>
		    <div>
		    <div class="row">
		    	<div class="col-xs-12">
		    		<button type="submit" class="btn btn-orange btn-block" style="color: white; font-weight: bold;">Update Blog</button>
		    	</div>
		    </div>
		</form>
	</div>
</div>
@endsection