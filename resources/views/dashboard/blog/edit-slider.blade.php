@extends('dashboard.layouts.main')

@section('dashboard-content')
	<div class="panel panel-warning margin-top">
	<div class="panel-heading"><h2>Remove Image Slider</h2></div>
	<div class="panel-body">
		@include('partials.form-errors')
		<form action="" method="post" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<input type="hidden" name="_method" value="put">
		<input type="hidden" name="blog_id" value="{{$blog->id}}">
		    <div class="row slider" style="width:100%">
		        <div class="col-xs-12">
		        	<div class="form-group">
	                    <label for="">Slider preview</label>
	                    <div class="help-block multiple-image-preview">
	                    @if('slider' == trim($blog->type))
	                    	 @foreach(json_decode($blog->placeholder, true) as $image_src)
								<div class="image-wrapper" style="display: inline;"><img src="{{url($image_src)}}" width="25%" alt="" data-href="{{$image_src}}" class="img-resposive img-rounded" /></div>
						     @endforeach
                    	@endif
	                    </div>
	                 </div>
		        </div>
		        <div class="hidden">
		        	<input type="hidden" name="removeImages">
		        </div>
		    </div>
		    <div class="row">
		    	<div class="col-xs-12">
		    		<button type="submit" class="btn btn-orange btn-block" style="color: white; font-weight: bold;">Remove Image Slider</button>
		    	</div>
		    </div>
		</form>
	</div>
</div>
@endsection