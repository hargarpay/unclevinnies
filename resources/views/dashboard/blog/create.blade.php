@extends('dashboard.layouts.main')

@section('dashboard-content')
<div class="panel panel-warning margin-top">
	<div class="panel-heading"><h2>Create Blog</h2></div>
	<div class="panel-body">
		@include('partials.form-errors')
		<form action="" method="post" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
			 
		    <div class="row">
		        <div class="col-xs-12">
		    		<div class="form-group">
		        		<label style="display:block;">Blog Tags</label>
		        		@forelse($tags as $tag)
			        		<div class="multiple-select-button disable-select @if(!empty(old('tags'))) {{in_array($tag->id, old('tags')) ? 'active' : ''}} @endif">
			        			{{$tag->tag}}
			        			<input type="checkbox" name="tags[]" value="{{$tag->id}}" @if(!empty(old('tags'))) {{in_array($tag->id, old('tags')) ? 'checked' : ''}} @endif>
			        		</div>
			        	@empty

		        		@endforelse
		            </div>
		        </div>
		    </div>
		    <div class="row">
		        <div class="col-xs-12">
		    		<div class="form-group">
		        		<label style="display:block;">Blog Category</label>
		        		@forelse($categories as $category)
			        		<div class="multiple-select-button disable-select @if(!empty(old('categories'))) {{in_array($category->id, old('categories')) ? 'active' : ''}} @endif">
			        			{{$category->category}}
			        			<input type="checkbox" name="categories[]" value="{{$category->id}}" @if(!empty(old('categories'))) {{in_array($category->id, old('categories')) ? 'checked' : ''}} @endif>
			        		</div>
		        		@empty

		        		@endforelse
		            </div>
		        </div>
		    </div>
		    <div class="row">
		        <div class="col-xs-12">
			        <div class="form-group">
			        	<label for="">Blog Title</label>
			            <input type="text" class="form-control" name="name" value="{{old('name')}}" placeholder="Kitchen Chef Title">
			        </div>
		        </div>
		    </div>
		    <div class="row">
		    	<div class="col-xs-12">
		    	<div class="form-group">
		    	  <label>Description Description</label>
		    	  <textarea cols="80" rows="10" id="articleContent" name="articleContent">
		    	  {{e(old('articleContent'))}}
			      </textarea>
			      <script type="text/javascript">
			      tinyMCE.init({
			        theme : "modern",
			        theme_advanced_toolbar_location : "top",
			        theme_advanced_toolbar_align : "left",
			        mode : "exact",
			        elements : "articleContent"
			      });
			      </script>		    	
      			</div>
		    	
		    	</div>
		    </div>
		    <div class="row  blog type">
		        <div class="col-xs-12">
		    		<div class="form-group">
		    		<label style="display:block;">Blog Type</label>
		    		<input type="hidden" name="blog_type" @if(!empty(old('blog_type')))
		    		value="{{old('blog_type')}}"
		    		@else
					value="image"
		    		@endif >
			        		@foreach($blog_types as $blog_type)
				        		<div class="single-select-button disable-select filter @if(!empty(old('blog_type'))) {{$blog_type == old('blog_type') ? 'active' : ''}}@else {{$blog_type == 'image'  ? 'active' : ''}}  @endif" data-filter=".{{$blog_type}}" data-filename="{{$blog_type}}">
				        			{{str_replace('_',' ',$blog_type)}}
				        			<input type="radio" value="{{$blog_type}}">
				        		</div>
			        		@endforeach
		           </div>
		        </div>
		    </div>
		    <div class="placeholder">
			    <div class="row mix image" style="width:100%">
			        <div class="col-xs-12">
			        	<div class="form-group">
		                    <label for="">Thumbnail preview</label>
		                    <div class="help-block image-preview">
		                        <img src="http://via.placeholder.com/1542x784" width="100%" alt="" class="img-resposive img-rounded" />
		                    </div>
		                     <input type="file" @if('image' == old('blog_type')) name="image" @endif @if(empty(old('blog_type'))) name="image" @endif class="fileUpload">
		                 </div>
			        </div>
			    </div>
			    <div class="row mix slider" style="width:100%">
			        <div class="col-xs-12">
			        	<div class="form-group">
		                    <label for="">Slider preview</label>
		                    <div class="help-block multiple-image-preview">
		                        <img src="http://via.placeholder.com/1542x784" width="25%" alt="" class="img-resposive img-rounded" />
		                        <img src="http://via.placeholder.com/1542x784" width="25%" alt="" class="img-resposive img-rounded" />
		                        <img src="http://via.placeholder.com/1542x784" width="25%" alt="" class="img-resposive img-rounded" />
		                    </div>
		                     <input type="file" @if('slider' == old('blog_type')) name="slider[]" @endif  class="filesUpload" multiple="" style="clear:both;display:none">
		                     <button type="button" class="btn btn-orange btn-block multiple" style="color: white; font-weight: bold;width:100%;clear: both;">Choose Files</button>
		                 </div>
			        </div>
			        <div class="hidden">
			        	<input type="hidden" name="removeImages">
			        </div>
			    </div>
			    <div class="row mix video_url" style="width:100%">
			        <div class="col-xs-12">
			        	<div class="form-group">
		                    <label for="">Video Url</label>
		                     <input type="url" name="video_url" value="{{old('video_url')}}" class="form-control" placeholder="https://">
		                 </div>
			        </div>
			    </div>
		    <div>
		    <div class="row">
		    	<div class="col-xs-12">
		    		<button type="submit" class="btn btn-orange btn-block" style="color: white; font-weight: bold;">Create Blog</button>
		    	</div>
		    </div>
		</form>
	</div>
</div>
@endsection