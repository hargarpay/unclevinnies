@extends('dashboard.layouts.main')

@section('dashboard-content')
<table  class="table table-condensed table-bordered">
	<thead>
		<tr class="warning">
			<th>Menu Title</th>
			<th>Menu Description</th>
			<th>Menu Thumbnail</th>
			<th>Menu Category</th>
			<th>Menu Amount</th>
			<th colspan="4" class="text-center">ACTIONS</th>
		</tr>
	</thead>
	<tbody>
		@forelse($menus as $menu)
		<tr>
			<td>{{$menu->title}}
			</td>
			<td>
				{{substr($menu->description, 0, 30)}}
			</td>
			<td>
				<img src="{{url($menu->thumbnail)}}" class="img-responsive img-rounded" style="max-width:150px; max-height: 120px">
			</td>
			<td>
				{{$menu->category->name}}
			</td>
			<td>
				{{$menu->amount}}
			</td>
			<td>
				@if($menu->published == 0)
				<button type="button" class="btn btn-warning" data-url="{{route('publish.menu', ['id' => $menu->id])}}" data-class="btn btn-warning" data-title="PUBLISH" data-toggle="modal" data-target="#modalStatus"> PUBLISH </button>
				@else
				<button type="button" class="btn" data-url="{{route('draft.menu', ['id' => $menu->id])}}" data-class="btn" data-title="DRAFT" data-toggle="modal" data-target="#modalStatus"> DRAFT </button>
				@endif
			</td>
			<td>
				@if($menu->featured == 0)
				<button type="button" class="btn btn-info" data-url="{{route('feature.menu', ['id' => $menu->id])}}" data-class="btn btn-info" data-title="FEATURE" data-toggle="modal" data-target="#modalStatus"> FEATURE </button>
				@else
				<button type="button" class="btn" data-url="{{route('unfeature.menu', ['id' => $menu->id])}}" data-class="btn" data-title="UNFEATURE" data-toggle="modal" data-target="#modalStatus"> UNFEATURE </button>
				@endif
			</td>
			<td>
				<button type="button" class="btn btn-primary" data-url="{{route('edit.menu', ['id' => $menu->id])}}" data-class="btn btn-primary" data-title="EDIT" data-toggle="modal" data-target="#editModal"> EDIT </button>
			</td>
			<td>
				<button type="button" class="btn btn-danger" data-url="{{route('delete.menu', ['id' => $menu->id])}}" data-class="btn btn-danger" data-title="DELETE" data-toggle="modal" data-target="#deleteModal"> DELETE </button>
			</td>			
		</tr>
		@empty
		<tr class="info">
			<td colspan="9">
				<span class="text-info"><i class="fa fa-fw fa-exclamation-triangle"></i> No Transactions</span>  
			</td>
		</tr>
		@endforelse
	</tbody>
	<tfoot>
		<tr class="warning">
			<td colspan="9">{{$menus->render()}}</td>
		</tr>
	</tfoot>
</table>
@endsection