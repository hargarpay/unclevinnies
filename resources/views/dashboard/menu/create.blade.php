@extends('dashboard.layouts.main')

@section('dashboard-content')
<div class="panel panel-warning margin-top">
	<div class="panel-heading"><h2>Create Menu</h2></div>
	<div class="panel-body">
		@include('partials.form-errors')
		<form action="" method="post" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
			 <div class="row">
		        <div class="col-xs-12">
		    		<div class="form-group">
		    		<label>Menu Category</label>
			            <select class="form-control" name="category">
			            <option> Select Category</option>
			                @foreach($categories as $category)
							<option value="{{$category->id}}" {{old('category') == $category->id ? 'selected="selected"' : ''}}>{{$category->name}}</option>
			               @endforeach
			            </select>
		           </div>
		        </div>
		    </div>
		    <div class="row">
		        <div class="col-xs-12">
			        <div class="form-group">
			        	<label for="">Menu Title</label>
			            <input type="text" class="form-control" name="title" value="{{old('title')}}" placeholder="Menu Title">
			        </div>
		        </div>
		    </div>
		    <div class="row">
		    	<div class="col-xs-12">
		    	<div class="form-group">
		    		<label>Menu Description</label>
		    		<textarea class="form-control" name="description" placeholder="Menu Description">{{old('description')}}</textarea>
		    	</div>
		    	
		    	</div>
		    </div>
		     <div class="row">
		        <div class="col-xs-12">
			        <div class="form-group">
			        	<label for="">Menu Amount </label>
			            <input type="text" class="form-control" name="amount" value="{{old('amount')}}" placeholder="1000">
			            <em class="help-block bg-danger text-center">Amount in Naira, DO NOT use comma</em>
			        </div>
		        </div>
		    </div>
		    <div class="row">
		        <div class="col-sm-6">
		        	<div class="form-group">
	                    <label for="">Thumbnail preview</label>
	                    <div class="help-block image-preview">
	                        <img src="http://via.placeholder.com/650x566" width="100%" alt="" class="img-resposive img-rounded" />
	                    </div>
	                     <input type="file" name="thumbnail" class="fileUpload">
	                 </div>
		        </div>
		    </div>
		    <div class="row">
		    	<div class="col-xs-12">
		    		<button type="submit" class="btn btn-orange btn-block" style="color: white; font-weight: bold;">Create Menu</button>
		    	</div>
		    </div>
		</form>
	</div>
</div>
@endsection