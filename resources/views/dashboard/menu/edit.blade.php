@extends('dashboard.layouts.main')

@section('dashboard-content')
<div class="panel panel-warning margin-top">
	<div class="panel-heading"><h2>Edit Menu</h2></div>
	<div class="panel-body">
		@include('partials.form-errors')
		<form action="" method="post" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<input type="hidden" name="_method" value="put">
			 <div class="row">
		        <div class="col-xs-12">
		    		<div class="form-group">
		    		<label>Menu Category</label>
			            <select class="form-control" name="category">
			            <option> Select Category</option>
			                @foreach($categories as $category)
							<option value="{{$category->id}}" 
							@if(!empty(old('category')))
								{{old('category') == $category->id ? 'selected="selected"' : ''}}
							@else
								{{$menu->category_id == $category->id ? 'selected="selected"' : ''}}
							@endif>{{$category->name}}</option>
			               @endforeach
			            </select>
		           </div>
		        </div>
		    </div>
		    <div class="row">
		        <div class="col-xs-12">
			        <div class="form-group">
			        	<label for="">Menu Title</label>
			            <input type="text" class="form-control" name="title" @if(!empty(old('title'))) value="{{old('title')}}" @else value="{{$menu->title}}" @endif placeholder="Menu Title">
			        </div>
		        </div>
		    </div>
		    <div class="row">
		    	<div class="col-xs-12">
		    	<div class="form-group">
		    		<label>Menu Description</label>
		    		<textarea class="form-control" name="description" placeholder="Menu Description">@if(!empty(old('description'))){{old('description')}}@else{{$menu->description}}@endif</textarea>
		    	</div>
		    	
		    	</div>
		    </div>
		     <div class="row">
		        <div class="col-xs-12">
			        <div class="form-group">
			        	<label for="">Menu Amount </label>
			            <input type="text" class="form-control" name="amount" @if(!empty(old('amount'))) value="{{old('amount')}}" @else value="{{$menu->amount}}" @endif placeholder="1000">
			            <em class="help-block bg-danger text-center">Amount in Naira, DO NOT use comma</em>
			        </div>
		        </div>
		    </div>
		    <div class="row">
		        <div class="col-sm-6">
		        	<div class="form-group">
	                    <label for="">Thumbnail preview</label>
	                    <div class="help-block image-preview">
	                        <img src="{{$menu->thumbnail}}" width="100%" alt="" class="img-resposive img-rounded" />
	                    </div>
	                     <input type="file" name="thumbnail" class="fileUpload">
	                 </div>
		        </div>
		    </div>
		    <div class="row">
		    	<div class="col-xs-12">
		    		<button type="submit" class="btn btn-orange btn-block" style="color: white; font-weight: bold;">Update Menu</button>
		    	</div>
		    </div>
		</form>
	</div>
</div>
@endsection