@extends('dashboard.layouts.main')

@section('dashboard-content')
<div class="panel panel-warning margin-top">
	<div class="panel-heading"><h2>Edit Kitchen Position</h2></div>
	<div class="panel-body">
		@include('partials.form-errors')
		<form action="" method="post" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<input type="hidden" name="_method" value="put">
		    <div class="row">
		        <div class="col-xs-12">
			        <div class="form-group">
			        	<label for="">Kitchen Position</label>
			            <input type="text" class="form-control" name="name" @if(!empty(old('name'))) value="{{old('name')}}" @else value="{{$kitchen_position->name}}" @endif placeholder="Kitchen Position">
			        </div>
		        </div>
		    </div>
		    <div class="row">
		    	<div class="col-xs-12">
		    		<button type="submit" class="btn btn-orange btn-block" style="color: white; font-weight: bold;">Update Kitchen Position</button>
		    	</div>
		    </div>
		</form>
	</div>
</div>
@endsection