@extends('dashboard.layouts.main')

@section('dashboard-content')
<table  class="table table-condensed table-bordered">
	<thead>
		<tr class="warning">
			<th>Kitchen Rank</th>
			<th colspan="2" class="text-center">ACTIONS</th>
		</tr>
	</thead>
	<tbody>
		@forelse($kitchen_ranks as $kitchen_rank)
		<tr>
			<td>{{$kitchen_rank->name}}
			</td>
			<td>
				<button type="button" class="btn btn-primary" data-url="{{route('edit.kitchen.rank', ['id' => $kitchen_rank->id])}}" data-class="btn btn-primary" data-title="EDIT" data-toggle="modal" data-target="#editModal"> EDIT </button>
			</td>
			<td>
				<button type="button" class="btn btn-danger" data-url="{{route('delete.kitchen.rank', ['id' => $kitchen_rank->id])}}" data-class="btn btn-danger" data-title="DELETE" data-toggle="modal" data-target="#deleteModal"> DELETE </button>
			</td>			
		</tr>
		@empty
		<tr class="info">
			<td colspan="3">
				<span class="text-info"><i class="fa fa-fw fa-exclamation-triangle"></i> No Transactions</span>  
			</td>
		</tr>
		@endforelse
	</tbody>
	<tfoot>
		<tr class="warning">
			<td colspan="9">{{$kitchen_ranks->render()}}</td>
		</tr>
	</tfoot>
</table>
@endsection