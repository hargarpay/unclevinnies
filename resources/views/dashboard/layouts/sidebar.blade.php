<!-- <div class="list-group">
    <a href="{{route('view.slider')}}" class="list-group-item active">
        <span class="fa fa-sliders"></span> Sliders <span class="badge">{{$slider_share->count()}}</span>
    </a>
    <a href="#" class="list-group-item">
        <span class="glyphicon glyphicon-file"></span> Blogs Post<span class="badge">145</span>
    </a>
    <a href="#" class="list-group-item">
        <span class="glyphicon glyphicon-music"></span> Menu Category <span class="badge">50</span>
    </a>
    <a href="#" class="list-group-item">
        <span class="glyphicon glyphicon-film"></span> Menu <span class="badge">8</span>
    </a>
</div> -->
<div class="panel-group" id="accordion">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-plus"></span> Sliders </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse {{ Request::is('dashboard/slider/*') == true ? 'in' : ''}}">
                <div class="panel-body">
                    <div class="list-group">
                        <a href="{{route('view.slider')}}" class="list-group-item {{ Request::is(str_replace(env('URL_SLASH'), '',route('view.slider'))) == true ? 'active' : ''}}">
                            <span class="fa fa-sliders"></span> View Sliders <span class="badge">{{$slider_share->count()}}</span>
                        </a>
                        <a href="{{route('create.slider')}}" class="list-group-item {{ Request::is(str_replace(env('URL_SLASH'), '',route('create.slider'))) == true ? 'active' : ''}}">
                            <span class="fa fa-sliders"></span> Create Slider
                        </a>
                    </div> 
                </div>
            </div>
        </div>
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="glyphicon glyphicon-plus"></span> Menu Category</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse {{ Request::is('dashboard/category/*') == true ? 'in' : ''}}">
                <div class="panel-body">
                    <div class="list-group">
                        <a href="{{route('view.category')}}" class="list-group-item {{ Request::is(str_replace(env('URL_SLASH'), '',route('view.category'))) == true ? 'active' : ''}}">
                            <span class="fa fa-object-group"></span> View Category <span class="badge">{{$category_share->count()}}</span>
                        </a>
                        <a href="{{route('create.category')}}" class="list-group-item {{ Request::is(str_replace(env('URL_SLASH'), '',route('create.category'))) == true ? 'active' : ''}}">
                            <span class="fa fa-object-group"></span> Create Category
                        </a>
                    </div> 
                </div>
            </div>
        </div>
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span class="glyphicon glyphicon-plus"></span> Menu</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse {{ Request::is('dashboard/menu/*') == true ? 'in' : ''}}">
                <div class="panel-body">
                    <div class="list-group">
                        <a href="{{route('view.menu')}}" class="list-group-item {{ Request::is(str_replace(env('URL_SLASH'), '',route('view.menu'))) == true ? 'active' : ''}}">
                            <span class="fa fa-object-group"></span> View Menu <span class="badge">{{$menu_share->count()}}</span>
                        </a>
                        <a href="{{route('create.menu')}}" class="list-group-item {{ Request::is(str_replace(env('URL_SLASH'), '',route('create.menu'))) == true ? 'active' : ''}}">
                            <span class="fa fa-object-group"></span> Create Menu
                        </a>
                    </div> 
                </div>
            </div>
        </div>
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="glyphicon glyphicon-plus"></span> Kitchen Position</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse {{ Request::is('dashboard/kitchen/position/*') == true ? 'in' : ''}}">
                <div class="panel-body">
                    <div class="list-group">
                        <a href="{{route('view.kitchen.position')}}" class="list-group-item {{ Request::is(str_replace(env('URL_SLASH'), '',route('view.kitchen.position'))) == true ? 'active' : ''}}">
                            <span class="fa fa-object-group"></span> View Kitchen Position <span class="badge">{{$kitchen_position_share->count()}}</span>
                        </a>
                        <a href="{{route('create.kitchen.position')}}" class="list-group-item {{ Request::is(str_replace(env('URL_SLASH'), '',route('create.kitchen.position'))) == true ? 'active' : ''}}">
                            <span class="fa fa-object-group"></span> Create Kitchen Position
                        </a>
                    </div> 
                </div>
            </div>
        </div>
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive"><span class="glyphicon glyphicon-plus"></span> Kitchen Rank</a>
                </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse {{ Request::is('dashboard/kitchen/rank/*') == true ? 'in' : ''}}">
                <div class="panel-body">
                    <div class="list-group">
                        <a href="{{route('view.kitchen.rank')}}" class="list-group-item {{ Request::is(str_replace(env('URL_SLASH'), '',route('view.kitchen.rank'))) == true ? 'active' : ''}}">
                            <span class="fa fa-object-group"></span> View Kitchen Rank <span class="badge">{{$kitchen_rank_share->count()}}</span>
                        </a>
                        <a href="{{route('create.kitchen.rank')}}" class="list-group-item {{ Request::is(str_replace(env('URL_SLASH'), '',route('create.kitchen.rank'))) == true ? 'active' : ''}}">
                            <span class="fa fa-object-group"></span> Create Kitchen Rank
                        </a>
                    </div> 
                </div>
            </div>
        </div>
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix"><span class="glyphicon glyphicon-plus"></span> Kitchen Chef</a>
                </h4>
            </div>
            <div id="collapseSix" class="panel-collapse collapse {{ Request::is('dashboard/kitchen/chef/*') == true ? 'in' : ''}}">
                <div class="panel-body">
                    <div class="list-group">
                        <a href="{{route('view.kitchen.chef')}}" class="list-group-item {{ Request::is(str_replace(env('URL_SLASH'), '',route('view.kitchen.chef'))) == true ? 'active' : ''}}">
                            <span class="fa fa-object-group"></span> View Kitchen Chef <span class="badge">{{$kitchen_chef_share->count()}}</span>
                        </a>
                        <a href="{{route('create.kitchen.chef')}}" class="list-group-item {{ Request::is(str_replace(env('URL_SLASH'), '',route('create.kitchen.chef'))) == true ? 'active' : ''}}">
                            <span class="fa fa-object-group"></span> Create Kitchen Chef
                        </a>
                    </div> 
                </div>
            </div>
        </div>
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven"><span class="glyphicon glyphicon-plus"></span>Blog Category</a>
                </h4>
            </div>
            <div id="collapseSeven" class="panel-collapse collapse {{ Request::is('dashboard/blog-category/*') == true ? 'in' : ''}}">
                <div class="panel-body">
                    <div class="list-group">
                        <a href="{{route('view.blog.category')}}" class="list-group-item {{ Request::is(str_replace(env('URL_SLASH'), '',route('view.blog.category'))) == true ? 'active' : ''}}">
                            <span class="fa fa-object-group"></span> View Blog Categories <span class="badge">{{$blog_category_share->count()}}</span>
                        </a>
                        <a href="{{route('create.blog.category')}}" class="list-group-item {{ Request::is(str_replace(env('URL_SLASH'), '',route('create.blog.category'))) == true ? 'active' : ''}}">
                            <span class="fa fa-object-group"></span> Create Blog Category
                        </a>
                    </div> 
                </div>
            </div>
        </div>
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#callapseNine"><span class="glyphicon glyphicon-plus"></span> Blog Tag</a>
                </h4>
            </div>
            <div id="callapseNine" class="panel-collapse collapse {{ Request::is('dashboard/blog-tag/*') == true ? 'in' : ''}}">
                <div class="panel-body">
                    <div class="list-group">
                        <a href="{{route('view.blog.tag')}}" class="list-group-item {{ Request::is(str_replace(env('URL_SLASH'), '',route('view.blog.tag'))) == true ? 'active' : ''}}">
                            <span class="fa fa-tag"></span> View Blog Tags <span class="badge">{{$blog_tag_share->count()}}</span>
                        </a>
                        <a href="{{route('create.blog.tag')}}" class="list-group-item {{ Request::is(str_replace(env('URL_SLASH'), '',route('create.blog.tag'))) == true ? 'active' : ''}}">
                            <span class="fa fa-tag"></span> Create Blog Tag
                        </a>
                    </div> 
                </div>
            </div>
        </div>
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#callapseEight"><span class="glyphicon glyphicon-plus"></span> Blog</a>
                </h4>
            </div>
            <div id="callapseEight" class="panel-collapse collapse {{ Request::is('dashboard/blog/*') == true ? 'in' : ''}}">
                <div class="panel-body">
                    <div class="list-group">
                        <a href="{{route('view.blog')}}" class="list-group-item {{ Request::is(str_replace(env('URL_SLASH'), '',route('view.blog'))) == true ? 'active' : ''}}">
                            <span class="fa fa-newspaper-o"></span> View Blogs<span class="badge">{{$blog_share->count()}}</span>
                        </a>
                        <a href="{{route('create.blog')}}" class="list-group-item {{ Request::is(str_replace(env('URL_SLASH'), '',route('create.blog'))) == true ? 'active' : ''}}">
                            <span class="fa fa-newspaper-o"></span> Create Blog
                        </a>
                    </div> 
                </div>
            </div>
        </div>
    </div>