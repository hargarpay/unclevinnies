@extends('layouts.main')

@section('content')
<section style="max-width: 1024px; margin: 155px auto 30px 20px;">
	<div class="container">
		<div class="row">
			<div class="col-sm-3 dashboard">
				@include('dashboard.layouts.sidebar')
			</div>
			<div class="col-sm-8">
			@yield('dashboard-content')
			</div>
		</div>
	</div>

	  <!-- Draft and Publish Modal Begins -->
    <div id="modalStatus" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Confirmation</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to <span class="status">publish</span></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal">Close</button>
                    <form style="display: inline-block;" action="" method="post">
                    	<input type="hidden" name="_token" value="{{csrf_token()}}">
                    	<input type="hidden" name="_method" value="put">
	                    <button type="submit" class="btn btn-primary">Save changes</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Draft and Publish Modal End -->

    <!--Edit Modal Begins -->
    <div id="editModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">EDIT</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to EDIT?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal">Close</button>
	                    <a href="" class="btn btn-primary"> EDIT </a>
                </div>
            </div>
        </div>
    </div>
    <!-- Edit Modal End -->

    <!--Edit Modal Begins -->
    <div id="editSliderModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">REMOVE IMAGE SLIDER?</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to REMOVE IMAGE SLIDER?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal">Close</button>
                        <a href="" class="btn btn-success"> REMOVE IMAGE SLIDER</a>
                </div>
            </div>
        </div>
    </div>
    <!-- Edit Modal End -->

    <!--DELETE Modal Begins -->
    <div id="deleteModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">DELETE</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to DELETE?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal">Close</button>
	                    <form style="display: inline-block;" action="" method="post">
                    	<input type="hidden" name="_token" value="{{csrf_token()}}">
                    	<input type="hidden" name="_method" value="delete">
	                    <button type="submit" class="btn btn-denger">Save changes</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- DELETE Modal End -->
	
</section>
@endsection