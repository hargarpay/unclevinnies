@extends('layouts.main')
@section('banner')
<!--Page header & Title-->
<section id="page_header" style="background: url('{{url('/build/images/banner-bg-8.jpg')}}') no-repeat">
<div class="page_title">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
         <h2 class="title">Blogs</h2>
         <!-- <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit</p> -->
      </div>
    </div>
  </div>
</div>  
</section>
@endsection
@section('content')
<!-- Blogs -->
<section id="blog" class="padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-sm-7">
      @forelse($blogs as $blog)
        <div class="blog_item padding-bottom">
          <h2>{{$blog->title}}</h2>
          <ul class="comments">
             <li><a href="#.">{{\Carbon\Carbon::parse($blog->created_at)->format('jS F, Y')}}</a></li>
             <!-- <li><a href="#."><i class="icon-chat-2"></i>5</a></li> -->
          </ul>
          <div class="image_container">
          @if($blog->type == 'image')
            @foreach(json_decode($blog->placeholder, true) as $image_src)
              <img src="{{url($image_src)}}" class="img-responsive" alt="blog post">
            @endforeach
          @elseif($blog->type == 'slider')
             <div id="blog-slider" class="owl-carousel">
            @foreach(json_decode($blog->placeholder, true) as $image_src)
              <div class="item"><img src="{{url($image_src)}}" class="img-responsive" alt="blog post"></div>
            @endforeach
             </div>
          @else
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="{{$blog->placeholder}}"></iframe>
            </div>
          @endif
          </div>
          <p>{{str_limit(ltrim(substr( $blog->description, 0, ( $pos = strpos( $blog->description, '&lt;/p&gt;' ) ) === false ? 0 : $pos ), '&lt;p&gt;'), 100)}}</p>
          <a class="btn-common button3" href="{{route('view.single.blog', ['id' => $blog->id, 'slug' => str_slug($blog->title)])}}">Read more</a>
        </div>
    @empty
    @endforelse
        {{$blogs->render()}}
      </div>
      <div class="col-md-4 col-sm-5">
        @include('blog-right-sidebar')
    </div>
    </div>
  </div>
</section>
@endsection