@extends('layouts.main')
@section('banner')
<section id="page_header" style="background: url('{{url('/build/images/banner-bg-1.jpg')}}') no-repeat">
<div class="page_title">
  <div class="container">
    <div class="row">
      <div class="col-md-12" style="text-align:left">
         <h2 class="title">About Us</h2>
         <p>An outdoor restaurant that provides a healthy environment for relaxation and family bonding.</p>
      </div>
    </div>
  </div>
</div>  
</section>
@endsection
@section('content')
<section id="overview" class="padding-top">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
         <h2 class="heading">Restaurant &nbsp; Overview</h2>
         <hr class="heading_space">
         <p>Uncle Vinnie's... is an outdoor restaurant that provides a healthy environment for relaxation and family bonding. We provide unique, healthy palatable continental dishes with fun and games. There are several outlets around Nigeria: Ndubuisi Kanu Park, VGC park, Lekki, amongst others places in Nigeria. We also offer delivery services that allows you access the palatable dishes at the comfort of your home.</p>
      </div>
    </div>
  </div>
</section>




<section id="gallery" class="padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="work-filter">
          <ul class="text-center">
             <li><a href="javascript:;" data-filter="all" class="active filter">All</a></li>
             @forelse($kitchen_positions as $kitchen_position)
             <li><a href="javascript:;" data-filter=".{{str_slug($kitchen_position->name)}}" class="filter">{{$kitchen_position->name}}</a></li>
             @empty

             @endforelse
          </ul>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="row">
      <div class="zerogrid">
        <div class="wrap-container">
          <div class="wrap-content clearfix">
          @forelse($kitchen_chefs as $kitchen_chef)
            <div class="col-1-4 mix work-item {{str_slug($kitchen_chef->kitchen_position->name)}} heading_space">
              <div class="wrap-col">
                <div class="item-container">
                  <div class="image">
                    <img src="{{$kitchen_chef->thumbnail}}" alt="cook"/>
                    <div class="overlay">
                      <div class="overlay-inner">
                        <ul class="social_icon">
                        <li><a href="#" class="facebook"><i class="icon-facebook5"></i></a></li>
                        <li><a href="#" class="twitter"><i class="icon-twitter4"></i></a></li>
                        <li><a href="#" class="google"><i class="icon-google"></i></a></li>
                        </ul>
                      </div>
                    </div>
                  </div> 
                  <div class="gallery_content text-left">
                    <h3>{{$kitchen_chef->name}}</h3>
                    <small>{{$kitchen_chef->kitchen_rank->name}}</small>
                    <p>{{$kitchen_chef->excerpt}}</p>
                  </div>
                </div>
              </div>
            </div>
            @empty

            @endforelse
            </div>
          </div>
        </div>
       </div>
      </div>
    </div>
  </div>
</section>


@endsection