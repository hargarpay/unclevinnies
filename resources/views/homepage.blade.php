@extends('layouts.main')
@section('banner')
<!-- REVOLUTION SLIDER -->			

				<div id="rev_slider_34_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="news-gallery34" style="margin:0px auto;background-color:#ffffff;padding:0px;margin-top:0px;margin-bottom:0px;">
				<!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
					<div id="rev_slider_34_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
						<ul>	<!-- SLIDE  -->
            @forelse($slider_publish_shared as $slider)
							<li class="text-{{$slider->position}}" data-index="rs-{{$monitor++}}" data-transition="fade" data-slotamount="default" data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7"  data-title="{{$slider->tab_title}}" data-description="{{$slider->tab_description}}">
								<!-- MAIN IMAGE -->
								<img src="{{url($slider->banner)}}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
								<!-- LAYER NR. 2 -->
								<h1 class="tp-caption tp-resizeme" 
                          data-x="{{$slider->position}}" data-hoffset="15"
                          data-y="70" 
                          data-transform_idle="o:1;"
                          data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                          data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                          data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                          data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                          data-start="500" 
                          data-splitin="none" 
                          data-splitout="none" 
                          style="z-index: 6;">
                          <span class="small_title">{{$slider->small_title}}</span> <br> {{$slider->major_title}}<span class="color">{{$slider->color_title}}</span>
                       </h1>
								<!-- LAYER NR. 2 -->
                        <p class="tp-caption tp-resizeme"
                          data-x="{{$slider->position}}" data-hoffset="15"
                          data-y="210" 
                          data-transform_idle="o:1;"
                          data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                          data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                          data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                          data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                          data-start="800"
                          style="z-index: 9;">{!!nl2br($slider->description, true)!!}
                          
                        </p>
                        <div class="tp-caption fade tp-resizeme"
                           data-x="{{$slider->position}}" data-hoffset="15"
                           data-y="280"
                           data-width = "full"  
                           data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                           data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;"  
                           data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                           data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                          data-start="1200"
                           style="z-index: 12;">

                      @if($slider->actions != "null")
                        @forelse(json_decode($slider->actions, true) as $action)
                        @if($action == 'order-form')
                         <a href="#order-form" class="btn-common btn-white page-scroll">Order Now</a>
                         @else
                            <a href="#{{$action}}" class="btn-common btn-white page-scroll">Learn More</a>
                         @endif
                         @empty

                         @endforelse
                      @endif
                       </div>
                        
                       
							</li>
              @empty

              @endforelse
							<!-- SLIDE  -->
						</ul>
					</div>
				</div>
 <!-- END REVOLUTION SLIDER -->
@endsection
@section('content')
<!--Features Section-->
<section class="feature_wrap padding-half" id="specialities">
  <div class="container">
    <div class="row">
     <div class="col-md-12 text-center">
        <h2 class="heading ">Our &nbsp; Specialities</h2>
        <hr class="heading_space">
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-sm-6 feature text-center">
        <i class="icon-glass"></i>
        <h3><a href="services.html">Dinner &amp; Dessert</a></h3>
        <!-- <p> you need a doctor for to consectetuer Lorem ipsum dolor, consectetur adipiscing onsectetur.</p> -->
      </div>
      <div class="col-md-6 col-sm-6 feature text-center">
        <i class="icon-coffee"></i>
        <h3><a href="services.html">Breakfast</a></h3>
        <!-- <p> you need a doctor for to consectetuer Lorem ipsum dolor, consectetur adipiscing onsectetur Graphic.</p> -->
      </div>
      <!-- <div class="col-md-3 col-sm-6 feature text-center">
        <i class="icon-glass"></i>
        <h3><a href="services.html">Ice Shakes</a></h3>
        <p> you need a doctor for to consectetuer Lorem ipsum dolor, consectetur adipiscing onsectetur Graphic Power.</p>
      </div>
      <div class="col-md-3 col-sm-6 feature text-center">
        <i class="icon-coffee"></i>
        <h3><a href="services.html">Beverges</a></h3>
        <p> you need a doctor for to coansectetuer Lorem ipsum dolor, consectetur adipiscing.</p>
      </div> -->
    </div>
    
  </div>
</section>


<!--Services plus working hours-->
<section id="services">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
         <h2 class="heading">Featured &nbsp; Food</h2>
         <hr class="heading_space">
         <div class="slider_wrap">
        <div id="service-slider" class="owl-carousel">
        @if($feature_menu->count() > 0)
            @foreach($feature_menu as $menu)
              <div class="item">
                <div class="item_inner">
                <div class="image">
                  <img src="{{url($menu->thumbnail)}}" alt="Services Image">
                  <a  href="services.html"></a>
                </div>
                  <h3><a href="services.html">{{$menu->title}}</a></h3>
                  <p>{!!substr(nl2br($menu->description), 0, 20)!!}</p>
                </div>
              </div>
            @endforeach
        @endif
        </div>
        </div>
      </div>
      <div class="col-md-4">
        <h2 class="heading">Our &nbsp; Menu</h2>
        <hr class="heading_space">
        <ul class="menu_widget">
          @forelse($limit_menus as $menu)
                  <li><a href="{{route('view.single.menu', ['id'=>$menu->id, 'slug' => str_slug($menu->title)])}}"><img src="{{url($menu->thumbnail)}}" style="height: 30px">&nbsp;&nbsp;&nbsp;&nbsp; {{str_limit($menu->title, 32)}}  <strong>&#8358;{{$menu->amount}}</strong> </a></li>
          @empty

          @endforelse 
        </ul>
         <!-- <h3>Other Special Menu</h3>
         <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse <strong>molestie consequat</strong>, vel illum dolore nulla facilisis.</p> -->
      </div>
    </div>
  </div>
</section>


<!-- image with content -->
<section class="info_section paralax" style="background: url('{{url('/build/images/deal-of-the-day-bg.jpg')}}') no-repeat; padding: 9% 0;">
  <div class="container">
    <div class="row">
      <div class="col-md-2"> </div>
      <div class="col-md-8">
         <div class="text-center">
         <h2 class="heading_space" style="color:yellow;text-shadow: 0px 0px 5px #444;">Meal of the Day</h2>
         <p class="heading_space detail" style="text-shadow: 0px 0px 5px #444;">Ugba and cassava with Ugwu leaves is one nominee<br>
 and the winner for the African Salad category. You can never go wrong with this delicacy.</p>
         <a href="{{route('order.page')}}" class="btn-common-white page-scroll">Order Now</a>
         </div>          
      </div>
      <div class="col-md-2"></div>
    </div>
  </div>
</section>



<!-- Food Gallery -->
<section id="gallery" class="padding">
  <div class="container">
      <div class="row">
     <div class="col-md-12 text-center">
        <h2 class="heading ">Delicious &nbsp; Food</h2>
        <hr class="heading_space">
        <div class="work-filter">
          <ul class="text-center">
             <li><a href="javascript:;" data-filter="all" class="active filter">All</a></li>
             @if($categories->count() > 0)
               @foreach($categories as $category)
               <li><a href="javascript:;" data-filter=".{{$category->class}}" class="filter"> {{$category->name}}</a></li>
               @endforeach
             @endif
          </ul>
        </div>
      </div>
    </div>
     <div class="row">
      <div class="zerogrid">
        <div class="wrap-container">
          <div class="wrap-content clearfix home-gallery">
            @if($menus->count() > 0)
            @foreach($menus as $menu)
              <div class="col-1-4 mix work-item {{$menu->category->class}} heading_space">
                <div class="wrap-col">
                  <div class="item-container">
                    <div class="image">
                      <img src="{{url($menu->thumbnail)}}" alt="cook"/>
                      <div class="overlay">
                          <a class="fancybox overlay-inner" href="{{url($menu->thumbnail)}}" data-fancybox-group="gallery"><i class=" icon-eye6"></i></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
          @endif
          </div>
        </div>
       </div>
      </div>
  </div>
</section>




<!-- facts counter  -->
<section id="facts" style="background: url('{{url('/build/images/deal-of-the-day-bg.jpg')}}') no-repeat">
  <div class="container">
    <div class="row number-counters"> 
      <!-- first count item -->
      <div class="col-sm-6 col-xs-12 text-center wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">
        <div class="counters-item row">
        <i class="icon-smile"></i> 
        <h2><strong data-to="4680">0</strong></h2>
          <p style="color:yellow;text-shadow: 0px 0px 5px #444;">Happy Customers</p>
        </div>
      </div>
      <div class="col-sm-6 col-xs-12 text-center wow fadeInDown" data-wow-duration="500ms" data-wow-delay="600ms">
        <div class="counters-item  row"> 
        <i class="icon-food"></i>
        <h2><strong data-to="865">0</strong></h2>
          <p style="color:yellow;text-shadow: 0px 0px 5px #444;">Dishes Served</p>
        </div>
      </div>
    </div>  
  </div>
</section>



<!-- Our cheffs -->
<section id="cheffs" class="padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
      <h2 class="heading">Our &nbsp; Kitchen &nbsp; Staff</h2>
      <hr class="heading_space">
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="cheffs_wrap_slider">
          <div id="our-cheffs" class="owl-carousel">
          @forelse($kitchen_chefs as $kitchen_chef)
            <div class="item">
              <div class="cheffs_wrap">
               <img src="{{$kitchen_chef->thumbnail}}" alt="Kitchen Staff">
               <h3>{{$kitchen_chef->name}}</h3>
               <small>{{$kitchen_chef->kitchen_rank->name}}</small>
               <p>{{$kitchen_chef->excerpt}}</p>
              </div>
            </div>
          @empty

          @endforelse
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<!-- Order Online --> 
<!--<section id="order-form" class="order_section">
  <div class="container order_form padding">
    <div class="row">
      <div class="col-md-12 appointment_form">
        <h2 class="heading">Online Order</h2>
        <hr class="heading_space">
      <div class="row">  
       <div class="col-md-8">
       <form onSubmit="return false" id="order_form" class="callus">
            <div class="row">
            <div class="form-group col-md-12">
            <div id="result" class="text-center"></div></div></div>
             <div class="row">
             <div class="form-group col-md-12">
            <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
            </div></div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Name"  name="name" id="name"  required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                   <input type="email" class="form-control" placeholder="Email address" name="email" id="email" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Phone No" name="phone" id="phone" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <select class="form-control" id="deal" name="deal">
                       <option value="Not Selected"> Select Deal </option>
                       <option> Deal One ($200) </option>
                       <option> Deal Two ($500) </option>
                       <option> Deal Three ($900) </option>
                       <option> Deal Four ($1300) </option>
                    </select>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <textarea placeholder="Order Details"  id="message" name="message" required></textarea>
                  </div>

                  <div class="form-group">
                     <div class="btn-submit btn-common-white">
                    <input type="submit" value="Place &nbsp; Order" id="btn_order_submit" />
                    </div>
                  </div>
                </div>
              </div>
            </form>
           </div> 
           </div>
      </div>
      <div class="col-md-3">
      </div>
    </div>
    <div class="col-md-3"></div>
  </div>
</section>-->



<!--Featured Receipes -->
<!--<section id="news" class="bg_grey padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
      <h2 class="heading">Featured &nbsp; Food &nbsp; Receipes</h2>
      <hr class="heading_space">
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="cheffs_wrap_slider">
          <div id="news-slider" class="owl-carousel">
            <div class="item">
              <div class="news_content">
               <img src="images/news-img3.jpg" alt="Docotor">
               <div class="date_comment">
                  <span>22<small>apr</small></span>
                  <a href="#."><i class="icon-comment"></i> 5</a>
               </div>
               <div class="comment_text">
                 <h3><a href="#.">Lorem ipsum dolor</a></h3>
                 <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore nulla facilisis.</p>
               </div>
              </div>
            </div>
            <div class="item">
              <div class="news_content">
               <img src="images/news-img2.jpg" alt="Docotor">
               <div class="date_comment">
                  <span>22<small>apr</small></span>
                  <a href="#."><i class="icon-comment"></i> 5</a>
               </div>
               <div class="comment_text">
                 <h3><a href="#.">Lorem ipsum</a></h3>
                 <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore nulla facilisis.</p>
               </div>
              </div>
            </div>
            <div class="item">
              <div class="news_content">
               <img src="images/news-img1.jpg" alt="Docotor">
               <div class="date_comment">
                  <span>22<small>apr</small></span>
                  <a href="#."><i class="icon-comment"></i> 5</a>
               </div>
               <div class="comment_text">
                 <h3><a href="#.">Lorem ipsum dolor</a></h3>
                 <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore nulla facilisis.</p>
               </div>
              </div>
            </div>
            <div class="item">
              <div class="news_content">
               <img src="images/news-img3.jpg" alt="Docotor">
               <div class="date_comment">
                  <span>22<small>apr</small></span>
                   <a href="#."><i class="icon-comment"></i> 5</a>
               </div>
               <div class="comment_text">
                 <h3><a href="#.">Lorem dolor</a></h3>
                 <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore nulla facilisis.</p>
               </div>
              </div>
            </div>
            <div class="item">
              <div class="news_content">
               <img src="images/news-img2.jpg" alt="Docotor">
               <div class="date_comment">
                  <span>22<small>apr</small></span>
                   <a href="#."><i class="icon-comment"></i> 5</a>
               </div>
               <div class="comment_text">
                 <h3><a href="#.">Lorem ipsum dolor</a></h3>
                 <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore nulla facilisis.</p>
               </div>
              </div>
            </div>
            <div class="item">
              <div class="news_content">
               <img src="images/news-img1.jpg" alt="Docotor">
               <div class="date_comment">
                  <span>22<small>apr</small></span>
                   <a href="#."><i class="icon-comment"></i> 5</a>
               </div>
               <div class="comment_text">
                 <h3><a href="#.">Lipsum dolor</a></h3>
                 <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore nulla facilisis.</p>
               </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>-->


<!-- testinomial -->
<section id="testinomial" class="padding">
  <div class="container">
  <div class="row">
      <div class="col-md-12 text-center">
      <h2 class="heading">Our &nbsp; happy &nbsp; Customers</h2>
      <hr class="heading_space">
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
      <div id="testinomial-slider" class="owl-carousel text-center">
        <div class="item">
          <h3>Every time hunger calls, you guy's delicacy never seems to amaze me.</h3>
          <p>Chris Martin</p>
        </div>
        <div class="item">
          <h3>A food with a difference.</h3>
          <p>Alex Hales</p>
        </div>
        <div class="item">
          <h3>Your service always want me to call on you guys again and again.</h3>
          <p>Shane Robertson</p>
        </div>
       </div>
      </div>
    </div>
  </div>
</section>
@endsection