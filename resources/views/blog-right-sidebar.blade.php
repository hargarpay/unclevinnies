<aside class="sidebar">
           <div class="widget">
            <ul class="tabs">
              <li class="active" rel="tab1">Popular </li>
              <li rel="tab2">Latest</li>
              <li rel="tab3">Comments</li>
            </ul>
           <div class="tab_container bg_grey">
          <h3 class="d_active tab_drawer_heading" rel="tab1">Popular</h3>
          <div id="tab1" class="tab_content">
          @forelse($popular_blogs as $blog)
            <div class="single_comments">
              <img alt="" src="{{url(head(json_decode($blog->placeholder, true)))}}">
              <p><a href="#.">{{$blog->title}}</a> <span>{{\Carbon\Carbon::parse($blog->created_at)->format('jS F, Y')}}</span></p>
            </div>
            <div class="clearfix"></div>
          @empty
          @endforelse
          </div>
          <h3 class="tab_drawer_heading" rel="tab2">Latest</h3>
          <div id="tab2" class="tab_content">
          @forelse($latest_blogs as $blog)
            <div class="single_comments">
              <img alt="" src="{{url(head(json_decode($blog->placeholder, true)))}}">
              <p><a href="#.">{{$blog->title}}</a> <span>{{\Carbon\Carbon::parse($blog->created_at)->format('jS F, Y')}}</span></p>
            </div>
            <div class="clearfix"></div>
          @empty
          @endforelse
          </div>
           <h3 class="tab_drawer_heading" rel="tab3">Comments</h3>
          <div id="tab3" class="tab_content">
           @forelse($blog_sidebar_comments as $blog_comment)
            <div class="single_comments">
                <img alt="" src="{{url($blog_comment->user->passport)}}">
                <p><a href="#.">{{str_limit($blog_comment->comment, 10)}}</a> <span>{{\Carbon\Carbon::parse($blog_comment->created_at)->format('jS F, Y')}}</span></p>
                
             </div>
          @empty
              <div class="single_comments">
                <i class="fa fa-fw fa-exclamation-triangle"></i>
                <p>No comment is available</p>
              </div>
          @endforelse
          </div> 
        </div>
           </div>
           <div class="widget">
             <h3>Categories</h3>
             <ul class="widget_links">
               @forelse($categories as $category)
               <li><a href="#.">{{$category->category}}</a></li>
               @empty
               <li>No Category is Available</li>
               @endforelse
             </ul>
           </div>
           <div class="widget">
             <h3>Tags</h3>
             <ul class="tags">
              @forelse($tags as $tag)
               <li><a href="#.">{{$tag->tag}}</a></li>
              @empty
               <li>No Tag is Available</li>
              @endforelse
             </ul>
           </div>
        </aside>