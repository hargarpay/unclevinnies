@extends('emails.layout.main')

@section('content')
<tr>
	<td class="two-column" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;text-align:center;font-size:0;" >
        <!--[if (gte mso 9)|(IE)]>
        <table width="100%" style="border-spacing:0;font-family:sans-serif;color:#333333;" >
        <tr>
        <td width="50%" valign="top" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
        <![endif]-->
        <div class="column" style="width:100%;max-width:300px;display:inline-block;vertical-align:top;" >
			<table width="100%" style="border-spacing:0;font-family:sans-serif;color:#333333;" >
		        <tbody><tr>
		            <td class="inner" style="padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;" >
		                <table class="contents" style="border-spacing:0;font-family:sans-serif;color:#333333;width:100%;font-size:14px;text-align:left;" >
						    <tbody><tr>
					            <td class="inner text ash-bg" style="padding-bottom:10px;padding-right:10px;padding-left:10px;background-color:#f1f1f1;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;padding-top:10px;" >
					            		<p class="h2" style="Margin:0;font-size:18px;font-weight:bold;Margin-bottom:12px;" >Payer Information</p>
					                    <p class="h3b bottom-border" style="Margin:0;font-size:15px;font-weight:bold;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#6c6868;Margin-bottom:5px;padding-bottom:5px;" >Fullname</p>
										<p class="h3 bottom-border" style="Margin:0;font-size:12px;font-weight:bold;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#6c6868;Margin-bottom:5px;padding-bottom:5px;" >{{$user['fullname']}}</p>
					                    <p class="h3b bottom-border" style="Margin:0;font-size:15px;font-weight:bold;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#6c6868;Margin-bottom:5px;padding-bottom:5px;" >Email </p>
					                    <p class="h3 bottom-border" style="Margin:0;font-size:12px;font-weight:bold;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#6c6868;Margin-bottom:5px;padding-bottom:5px;" >{{$user['email']}}</p>
					                    <p class="h3b bottom-border" style="Margin:0;font-size:15px;font-weight:bold;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#6c6868;Margin-bottom:5px;padding-bottom:5px;" >Phone Number </p>
					                    <p class="h3 bottom-border" style="Margin:0;font-size:12px;font-weight:bold;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#6c6868;Margin-bottom:5px;padding-bottom:5px;" >{{$user['phone']}}</p>
					                    <p class="h3b bottom-border" style="Margin:0;font-size:15px;font-weight:bold;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#6c6868;Margin-bottom:5px;padding-bottom:5px;" >Address </p>
					                    <p class="h3 bottom-border" style="Margin:0;font-size:12px;font-weight:bold;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#6c6868;Margin-bottom:5px;padding-bottom:5px;" >{{$user['address']}} : <span style="color:green; font-weight:400"></span></p>
					                    <p class="h3 bottom-border" style="Margin:0;font-size:12px;font-weight:bold;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#6c6868;Margin-bottom:5px;padding-bottom:5px;" >Payment Method : <span style="color:green; font-weight:400">{{$user['payment_method']}}</span></p>
					                    <p class="h3 bottom-border" style="Margin:0;font-size:12px;font-weight:bold;" >Delivery : <span style="color:green; font-weight:400">{{$user['delivery']}}</span></p>
					            </td>
						    </tr>
						</tbody></table>
		            </td>
		        </tr>
		    </tbody></table>
		</div>
        <!--[if (gte mso 9)|(IE)]>
        </td><td width="50%" valign="top" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
        <![endif]-->
        <div class="column" style="width:100%;max-width:300px;display:inline-block;vertical-align:top;" >
			<table width="100%" style="border-spacing:0;font-family:sans-serif;color:#333333;" >
		        <tbody><tr>
		            <td class="inner" style="padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;" >
		                <table class="contents" style="border-spacing:0;font-family:sans-serif;color:#333333;width:100%;font-size:14px;text-align:left;" >
						    <tbody><tr>
					            <td class="inner text ash-bg" style="padding-bottom:10px;padding-right:10px;padding-left:10px;background-color:#f1f1f1;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;padding-top:10px;" >
					            		<p class="h2" style="Margin:0;font-size:18px;font-weight:bold;Margin-bottom:12px;" >Order Menu</p>
					            		@foreach($items as $menu)
					                   	<div class="bottom-border" style="border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#6c6868;Margin-bottom:5px;padding-bottom:5px;" >
								            <table style="border-spacing:0;font-family:sans-serif;color:#333333;" >
								                <tbody><tr>
								                    <td class="products-small" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
								                    	<a href="{{route('view.single.menu', ['id' => $menu['id'], 'slug' => str_slug($menu['name'])])}}" style="color:#000000;text-decoration:none;">
							                            <div class="img" style="display:inline-block;" > <img src="{{str_replace(' ', '%20', url($menu['image']))}}" height="30" style="border-width:0;" >  </div>
							                            <div class="content" style="float:right;width:75%;" >
							                                <p class="h3" style="Margin:0;font-size:12px;font-weight:bold;" >{{$menu['quantity']}} x {{$menu['name']}}</p>
							                                <p class="price" style="Margin:0;" >Unit Price: <b>₦{{$menu['price']}}</b>
							                                <br>
							                                 SubTotal:  <b>₦{{$menu['quantity'] * $menu['price']}}</b></p>
							                                
							                            </div>
							                            </a>
								                    </td>
								                </tr> 
								            </tbody></table>
								         </div>
								         @endforeach
								     	<table style="border-spacing:0;font-family:sans-serif;color:#333333;" >
							                <tbody><tr>
							                    <td class="products-small" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
						                            <div class="content" >
						                                <p class="h3b" style="Margin:0;font-size:15px;font-weight:bold;Margin-bottom:5px;color:green;" >Grand Total:  <b>₦{{$grand_total}}</b></p>
						                                
						                            </div>
							                    </td>
							                </tr> 
							            </tbody>
							         </table>
					            </td>
						    </tr>
						</tbody></table>
		            </td>
		        </tr>
		    </tbody></table>
		</div>
        <!--[if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]-->
    </td>
</tr>

@endsection