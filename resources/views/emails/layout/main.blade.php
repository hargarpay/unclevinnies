<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!--[if !mso]><!-->
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!--<![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <style type="text/css">
        /* Basics */
        body {
            margin: 0 !important;
            padding: 0;
            background-color: #ffffff;
        }
        table {
            border-spacing: 0;
            font-family: sans-serif;
            color: #333333;
        }
        td {
            padding: 0;
        }
        img {
            border: 0;
        }
        div[style*="margin: 16px 0"] { 
            margin:0 !important;
        }
        .wrapper {
            width: 100%;
            table-layout: fixed;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }
        .webkit {
            max-width: 600px;
            margin: 0 auto;
        }
        .outer {
            Margin: 0 auto;
            width: 100%;
            max-width: 600px;
        }
        .full-width-image img {
            width: 100%;
            max-width: 600px;
            height: auto;
        }
        .inner {
            padding: 10px;
        }
        p{
            Margin: 0;
        }
        a {
            color: #ee6a56;
            text-decoration: underline;
        }
        .h1 {
            font-size: 21px;
            font-weight: bold;
            Margin-bottom: 18px;
        }
        .h2 {
            font-size: 18px;
            font-weight: bold;
            Margin-bottom: 12px;
        }
        .h3{
            font-size: 12px;
            font-weight: bold;
        }

        .h3b{
            font-size: 15px;
            font-weight: bold;
            Margin-bottom: 5px;
        }
         
        /* One column layout */
        .one-column .contents {
            text-align: left;
        }
        .one-column .contents.center{
            text-align: center;
            vertical-align: center;
        }
        .one-column p {
            font-size: 14px;
            Margin-bottom: 10px;
        }
        /*Two column layout*/
        .two-column {
            text-align: center;
            font-size: 0;
        }
        .two-column .column {
            width: 100%;
            max-width: 300px;
            display: inline-block;
            vertical-align: top;
        }
        .contents {
            width: 100%;
        }
        .two-column .contents {
            font-size: 14px;
            text-align: left;
        }
        .two-column .img {
            display: inline-block;
        }

        .two-column .text {
            padding-top: 10px;
        }
        /*Colors*/
        .ash-bg{
            background: #f1f1f1;
        }
        .orange-bg{
            background: #f65708;
        }
        .white-color{
            color: #ffffff;
        }
        .black-bg{
            background: #323232;
        }

        /* Right sidebar layout */
        .right-sidebar {
            text-align: center;
            font-size: 0;
        }
        .right-sidebar .column {
            width: 100%;
            display: inline-block;
            vertical-align: middle;
        }
        .right-sidebar .left {
            max-width: 150px;
        }
        .right-sidebar .right {
            max-width: 450px;
        }
        .right-sidebar .img {
            width: 100%;
            max-width: 130px;
            height: auto;
        }
        .right-sidebar .contents {
            font-size: 14px;
            text-align: center;
        }

        .right-sidebar .contents.left {
            text-align: left;
        }

        .right-sidebar .contents.right {
            text-align: right;
        }

        .right-sidebar a {
            color: #70bbd9;
        }

        .bottom-border{
            border-bottom: 1px solid #6c6868;
            Margin-bottom: 5px;
            padding-bottom:5px;
        }

        /*Three column layout*/
        .three-column {
            text-align: center;
            font-size: 0;
            padding-top: 10px;
            padding-bottom: 10px;
        }
        .three-column .column {
            width: 100%;
            max-width: 200px;
            display: inline-block;
            vertical-align: top;
        }
        .three-column .contents {
            font-size: 14px;
            text-align: center;
        }
        .three-column img {
            width: 100%;
            max-width: 180px;
            height: auto;
        }
        .three-column .text {
            padding-top: 10px;
        }

    </style>
    <!--[if (gte mso 9)|(IE)]>
    <style type="text/css">
        table {border-collapse: collapse;}
    </style>
    <![endif]-->
</head>
<body style="margin-top:0 !important;margin-bottom:0 !important;margin-right:0 !important;margin-left:0 !important;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;background-color:#ffffff;" >
    <center class="wrapper" style="width:100%;table-layout:fixed;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;" >
        <div class="webkit" style="max-width:600px;margin-top:0;margin-bottom:0;margin-right:auto;margin-left:auto;" >
            <!--[if (gte mso 9)|(IE)]>
            <table width="600" align="center" cellpadding="0" cellspacing="0" border="0" style="border-spacing:0;font-family:sans-serif;color:#333333;" >
            <tr>
            <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
            <![endif]-->
            <table class="outer" align="center" style="border-spacing:0;font-family:sans-serif;color:#333333;Margin:0 auto;width:100%;max-width:600px;" >
            <!-- Header -->
                <tbody><tr>
                    <td class="right-sidebar black-bg" dir="ltr" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;background-color:#323232;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;text-align:center;font-size:0;" >
                        <!--[if (gte mso 9)|(IE)]>
                        <table width="100%" dir="rtl" style="border-spacing:0;font-family:sans-serif;color:#333333;" >
                        <tr>
                        <td width="100" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
                        <![endif]-->
                        <div class="column left" dir="rtl" style="width:100%;display:inline-block;vertical-align:middle;max-width:150px;" >
                            <table width="100%" style="border-spacing:0;font-family:sans-serif;color:#333333;" >
                                <tbody><tr>
                                    <td class="inner contents" style="padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;width:100%;font-size:14px;text-align:center;" >
                                       <a href="{{url('/')}}" style="text-decoration:underline;color:#70bbd9;" ><img src="{{url('/images/logo.png')}}" width="130" alt="" style="border-width:0;" ></a>
                                    </td>
                                </tr>
                            </tbody></table>
                        </div>
                        <!--[if (gte mso 9)|(IE)]>
                        </td><td width="500" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
                        <![endif]-->
                        <div class="column right" dir="rtl" style="width:100%;display:inline-block;vertical-align:middle;max-width:450px;" >
                            <table width="100%" style="border-spacing:0;font-family:sans-serif;color:#333333;" >
                                <tbody><tr>
                                    <td class="inner contents" style="padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;width:100%;font-size:14px;text-align:center;" >
                                        <h2 class="white-color" style="color:#ffffff;" >Uncle Vinnies ...taste is in the grill</h2>
                                    </td>
                                </tr>
                            </tbody></table>
                        </div>
                        <!--[if (gte mso 9)|(IE)]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                    </td>
                </tr>
            <!-- Content -->
                @yield('content')
            <!-- Latest Menu Avaialabe -->
            <tr>
                <td class="one-column" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
                    <table width="100%" class="black-bg" style="border-spacing:0;font-family:sans-serif;color:#333333;background-color:#323232;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;" >
                        <tbody><tr>
                            <td class="inner contents center" style="padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;width:100%;text-align:left;" >
                                <p style="Margin:0;font-size:18px;font-weight: bold; color:#ee6a56;" >New Arrival</p>
                            </td>
                        </tr>
                    </tbody>
                  </table>
                </td>
            </tr>
            <tr>
                <td class="three-column" style="padding-right:0;padding-left:0;text-align:center;font-size:0;padding-top:10px;padding-bottom:10px;" >
                        <!--[if (gte mso 9)|(IE)]>
                        <table width="100%" style="border-spacing:0;font-family:sans-serif;color:#333333;" >
                        <tr><![endif]-->
                        @forelse($latest_menu as $menu)
                                <!--[if (gte mso 9)|(IE)]>
                        <td width="200" valign="top" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
                        <![endif]-->
                        <div class="column" style="width:100%;max-width:200px;display:inline-block;vertical-align:top;" >
                            <table width="100%" style="border-spacing:0;font-family:sans-serif;color:#333333;" >
                                <tbody><tr>
                                    <td class="inner" style="padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;" >
                                        <table class="contents" style="border-spacing:0;font-family:sans-serif;color:#333333;width:100%;font-size:14px;text-align:center;" >
                                            <tbody><tr>
                                                <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
                                                    <a href="{{route('view.single.menu', ['id' => $menu['id'], 'slug' => str_slug($menu['title'])])}}" style="color:#ee6a56;text-decoration:none;">
                                                        <img src="{{str_replace(' ', '%20',url($menu['thumbnail']))}}" width="180" alt="" style="border-width:0;width:100%;max-width:180px;height:auto;" >
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text" style="padding-bottom:0;padding-right:0;padding-left:0;padding-top:10px;" >
                                                    <p class="h3b" style="Margin:0;font-size:15px;font-weight:bold;Margin-bottom:5px;" ><a href="{{route('view.single.menu', ['id' => $menu['id'], 'slug' => str_slug($menu['title'])])}}" style="color:#ee6a56;text-decoration:none;">{{$menu['title']}}</a></p>
                                                    <p style="Margin:0;" >{{str_limit($menu['description'], 25)}}</p>
                                                </td>
                                            </tr>
                                        </tbody></table>
                                    </td>
                                </tr>
                            </tbody></table>
                        </div>
                        <!--[if (gte mso 9)|(IE)]>
                        </td>
                        <![endif]-->
                        @empty
                        @endforelse
                                
                        <!--[if (gte mso 9)|(IE)]>
                        </tr>
                        </table>
                        <![endif]-->
                </td>
            </tr>
            <!-- Footer -->
                <tr>
                    <td class="one-column" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
                        <table width="100%" class="black-bg" style="border-spacing:0;font-family:sans-serif;color:#333333;background-color:#323232;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;" >
                            <tbody><tr>
                                <td class="inner contents center" style="padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;width:100%;text-align:left;" >
                                    <p style="Margin:0;font-size:14px;Margin-bottom:10px;text-align: center" ><a href="{url('/register')}}" style="color:#ee6a56;text-decoration:underline;" >Create Account</a> &nbsp;&nbsp;&nbsp;&nbsp;<a href="{{route('order.page')}}" style="color:#ee6a56;text-decoration:underline;" >View All menu</a> &nbsp;&nbsp;&nbsp;&nbsp;<a href="{{route('view.blogs')}}" style="color:#ee6a56;text-decoration:underline;" >View Blog</a> </p>
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </div>
    </center>

</body></html>