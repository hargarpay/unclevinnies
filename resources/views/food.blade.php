@extends('layouts.main')
@section('banner')
<!--Page header & Title-->
<section id="page_header" style="background: url('{{url('/build/images/banner-bg-2.jpg')}}') no-repeat">
<div class="page_title">
  <div class="container">
    <div class="row">
      <div class="col-md-12" style="text-align: center;">
         <h2 class="title">Our Food</h2>
         <!-- <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit</p> -->
      </div>
    </div>
  </div>
</div>  
</section>
@endsection

@section('content')
<!--Welcome-->
<section id="welcome" class="padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
         <h2 class="heading">Welcome to Uncle Vinnies</h2>
         <hr class="heading_space">
      </div>
      <div class="col-md-7 col-sm-6">
        <p class="half_space">Uncle vinnies grill offers food made by the best hands, well packaged by the best of minds. Not your ordinary restaurant. We offer you food made in a healthy environment. If trees and kids playing, in general- parks, make you happy, then uncle vinnies would certainly get you excited.</p>

        <p class="half_space">It's a place where you can eat and relax and enjoy nature at its best. We offer different kinds of food. From Continental to African. There's something for everyone. Perfect for that family outing you've been wishing for! What's more, we have amazing, affordable prices. Uncle vinnies grill will certainly leave you satisfied with an unforgettable experience. So what are you waiting for, head over to the grill right now!!!</p>
        <ul class="welcome_list">
        <li>Business Events</li>
        <li>Birthdays</li>
        <li>Weddings</li>
        <li>Party & Others</li>
        </ul> 
      </div>
      <div class="col-md-5 col-sm-6">
       <img class="img-responsive" src="{{url('images/food-menu-bg.jpg')}}" alt="welcome to uncle vinnies">
      </div>
    </div>
  </div>
</section> 


<!--Food Facilities-->
<section id="food" class="padding bg_grey">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 class="heading">Our &nbsp; Menu</h2>
        <hr class="heading_space">
      </div>
    </div>
    <div class="row">
    <div class="col-md-4">
        <ul class="menu_widget">
        @forelse($limit_menus as $menu)
          <li><a href="{{route('view.single.menu', ['id'=>$menu->id, 'slug' => str_slug($menu->title)])}}"><img src="{{url($menu->thumbnail)}}" style="height: 30px">&nbsp;&nbsp;&nbsp;&nbsp; {{$menu->title}}  <span>&#8358;{{$menu->amount}}</span> </a></li>
        @empty

        @endforelse
        </ul>
      </div>
      <div class="col-md-8 grid_layout">
      <div class="row">
      <div class="zerogrid">
          <div class="wrap-container">
            <div class="wrap-content clearfix">
            @if($menus->count() > 0)
            @for($i = 0 ; $i <= 1; $i++)
              <div class="col-1-2">
              <div class="wrap-col first">
                  <div class="item-container"> 
                   <img src="{{url($menus[$i]->thumbnail)}}" alt="cook"/>
                   <div class="overlay">
                       <a class="overlay-inner fancybox" href="images/grid-layout2.jpg" data-fancybox-group="gallery">
                           {{$menus[$i]->title}}
                       </a> 
                   </div>
                  </div>
                </div>
              </div>
             @endfor
          @endif
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!--Populars-->
<section class="padding" id="popular">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
      <h2 class="heading">Featured Food</h2>
      <hr>
      </div>
      @forelse($feature_menus as $menu)
      <div class="col-sm-3">
        <div class="popular top40 text-center">
          <div class="image">
             <img src="{{url($menu->thumbnail)}}" alt="popular1">
             <div class="overlay">
               <a class="fancybox overlay-inner" href="{{url($menu->thumbnail)}}" data-fancybox-group="gallery">
                 <i class="icon-eye6"></i>
               </a>
             </div>
          </div>
          <a href="{{route('view.single.menu', ['id'=>$menu->id, 'slug' => str_slug($menu->title)])}}">
          <h4>{{$menu->title}}</h4>
          <span class="dish">&#8358;{{$menu->amount}}</span>
          <p>{!!substr(nl2br($menu->description, true), 0, 20)!!}</p>
          </a>
        </div>
      </div>
     @empty

     @endforelse
    </div>
  </div>
</section>


<!--Blue Section-->
<section class="bg_bistro padding-half">
  <div class="container">
    <div class="row">
      @forelse($limit_categories as $category)
      <div class="col-md-4 col-sm-6 white_content">
      <a href="{{route('order.page')}}">
        <img src="{{url($category->banner)}}" class="img-responsive img-circle" style="box-shadow: 0 0 10px rgba(50,50,50,0.5);">
        <h3 style="text-align: center;text-shadow: 0 0 5px #444;margin-top: 10px">{{$category->name}}</h3>
      </a>
        <!-- <p> you need a doctor for to consectetuer Lorem ipsum dolor, consectetur adipiscing onsectetur Graphic Power Ut eros.</p> -->
      </div>
      @empty
        <div class="col-md-4 col-sm-6 white_content">
          <i class="icon-coffee"></i>
          <h3><a href="{{route('order.page')}}">Breakfast</a></h3>
          <p> you need a doctor for to consectetuer Lorem ipsum dolor, consectetur adipiscing onsectetur Graphic Power Ut eros.</p>
        </div>
        <div class="col-md-4 col-sm-6 white_content">
          <i class="icon-coffee2"></i>
          <h3><a href="{{route('order.page')}}">Ice Shakes</a></h3>
          <p> you need a doctor for to consectetuer Lorem ipsum dolor, consectetur adipiscing onsectetur Graphic Power Ut eros.</p>
        </div>
        <div class="col-md-4 col-sm-6 white_content">
          <i class="icon-glass2"></i>
          <h3><a href="{{route('order.page')}}">Beverages</a></h3>
          <p> you need a doctor for to consectetuer Lorem ipsum dolor, consectetur adipiscing onsectetur Graphic Power Ut eros.</p>
        </div>
        <div class="col-md-4 col-sm-6 white_content">
          <i class="icon-spoon-knife"></i>
          <h3><a href="{{route('order.page')}}">Delicious Food</a></h3>
          <p> you need a doctor for to consectetuer Lorem ipsum dolor, consectetur adipiscing onsectetur Graphic Power Ut eros.</p>
        </div>
        <div class="col-md-4 col-sm-6 white_content">
          <i class="icon-bowl"></i>
          <h3><a href="{{route('order.page')}}">Desserts</a></h3>
          <p> you need a doctor for to consectetuer Lorem ipsum dolor, consectetur adipiscing onsectetur Graphic Power Ut eros.</p>
        </div>

      @endforelse
    </div>
  </div>
</section>



<!-- testinomial -->
<section id="testinomial" class="padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
      <div id="testinomial-slider" class="owl-carousel text-center">
        <div class="item">
          <h3>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram.</h3>
          <p>Rodney Stratton, <span>Heart Patient</span></p>
        </div>
        <div class="item">
          <h3>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. quam nunc putamus parum claram, Mirum est notare quam littera gothica.</h3>
          <p>Rodney Robert, <span>Kidney Patient</span></p>
        </div>
        <div class="item">
          <h3>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse.</h3>
          <p>Rodney Alzbeth, <span>Liver Patient</span></p>
        </div>
       </div>
      </div>
    </div>
  </div>
</section>
@endsection