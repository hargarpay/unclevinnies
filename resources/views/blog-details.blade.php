@extends('layouts.main')
@section('banner')
<!--Page header & Title-->
<section id="page_header" style="background: url('{{url('/build/images/banner-bg-8.jpg')}}') no-repeat">
<div class="page_title">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
         <h2 class="title">{{$blog->title}} </h2>
         <!-- <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit</p> -->
      </div>
    </div>
  </div>
</div>  
</section>
@endsection
@section('content')
<!-- Blogs Details-->
<!-- Blog Details -->
<section id="blog" class="padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-sm-7">
        <div class="blog_item padding-bottom order-page">
           <h2>{{$blog->title}} </h2>
           <ul class="comments">
             <li><a href="#.">{{\Carbon\Carbon::parse($blog->created_at)->format('jS F, Y')}}</a></li>
             <li><a href="#."><i class="icon-chat-2"></i>5</a></li>
           </ul>
          <div class="image_container">
          @if($blog->type == 'image')
            @foreach(json_decode($blog->placeholder, true) as $image_src)
              <img src="{{url($image_src)}}" class="img-responsive" alt="blog post">
            @endforeach
          @elseif($blog->type == 'slider')
             <div id="blog-slider" class="owl-carousel">
            @foreach(json_decode($blog->placeholder, true) as $image_src)
              <div class="item"><img src="{{url($image_src)}}" class="img-responsive" alt="blog post"></div>
            @endforeach
             </div>
          @else
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="{{$blog->placeholder}}"></iframe>
            </div>
          @endif
          </div>
          {!!str_replace(['&lt;p&gt;', '&lt;/p&gt;'], ['<p>', '</p>'],$blog->description)!!}
          <div class="clearfix">
          <ul class="comments pull-left">
          <li><a href="#."><i class="icon-tag2"></i>{{rtrim($blog_tags, ', ')}}</a></li>
          </ul>
          <ul class="social_icon pull-right">
               <li><a href="#." class="black" id="share_button" data-href="{{route('view.single.blog', ['id' => $blog->id, 'slug' => str_slug($blog->title)])}}"><i class="fa fa-facebook" style="font-size:20px; color:#1684f2;"></i></a></li>
               <li><a href="https://twitter.com/intent/tweet?text={{urlencode($blog->title)}}&url={{urlencode(route('view.single.blog', ['id' => $blog->id, 'slug' => str_slug($blog->title)]))}}" class="black"><i class="fa fa-twitter" style="font-size:20px; color:#1684f2;"></i></a></li>
               <li><a href="https://plus.google.com/share?url={{urlencode(route('view.single.blog', ['id' => $blog->id, 'slug' => str_slug($blog->title)]))}}" class="black" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'"><i class="fa fa-google-plus" style="font-size:20px; color:#e25111;"></i></a></li>
              </ul>
          </div>
          <h3>{{$all_blog_comments->count()}} {{$all_blog_comments->count() == 1 ? 'Comment' : 'Comments'}}</h3>
          <div class="media-list">
            @forelse($blog_comments as $blog_comment)
              @php getComments($blog_comment) @endphp
            @empty

            @endforelse
          </div>

          
          <h3>Leave a Reply</h3>
          @include('partials.form-errors')
          <form class="callus" id="reply-form" method="post" action="{{route('comment.blog', ['id' => $blog->id])}}">
          @if(auth()->check())
          <input type="hidden" name="_token" value="csrf_token()">
          <input type="hidden" name="user_id" value="{{auth()->id()}}">
          <input type="hidden" name="email" value="{{auth()->user()->email}}">
          <input type="hidden" name="parent_id" value="0">
          <input type="hidden" name="blog_id" value="{{$blog->id}}">
            @endif
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <textarea placeholder="Message" name="comment"></textarea>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn-submit button3 pull-right">Comment</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="col-md-4 col-sm-5">
        @include('blog-right-sidebar')
    </div>
    </div>
  </div>
</section>
@endsection