@extends('layouts.main')

@section('content')

<div class="container"  style="margin-top:9em">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="panel panel-warning">
                <div class="panel-heading"><h2>Register</h2></div>
                <div class="panel-body">
                @include('partials.form-errors')
                    <form class="form-horizontal callus" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('fullname') ? ' has-error' : '' }}">
                            <label for="fullname" class="col-sm-2 controfullnamel-label">Fullname</label>

                            <div class="col-sm-8">
                                <input id="fullname" type="text" class="form-control" name="fullname" value="{{ old('fullname') }}">
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-sm-2 control-label">E-Mail Address</label>

                            <div class="col-sm-8">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-sm-2 control-label">Password</label>

                            <div class="col-sm-8">
                                <input id="password" type="password" class="form-control" name="password">
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-sm-2 control-label">Confirm Password</label>

                            <div class="col-sm-8">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="address" class="col-sm-2 control-label">Contact Address</label>

                            <div class="col-sm-8">
                                <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}">
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="col-sm-2 control-label">Phone</label>

                            <div class="col-sm-8">
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}">
                            </div>
                        </div>

                        <div class="form-group" style="margin-top:30px;">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button type="submit" class="btn-submit padding" style="padding-top: 15px;padding-bottom: 15px;margin-top: -15px;">
                                    <i class="fa fa-btn fa-user"></i> Register
                                </button>
                            </div>
                            <div class="col-sm-6">
                                <a href="{{url('/login')}}" class="btn-submit padding">Login</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
