@extends('layouts.main')

@section('content')
<div class="container"  style="margin-top:9em">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="panel panel-warning">
                <div class="panel-heading"><h2>Login</h2></div>
                <div class="panel-body">
                @include('partials.form-errors')
                    <form class="form-horizontal callus" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-sm-2 control-label">E-Mail Address</label>

                            <div class="col-sm-8">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-sm-2 control-label">Password</label>

                            <div class="col-sm-8">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-2">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <a href="{{url('/register')}}" class="btn-submit padding">Register</a>
                            </div>
                            <div class="col-sm-6">
                                <button type="submit" class="btn-submit padding" style="padding-top: 15px;padding-bottom: 15px;margin-top: -15px;">
                                    <i class="fa fa-btn fa-sign-in"></i> Login
                                </button>
                            </div>
                            <div class="col-sm-4 col-sm-offset-2">
                                <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
