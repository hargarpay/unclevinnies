@extends('layouts.main')
@section('banner')
<section id="page_header" style="background: url('{{url('/build/images/banner-bg-9.jpg')}}') no-repeat">
<div class="page_title">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
         <h2 class="title">Order Now</h2>
         <!-- <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit</p> -->
      </div>
    </div>
  </div>
</div>  
</section>
@endsection
@section('content')
 <!-- Content -->
    <div id="content">

        <!-- Page Content -->
        <div class="page-content">
            <div class="container">
                <div class="row no-gutters">
                    <div class="col-md-3">
                        <!-- Menu Navigation -->
                        <nav id="menu-navigation" class="stick-to-content" data-local-scroll>
                            <div class="nav nav-menu" id="cart-view" style="margin-bottom:0; font-size:25px;padding: 10px;text-align: center; border: 1px solid;">
                                <a href="#" class="module module-cart right" data-toggle="panel-cart">
                                    <span class="cart-icon" style="position:relative;">
                                        <i class="glyphicon glyphicon-shopping-cart"></i>
                                        <span class="notification">{{\Cart::totalItems(true)}}</span>
                                    </span>
                                    <span class="cart-value" style="margin-left: 1.8em;border-left: 1px solid #dad2d2;padding-left: 0.5em" data-current-amount="{{(int)\Cart::total()}}">&#8358; {{Cart::total(false)}}</span>
                                </a>
                            </div>
                            <ul class="nav nav-menu bg-dark dark">
                            	 @forelse($categories as $category)
                                <li><a href="#{{$category->class}}" class="orderpage">{{$category->name}}</a></li>
                                @empty

                                @endforelse
                            </ul>
                        </nav>
                    </div>
                    <div class="col-md-9">
                        <!-- Menu Category / Burgers -->
                        @forelse($categories as $category)
                        <div id="{{$category->class}}" class="menu-category">
                            <div class="menu-category-title">
                                <div class="bg-image"><img src="{{url($category->banner)}}" alt=""></div>
                                <h2 class="title">{{$category->name}}</h2>
                            </div>
                            <div class="menu-category-content">
                                <!-- Menu Item -->
                                @forelse($category->publishMenu() as $menu)
                                <div class="menu-item menu-list-item">
                                    <div class="row align-items-center">
                                        <div class="col-sm-6 mb-2 mb-sm-0 menu-name">
                                            <a href="{{route('view.single.menu', ['id'=>$menu->id, 'slug' => str_slug($menu->title)])}}">
                                                <h6 class="mb-0" style="margin-bottom:5px">{{$menu->title}}</h6>
                                                <span class="caption text-muted">{{str_limit($menu->description, 15)}}</span>
                                            </a> 
                                        </div>
                                        <div class="col-sm-6 text-sm-right menu-amount-wrapper">
                                            <div class="cart-add-wrapper" style="display: inline-block;" data-menu-amount="{{$menu->amount}}" data-menu-id="{{$menu->id}}" data-href="{{url(route('add.to.cart'))}}" data-delete-href="{{url(route('remove.menu.from.cart'))}}">
                                                <span class="text-md mr-4"> &#8358;{{$menu->amount}}</span>
                                                <button class="btn btn-outline-secondary btn-sm {{in_array($menu->id, $cart_menu_ids) ? 'added-cart' : 'add-cart'}}" ><span>{{in_array($menu->id, $cart_menu_ids) ? 'added' : 'add'}} to cart</span></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @empty

                                @endforelse
                            </div>
                        </div>
                        @empty

                        @endforelse
                    </div>
                </div>
            </div>
        </div>

    </div>
<!-- Content / End -->


<!-- Panel Cart -->
    <div id="panel-cart">
        <div class="panel-cart-container">
            <div class="panel-cart-title">
                <h5 class="title">Your Cart</h5>
                <button class="close" data-toggle="panel-cart"><i class="glyphicon glyphicon-remove"></i></button>
            </div>
            <div class="panel-cart-content">
                <table class="table-cart">
                    <tbody>
                    @if(\Cart::totalItems(true) > 0)
                        @foreach(\Cart::contents() as $cart_menu)
                            <tr data-menu-id="{{$cart_menu->id}}">
                            <td class="title"><input type="hidden" name="menu[{{$cart_menu->id}}]" value="1">
                                    <span class="name"><a href="#productModal" data-toggle="modal">{{$cart_menu->name}}</a></span>
                                    <span class="caption text-muted">Unit Price :₦  {{$cart_menu->price}}</span>
                                    </td>
                                    <td class="price" data-price="{{$cart_menu->price}}" data-quantity="{{$cart_menu->quantity}}" style="padding:0">₦ {{$cart_menu->total()}}</td>
                                    <td class="actions">
                                        <a data-target="#productModal" data-toggle="modal" class="action-icon edit"><i class="glyphicon glyphicon-pencil"></i></a> &nbsp;&nbsp;&nbsp;
                                        <a href="#" class="action-icon delete" data-menu-id="{{$cart_menu->id}}"><i class="glyphicon glyphicon-remove"></i></a>
                                    </td>
                                </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                <div class="cart-summary">
                    <div class="row">
                        <div class="col-sm-7 text-right text-muted">Order total:</div>
                        <div class="col-sm-5 amount-total"><strong>₦ {{\Cart::total(true)}}</strong></div>
                    </div>
                   <!--  <div class="row">
                        <div class="col-sm-7 text-right text-muted">Devliery:</div>
                        <div class="col-sm-5"><strong>₦ </strong></div>
                    </div> -->
                    <hr class="hr-sm">
                    <div class="row text-lg">
                        <div class="col-sm-7 text-right text-muted">Total:</div>
                        <div class="col-sm-5 amount-paid"><strong>₦ {{\Cart::total(true)}}</strong></div>
                    </div>
                </div>
            </div>
        </div>
        <a href="{{route('order.checkout')}}" class="panel-cart-action btn btn-secondary btn-block btn-lg"><span>Go to checkout</span></a>
    </div>

<!-- product Modal -->
<div class="modal fade" id="productModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-lg dark bg-dark">
                <div class="bg-image"><img src="{{url('images/menu-title-drinks.jpg')}}" alt=""></div>
                <h4 class="modal-title">Specify your dish</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ti-close"></i></button>
            </div>
            <div class="modal-product-details">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <h6 class="mb-0">Boscaiola Pasta</h6>
                    </div>
                    <div class="col-sm-3 text-center">
                    	<div class="form-group">
                    		<input type="number" style="border: 1px solid" placeholder="1" data-edit-href="{{url(route('update.menu.qty.cart'))}}">
                    	</div>
                    </div>
                    <div class="col-sm-3 text-lg text-right">$9.00</div>
                </div>
            </div>
            <button type="button" class="modal-btn btn btn-secondary btn-block btn-lg" data-dismiss="modal"><span>Add to Cart</span></button>
        </div>
    </div>
</div>

@endsection