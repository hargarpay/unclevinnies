@extends('layouts.main')

@section('banner')
<!--Page header & Title-->
<section id="page_header" style="background: url('{{url('/build/images/banner-bg-4.jpg')}}') no-repeat">
<div class="page_title">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
         <h2 class="title">Gallery</h2>
         <!-- <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit</p> -->
      </div>
    </div>
  </div>
</div>  
</section>
@endsection

@section('content')
<section id="gallery" class="padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="work-filter">
          <ul class="text-center">
             <li><a href="javascript:;" data-filter="all" class="active filter">All</a></li>
             @if($categories->count() > 0)
	             @foreach($categories as $category)
	             <li><a href="javascript:;" data-filter=".{{$category->class}}" class="filter"> {{$category->name}}</a></li>
	             @endforeach
             @endif
          </ul>
        </div>
      </div>
    </div>
     <div class="row">
      <div class="zerogrid">
        <div class="wrap-container">
          <div class="wrap-content clearfix">
          @if($menus->count() > 0)
          	@foreach($menus as $menu)
	            <div class="col-1-3 mix work-item {{$menu->category->class}} heading_space">
	              <div class="wrap-col">
	                <div class="item-container">
	                  <div class="image">
	                    <img src="{{url($menu->thumbnail)}}" alt="cook"/>
	                    <div class="overlay">
	                        <a class="fancybox overlay-inner" href="{{url($menu->thumbnail)}}" data-fancybox-group="gallery"><i class=" icon-eye6"></i></a>
	                    </div>
	                  </div> 
	                  <div class="gallery_content">
	                    <h3><a href="{{route('view.single.menu', ['id'=>$menu->id, 'slug' => str_slug($menu->title)])}}">{{$menu->title}}</a></h3>
	                    <p>{!!substr(nl2br($menu->description), 0, 20)!!}</p>
	                  </div>
	                </div>
	              </div>
	            </div>
            @endforeach
          @endif
          </div>
        </div>
       </div>
      </div>
  </div>
</section>
@endsection