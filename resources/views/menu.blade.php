@extends('layouts.main')

<!--Page header & Title-->
@section('banner')
<section id="page_header" style="background: url('{{url('/build/images/banner-bg-3.jpg')}}') no-repeat">
<div class="page_title">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
         <h2 class="title">Menu</h2>
         <!-- <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit</p> -->
      </div>
    </div>
  </div>
</div>  
</section>
@endsection

<!--Pricings-->
@section('content')
<section id="pricing" class="padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 class="heading">Our menu</h2>
        <hr class="heading_space">
      </div>
      <div class="col-sm-3 dashboard">
      	<div class="panel-group" id="accordion">
	        <div class="panel panel-warning">
	            <div class="panel-heading">
	                <h4 class="panel-title">
	                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-plus"></span> Category Menu </a>
	                </h4>
	            </div>
	            <div id="collapseOne" class="panel-collapse collapse in">
	                <div class="panel-body">
	                    <div class="list-group category-menu">
	                    	@forelse($categories as $category)
		                        <a href="javascript:;" class="list-group-item filter" data-filter=".{{$category->class}}" data-menu-class="{{$category->class}}">
		                          {{$category->name}}
		                        </a>
	                        @empty

	                        @endforelse
	                    </div> 
	                </div>
	            </div>
	        </div>
	    </div>
      </div>
      <div class="col-sm-8">
        <div class="price padding-bottom menu-list">
        @forelse($categories as $category)
	          <div class="price_body mix {{$category->class}}" style="width:100%">
	             <ul class="pricing_feature">
        		@forelse(getMenuByCategoryID($category->id) as $menu)
	        		@if($menu->published == 1)
		              <li><a href="{{route('view.single.menu', ['id'=>$menu->id, 'slug' => str_slug($menu->title)])}}"><img src="{{url($menu->thumbnail)}}" style="height: 50px">&nbsp;&nbsp;&nbsp;&nbsp; {{$menu->title}}  <strong>&#8358;{{$menu->amount}}</strong> </a></li>
		            @endif
	            @empty

	    		@endforelse 
	            </ul>
	          </div>
        @empty

	    @endforelse  
        </div>
      </div>
    </div>
  </div>
</section>
@endsection