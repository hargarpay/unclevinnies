@extends('dashboard.layouts.main')

@section('dashboard-content')
<table  class="table table-condensed table-bordered">
	<thead>
		<tr class="warning">
			<th>Title</th>
			<th>Description</th>
			<th>Banner</th>
			<th>Tab Title</th>
			<th>Tab Description</th>
			<th colspan="3" class="text-center">ACTIONS</th>
		</tr>
	</thead>
	<tbody>
		@forelse($sliders as $slider)
		<tr>
			<td>{{$slider->small_title}}<br>
				{{$slider->major_title.' '.$slider->color_title}}
			</td>
			<td>
				{{substr($slider->description, 0, 30)}}
			</td>
			<td>
				<img src="{{url($slider->banner)}}" class="img-responsive img-rounded" style="max-width:200px; max-height: 100px">
			</td>
			<td>
				{{$slider->tab_title}}
			</td>
			<td>
				{{$slider->tab_description}}
			</td>
			<td>
				@if($slider->published == 0)
				<button type="button" class="btn btn-warning" data-url="{{route('publish.slider', ['id' => $slider->id])}}" data-class="btn btn-warning" data-title="PUBLISH" data-toggle="modal" data-target="#modalStatus"> PUBLISH </button>
				@else
				<button type="button" class="btn" data-url="{{route('draft.slider', ['id' => $slider->id])}}" data-class="btn" data-title="DRAFT" data-toggle="modal" data-target="#modalStatus"> DRAFT </button>
				@endif
			</td>
			<td>
				<button type="button" class="btn btn-primary" data-url="{{route('edit.slider', ['id' => $slider->id])}}" data-class="btn btn-primary" data-title="EDIT" data-toggle="modal" data-target="#editModal"> EDIT </button>
			</td>
			<td>
				<button type="button" class="btn btn-danger" data-url="{{route('delete.slider', ['id' => $slider->id])}}" data-class="btn btn-danger" data-title="DELETE" data-toggle="modal" data-target="#deleteModal"> DELETE </button>
			</td>			
		</tr>
		@empty
		<tr class="info">
			<td colspan="8">
				<span class="text-info"><i class="fa fa-fw fa-exclamation-triangle"></i> No Transactions</span>  
			</td>
		</tr>
		@endforelse
	</tbody>
	<tfoot>
		<tr class="warning">
			<td colspan="8">{{$sliders->render()}}</td>
		</tr>
	</tfoot>
</table>
@endsection