@extends('dashboard.layouts.main')

@section('dashboard-content')
<div class="panel panel-warning margin-top">
	<div class="panel-heading"><h2>Create Slider</h2></div>
	<div class="panel-body">
		@include('partials.form-errors')
		<form action="" method="post" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{csrf_token()}}">

		    <div class="row">
		    	<div class="col-xs-12">
			    	<div class="form-group" style="margin-bottom:0px;">
			    		<label>Banner Title</label>
			    	</div>
		    	</div>
		        <div class="col-sm-4">
			        <div class="form-group">
			            <input type="text" class="form-control" name="small_title" value="{{old('small_title')}}">
			            <span class="help-block">First Row Title</span>
			        </div>
		        </div>
		        <div class="col-sm-5">
			        <div class="form-group">
			            <input type="text" class="form-control" name="major_title" value="{{old('major_title')}}">
			            <span class="help-block">Second Row Title</span>
			        </div>
		        </div>
		        <div class="col-sm-3">
			        <div class="form-group">
			            <input type="text" class="form-control" name="color_title" value="{{old('color_title')}}">
			            <span class="help-block">Colored Text Title</span>
			        </div>
		        </div>
		    </div>
		    <div class="row">
		        <div class="col-sm-12">
		        	<div class="form-group">
	                    <label for="">Image preview</label>
	                    <div class="help-block image-preview">
	                        <img src="http://via.placeholder.com/1920x1098" width="100%" alt="" class="img-resposive img-rounded" />
	                    </div>
	                     <input type="file" name="banner" class="fileUpload">

	                 </div>
		        </div>
		    </div>
		    <div class="row">
		    	<div class="col-xs-12">
		    	<div class="form-group">
		    		<label>Banner Description</label>
		    		<textarea class="form-control" name="description" placeholder="Banner Description">{{old('description')}}</textarea>
		    	</div>
		    	
		    	</div>
		    </div>
		    <div class="row">
		    	<div class="col-sm-4">
		    		<div class="form-group">
		    			<label for="">Tab Title</label>
		    			<input type="text" name="tab_title" class="form-control" value="{{old('tab_title')}}">
		    		</div>
		    	</div>
		    	<div class="col-sm-8">
		    		<div class="form-group">
		    			<label for="">Tab Description</label>
		    			<input type="text" name="tab_description" class="form-control" value="{{old('tab_description')}}">
		    		</div>
		    	</div>
		    </div>
		     <div class="row">
			        <div class="col-sm-4">
			    		<div class="form-group">
			    		<label>Banner Description Position</label>
				            <select class="form-control" name="position">
				            <option> Select Position</option>
				                @foreach($positions as $key => $position)
								<option value="{{$key}}" {{old('position') == $key ? 'selected="selected"' : ''}}>{{$position}}</option>
				               @endforeach
				            </select>
			           </div>
			        </div>
			        <div class="col-sm-8">
			        	<div class="form-group">
			        		<label style="display:block;">Action on the Banner</label>
			        		@foreach($actions as $key => $action)
				        		<div class="multiple-select-button disable-select @if(!empty(old('actions'))) {{in_array($key, old('actions')) ? 'active' : ''}} @endif">
				        			{{$action}}
				        			<input type="checkbox" name="actions[]" value="{{$key}}">
				        		</div>
			        		@endforeach
			            </div>
			        </div>
		    </div>
		    <div class="row">
		    	<div class="col-xs-12">
		    		<button type="submit" class="btn btn-orange btn-block" style="color: white; font-weight: bold;">Create Slider</button>
		    	</div>
		    </div>
		</form>
	</div>
</div>
@endsection