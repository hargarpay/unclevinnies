@extends('layouts.main')
@section('banner')
<section id="page_header" style="background: url('{{url('/build/images/banner-bg-9.jpg')}}') no-repeat">
<div class="page_title">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
         <h2 class="title">Checkout</h2>
         <!-- <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit</p> -->
      </div>
    </div>
  </div>
</div>  
</section>
@endsection
@section('content')
 <!-- Content -->
    <!-- Section -->
        <section class="section bg-light">

            <div class="container">
                <div class="row">
                    <div class="col-xl-4 push-xl-8 col-lg-5 push-lg-7">
                        <div class="shadow bg-white stick-to-content mb-4">
                            <div class="bg-dark dark p-4"><h5 class="mb-0" style="padding:20px" id="your-order" data-delete-href ="{{url(route('remove.menu.from.cart'))}}" >You order</h5></div>

                            <table class="table-cart">
                                <tbody>
                                @if(\Cart::totalItems(true) > 0)
                                    @foreach(\Cart::contents() as $cart_menu)
                                        <tr data-menu-id="{{$cart_menu->id}}">
                                        <td class="title"><input type="hidden" name="menu[{{$cart_menu->id}}]" value="1">
                                                <div class="img" style="display:inline-block;" > <img src="{{url($cart_menu->image)}}" height="30" style="border-width:0; height: 30px" >  </div>
                                                <div style="float:right;width:75%;" >
                                                <span class="name"><a href="#productModal" data-toggle="modal">{{$cart_menu->quantity}} x {{$cart_menu->name}}</a></span>
                                                <span class="caption text-muted">Unit Price :₦  {{$cart_menu->price}}</span>
                                                </div>
                                                </td>
                                                <td class="price" data-price="{{$cart_menu->price}}" data-quantity="{{$cart_menu->quantity}}" style="padding:0">₦ {{$cart_menu->total()}}</td>
                                                <td class="actions">
                                                    <a data-target="#productModal" data-toggle="modal" class="action-icon edit"><i class="glyphicon glyphicon-pencil"></i></a> &nbsp;&nbsp;&nbsp;
                                                    <a href="#" class="action-icon delete" data-menu-id="{{$cart_menu->id}}"><i class="glyphicon glyphicon-remove"></i></a>
                                                </td>
                                            </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                            <div class="cart-summary">
                                <div class="row">
                                    <div class="col-sm-7 text-right text-muted">Order total:</div>
                                    <div class="col-sm-5 amount-total"><strong>₦ {{\Cart::total(true)}}</strong></div>
                                </div>
                               <!--  <div class="row">
                                    <div class="col-sm-7 text-right text-muted">Devliery:</div>
                                    <div class="col-sm-5"><strong>₦ </strong></div>
                                </div> -->
                                <hr class="hr-sm">
                                <div class="row text-lg">
                                    <div class="col-sm-7 text-right text-muted">Total:</div>
                                    <div class="col-sm-5 amount-paid"><strong>₦ {{\Cart::total(true)}}</strong></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-8 pull-xl-4 col-lg-7 pull-lg-5 margin-top" >

                    <form action="" method="post">
                    {{csrf_field()}}
                    <input type="hidden" name="items" value="{{\Cart::totalItems(true)}}">
                        <div class="bg-white p-4 p-md-5 mb-4" style="padding:20px;margin-bottom: 15px;">
                            <h4 class="border-bottom pb-4"><i class="ti ti-user mr-3 text-primary"></i>Basic information</h4>
                            @include('partials.form-errors')
                            @if(auth()->check())
                                <input type="hidden" name="fullname" value="{{auth()->user()->fullname}}">
                                <input type="hidden" name="address" value="{{auth()->user()->address}}">
                                <input type="hidden" name="phone" value="{{auth()->user()->phone}}">
                                <input type="hidden" name="email" value="{{auth()->user()->email}}">

                            @else
                            <div class="row mb-5">
                                <div class="form-group col-sm-12">
                                    <label>Fullname:</label>
                                    <input type="text" class="form-control" name="fullname" value="{{old('fullname')}}">
                                </div>
                                <div class="form-group col-sm-12">
                                    <label>Address:</label>
                                    <input type="text" class="form-control" name="address" value="{{old('address')}}">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label>Phone number:</label>
                                    <input type="number" class="form-control" name="phone" value="{{old('phone')}}">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label>E-mail address:</label>
                                    <input type="email" class="form-control" name="email" value="{{old('email')}}">
                                </div>
                            </div>
                            @endif
                            <input type="hidden" name="ref_no" value="{{getUniqueRef()}}">

                            <!-- <h4 class="border-bottom pb-4"><i class="ti ti-package mr-3 text-primary"></i>Delivery</h4>
                            <div class="row mb-5">
                                <div class="form-group col-sm-6">
                                    <label>Delivery time:</label>
                                    <div class="select-container">
                                        <select class="form-control" name="delivery_time">
                                            <option>As fast as possible</option>
                                            <option>In one hour</option>
                                            <option>In two hours</option>
                                        </select>
                                    </div>
                                </div>
                            </div> -->

                            <h4 class="border-bottom pb-4"><i class="ti ti-wallet mr-3 text-primary"></i>Payment</h4>
                            <div class="row text-lg">
                                <!-- <div class="col-md-4 col-sm-6 form-group">
                                    <label class="custom-control custom-radio">
                                        <input type="radio" name="payment_type" class="custom-control-input" value="PayPal">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">PayPal</span>
                                    </label>
                                </div> -->
                                <div class="col-md-4 col-sm-6 form-group">
                                    <label class="custom-control custom-radio">
                                        <input type="radio" name="payment_type" class="custom-control-input" value="Cash" @if(empty(old('payment_type'))) checked="checked" @elseif(old('payment_type') == 'Cash') checked="checked" @endif >
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">Cash</span>
                                    </label>
                                </div>
                                <div class="col-md-6 col-sm-6 form-group">
                                    <label class="custom-control custom-radio">
                                        <input type="radio" name="payment_type" class="custom-control-input" value="paystack" @if(old('payment_type') == 'paystack') checked="checked" @endif>
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">Credit or Debit Card</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary btn-lg"><span>Order now!</span></button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>

        </section> 
<!-- Content / End -->


<!-- Panel Cart -->
    <div id="panel-cart">
        <div class="panel-cart-container">
            <div class="panel-cart-title">
                <h5 class="title">Your Cart</h5>
                <button class="close" data-toggle="panel-cart"><i class="glyphicon glyphicon-remove"></i></button>
            </div>
            <div class="panel-cart-content">
                <table class="table-cart">
                    <tbody>
                    @if(\Cart::totalItems(true) > 0)
                        @foreach(\Cart::contents() as $cart_menu)
                            <tr data-menu-id="{{$cart_menu->id}}">
                            <td class="title"><input type="hidden" name="menu[{{$cart_menu->id}}]" value="1">
                                    <span class="name"><a href="#productModal" data-toggle="modal">{{$cart_menu->name}}</a></span>
                                    <span class="caption text-muted">Unit Price :₦  {{$cart_menu->price}}</span>
                                    </td>
                                    <td class="price" data-price="{{$cart_menu->price}}" data-quantity="{{$cart_menu->quantity}}" style="padding:0">₦ {{$cart_menu->total()}}</td>
                                    <td class="actions">
                                        <a data-target="#productModal" data-toggle="modal" class="action-icon edit"><i class="glyphicon glyphicon-pencil"></i></a> &nbsp;&nbsp;&nbsp;
                                        <a href="#" class="action-icon delete" data-menu-id="{{$cart_menu->id}}"><i class="glyphicon glyphicon-remove"></i></a>
                                    </td>
                                </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                <div class="cart-summary">
                    <div class="row">
                        <div class="col-sm-7 text-right text-muted">Order total:</div>
                        <div class="col-sm-5 amount-total"><strong>₦ {{\Cart::total(true)}}</strong></div>
                    </div>
                   <!--  <div class="row">
                        <div class="col-sm-7 text-right text-muted">Devliery:</div>
                        <div class="col-sm-5"><strong>₦ </strong></div>
                    </div> -->
                    <hr class="hr-sm">
                    <div class="row text-lg">
                        <div class="col-sm-7 text-right text-muted">Total:</div>
                        <div class="col-sm-5 amount-paid"><strong>₦ {{\Cart::total(true)}}</strong></div>
                    </div>
                </div>
            </div>
        </div>
        <a href="checkout.html" class="panel-cart-action btn btn-secondary btn-block btn-lg"><span>Go to checkout</span></a>
    </div>

<!-- product Modal -->
<div class="modal fade" id="productModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-lg dark bg-dark">
                <div class="bg-image"><img src="{{url('images/menu-title-drinks.jpg')}}" alt=""></div>
                <h4 class="modal-title">Specify your dish</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ti-close"></i></button>
            </div>
            <div class="modal-product-details">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <h6 class="mb-0">Boscaiola Pasta</h6>
                    </div>
                    <div class="col-sm-3 text-center">
                    	<div class="form-group">
                    		<input type="number" style="border: 1px solid" placeholder="1" data-edit-href="{{url(route('update.menu.qty.cart'))}}">
                    	</div>
                    </div>
                    <div class="col-sm-3 text-lg text-right">$9.00</div>
                </div>
            </div>
            <button type="button" class="modal-btn btn btn-secondary btn-block btn-lg" data-dismiss="modal"><span>Add to Cart</span></button>
        </div>
    </div>
</div>

@endsection