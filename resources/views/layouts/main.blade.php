<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>{{ isset($title) ? $title.' : ' : ''}}Uncle Vinnies  ...taste is in the grill</title>
<meta name="csrf_token" content="{{ csrf_token() }}">
@if(Request::is('blogs/view/*'))
<meta name="twitter:card" content="summary"><!-- 
<meta name="twitter:site" content="@InnerCityHQ">
<meta name="twitter:creator" content="@InnerCityHQ"> -->
<meta name="twitter:title" content="{{$blog->title}}">
<meta name="twitter:description" content="{{str_limit(str_replace(['&lt;p&gt;', '&lt;/p&gt;'], ['',''],$blog->description), 50)}}">
<meta property="og:title" content="{{$blog->title}}">
<meta property="og:type" content="website">
<meta property="og:image:width" content="600">
<meta property="og:image:height" content="400">
<meta property="og:url" content="{{route('view.single.blog', ['id' => $blog->id, 'slug' => str_slug($blog->title)])}}">
<meta property="og:site_name" content="Uncle Vinnies">
<meta property="og:description" content="{{str_limit(str_replace(['&lt;p&gt;', '&lt;/p&gt;'], ['',''],$blog->description), 50)}}">
<meta name="twitter:image" content="{{url(json_decode($blog->placeholder, true)[0])}}">
  @if($blog->type != 'video_url')
  <link rel="canonical" href="{{url(json_decode($blog->placeholder, true)[0])}}">
  <meta property="og:image" content="{{url(json_decode($blog->placeholder, true)[0])}}">
  <meta property="fb:app_id" content="187921615099936">
  @endif
@endif
<link rel="stylesheet" type="text/css" href="{{elixir('css/all.css')}}">
@if(Request::is(str_replace(env('URL_SLASH'), '',route('order.page'))) || Request::is(str_replace(env('URL_SLASH'), '',route('order.checkout'))) || Request::is('menu/*'))
<link rel="stylesheet" type="text/css" href="{{elixir('css/button.css')}}">
@endif

@if(Request::is('dashboard/blog/*'))
  <script type="text/javascript" src="{{elixir('js/tinymce.min.js')}}"></script>
@endif
<link rel="shortcut icon" href="{{url('/images/uv-logo-white.png')}}" type="image/x-icon">
  <link rel="icon" href="{{url('/images/uv-logo-white.png')}}" type="image/x-icon">
  <link rel="shortcut icon" href="{{url('images/uv-logo-white.png')}}">
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
<body>
@if(Request::is('blogs/view/*'))
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '187921615099936',
      xfbml      : true,
      version    : 'v2.6'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
@endif
<!--Loader-->
<div class="loader"> 
   <div class="cssload-container">
     <div class="cssload-circle"></div>
     <div class="cssload-circle"></div>
   </div>
</div>

<!--Topbar-->
<div class="topbar">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <p class="pull-left hidden-xs">Uncle Vinnies ...taste is in the grill</p>
        <p class="pull-right"><i class="fa fa-phone"></i>Order Online 09060009830 | 014546999</p>
      </div>
    </div>
  </div>
</div>

<!--Header-->
<header id="main-navigation">
  <div id="navigation" data-spy="affix" data-offset-top="20"
  @if(!Request::is('/') && !Request::is(str_replace(env('URL_SLASH'), '',route('gallery'))) && !Request::is(str_replace(env('URL_SLASH'), '',route('about-us'))) && !Request::is(str_replace(env('URL_SLASH'), '',route('menus'))) && !Request::is(str_replace(env('URL_SLASH'), '',route('order.page'))) && !Request::is(str_replace(env('URL_SLASH'), '',route('location'))) && !Request::is('blog/*') && !Request::is('order/*') && !Request::is('menu/*') && !Request::is(str_replace(env('URL_SLASH'), '',route('view.blogs'))) && !Request::is('blogs/*'))
      style="background-color: rgba(0, 0, 0, 0.4);"
  @endif
  >
    <div class="container">
      <div class="row">
      <div class="col-md-12">
        <nav class="navbar navbar-default">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#fixed-collapse-navbar" aria-expanded="false"> 
            <span class="icon-bar top-bar"></span> <span class="icon-bar middle-bar"></span> <span class="icon-bar bottom-bar"></span> 
            </button>
           <a class="navbar-brand" href="/"><img src="{{url('images/uv-logo-white.png')}}" alt="logo" class="img-responsive"></a> 
         </div>
        
            <div id="fixed-collapse-navbar" class="navbar-collapse collapse navbar-right">
              <ul class="nav navbar-nav">
                <li>
                   <a href="/" >Home</a>
                </li>
                <li><a href="{{route('food')}}">Our Food</a></li>
                
                <li class="dropdown">
                  <a data-toggle="dropdown" href="#" class="dropdown-toggle">pages</a>
                  <ul class="dropdown-menu">
                    <li><a href="{{route('about-us')}}">About Us</a></li>
                    <li><a href="{{route('menus')}}">Menu</a></li>
                    <li><a href="{{route('gallery')}}">Gallery</a></li>
                    <li><a href="{{route('location')}}">Location V1</a></li>
                  </ul>
                 </li>
                <li><a href="{{route('view.blogs')}}">blog</a></li>
                <li><a href="{{route('order.page')}}">Order Now</a></li>
                <li class="dropdown">
                  <a data-toggle="dropdown" href="#" class="dropdown-toggle">{{auth()->check() == true ? 'Logout' : 'Login'}}</a>
                  <ul class="dropdown-menu">
                       @if(auth()->check())
                        <li><a href="{{url('/logout')}}">Log Out</a></li>

                        @if(auth()->user()->admin == 1)
                          <li><a href="{{route('view.slider')}}">Admin Dashboard</a></li>
                        @endif
                      @else
                        <li><a href="{{url('/login')}}">log In</a></li>
                      @endif
                  </ul>
                </li>
               
              </ul>
            </div>
         </nav>
         </div>
       </div>
     </div>
  </div>
</header>
@if (Session::has('flash_notification'))
    <div style="position:absolute; margin-top: 10px; left: 20%; right: 20%; z-index:9999999;" class="notification-wrapper">
        @include('flash::message')
    </div>
@endif
@yield('banner')

@yield('content')

<!--Footer-->
<footer class="padding-top bg_black" id="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-6 footer_column">
        <h4 class="heading">Why Unclevinnies?</h4>
        <hr class="half_space">
        <p class="half_space">We fully believe that the taste trip your buds are looking for is in our food. Also, great food+ great ambience+ great customer service= a very happy uncle vinnies customer.</p>
        <p>Come over and prove us right!</p>
      </div>
      <div class="col-md-3 col-sm-6 footer_column">
        <h4 class="heading">Quick Links</h4>
        <hr class="half_space">
        <ul class="widget_links">
          <li><a href="{{url('/')}}">Home</a></li>
          <li><a href="{{route('view.blogs')}}">Blog</a></li>
          <li><a href="{{route('about-us')}}">About Us</a></li>
          <li><a href="{{route('location')}}">Locations</a></li>
          <li><a href="{{route('menus')}}">Menu</a></li>
          <!-- <li><a href="faq.html">Faq's</a></li> -->
          <li><a href="{{route('order.page')}}">Order Now</a></li>
          <li><a href="{{route('gallery')}}">Gallery</a></li>
          <li><a href="{{route('food')}}">Food</a></li>
        </ul>
      </div>
      <div class="col-md-3 col-sm-6 footer_column">
        <h4 class="heading">Newsletter</h4>
        <hr class="half_space">
        <p class="icon" style="font-size:15px; width:100%"><i class="icon-dollar"></i>Sign up with your name and email to get updates fresh updates.</p>
        <div id="result1" class="text-center"></div>
        <p class="icon" style="font-size:15px; width:100%">Coming Soon</p>      
        
       <!-- <form action="" method="get" onSubmit="return false" class="newsletter">
          <div class="form-group">
            <input type="email" placeholder="E-mail Address" required name="EMAIL" id="EMAIL" class="form-control" />
          </div>
          <div class="form-group">
            <input type="submit" class="btn-submit button3" value="Subscribe" />
          </div>
        </form>
      -->
      </div> 
      <div class="col-md-3 col-sm-6 footer_column">
        <h4 class="heading">Get in Touch</h4>
        <hr class="half_space">
        <p> It's more than just food. We make memorable moments at</p>
        <p class="address"><i class="icon-location"></i>The Ndubuisi Kanu Park, Ikeja</p>
        <p class="address"><i class="icon-location"></i>VGC Park, Victoria Garden City, Lekki</p>
        <p class="address"><i class="icon-location"></i>Rose Park, LSDPC Estate Ogba</p>
        <p class="address"><i class="fa fa-phone"></i>09060009830 | 014546999</p>
        <p class="address"><i class="icon-dollar"></i><a href="mailto:info@unclevinnies.ng">info@unclevinnies.ng</a></p>
      </div> 
    </div>
    <div class="row">
     <div class="col-md-12">
        <div class="copyright clearfix">
          <p>Copyright &copy; {{date('Y')}} Uclevinnies. All Right Reserved</p>
          <ul class="social_icon">
               <li><a href="#" class="facebook"><i class="icon-facebook5"></i></a></li>
               <li><a href="#" class="twitter"><i class="icon-twitter4"></i></a></li>
               <li><a href="#" class="google"><i class="icon-google"></i></a></li>
              </ul>
        </div>
      </div>
    </div>
  </div>
</footer>

<a href="#" id="back-top"><i class="fa fa-angle-up fa-2x"></i></a>
@if(Request::is(str_replace(env('URL_SLASH'), '',route('location'))))
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAOBKD6V47-g_3opmidcmFapb3kSNAR70U&amp;libraries=places"></script>
@endif
<script type="text/javascript" src="{{elixir('js/all.js')}}"></script>

@if(Request::is(str_replace(env('URL_SLASH'), '',route('order.page'))) || Request::is(str_replace(env('URL_SLASH'), '',route('order.checkout'))) || Request::is('menu/*'))
<script type="text/javascript" src="{{elixir('js/core.js')}}"></script>
@endif

@if(Request::is('blogs/view/*'))
<script type="text/javascript">
$('#share_button').click(function(e){
e.preventDefault();
var $this = $(this).data('href');
  FB.ui(
  {
    method: 'share',
    href: $this,
  });
});
</script>
@endif
</body>
</html>
