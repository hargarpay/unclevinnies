<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKitchenChefsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kitchen_chefs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('kitchen_rank_id')->unsigned();
            $table->foreign('kitchen_rank_id')->references('id')->on('kitchen_ranks')->onDelete('cascade');
            $table->integer('kitchen_position_id')->unsigned();
            $table->foreign('kitchen_position_id')->references('id')->on('kitchen_positions')->onDelete('cascade');
            $table->string('excerpt');
            $table->string('thumbnail');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kitchen_chefs');
    }
}
