<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sliders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('banner');
            $table->string('small_title');
            $table->string('major_title');
            $table->string('color_title')->nullable();
            $table->string('description');
            $table->boolean('published')->default(0);
            $table->string('tab_title');
            $table->string('tab_description');
            $table->text('actions');
            $table->enum('position', ['center', 'left', 'right']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sliders');
    }
}
