<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model
{
    protected $table = 'blog_categories';

    protected $fillable = ['category', 'banner'];

     public function blogs(){
    	return $this->belongsToMany('\App\Blog', 'blogs_categories_pivot', 'category_id', 'blog_id')->withTimestamps();
    }
}
