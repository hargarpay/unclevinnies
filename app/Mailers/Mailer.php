<?php
namespace App\Mailers;

use Illuminate\Mail\Mailer as Mail;

abstract class Mailer{

	private $mailer;

	public function __construct(Mail $mailer){
		$this->mailer = $mailer;
	}

	public function sendTo($user, $view, $subject, $data = []){

		$this->mailer->queue($view, $data, function($message) use ($user, $subject){

					$message->to($user->email)->subject($subject);
					$message->to('info@unclevinnies.ng')->subject($subject);

		});
	}
}
?>