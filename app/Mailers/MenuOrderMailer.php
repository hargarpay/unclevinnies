<?php
namespace App\Mailers;

use App\Mailers\Mailer;

class MenuOrderMailer extends Mailer{
	
	public function sendMenuOrder($user,$items, $grand_total, $latest_menu){

		$subject = "Order Menu";

		$view = 'emails.menuorder';

		$data = compact('user', 'items', 'grand_total', 'latest_menu');

		return $this->sendTo($user, $view, $subject, $data);

	}
}


 ?>