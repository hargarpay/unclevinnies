<?php
namespace App\Mailers;

use App\Mailers\Mailer;

class RegisterMailer extends Mailer{
	
	public function sendRegister($user, $promo_menu, $latest_menu){

		$subject = "Registration Information";

		$view = 'emails.register';

		$data = compact('user', 'promo_menu', 'latest_menu');

		return $this->sendTo($user, $view, $subject, $data);

	}
}


 ?>