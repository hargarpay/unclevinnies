<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = 'blogs';

    protected $fillable = ['title', 'type', 'description', 'placeholder'];

    public static $rules = [
        'name' => 'required',
        'blog_type' => ['required', 'regex:/(image|video_url|slider)/', 'blog_type_placeholder'],
        'articleContent' => 'required|min:20',
        'tags' => 'required|array',
        'categories' => 'required|array',
        'image' => 'sometimes|allowexts:jpeg,jpg,png,gif|image_dimension:min_width=900,min_height=600',
        'slider' => 'sometimes|array|min_array:2|allowexts:jpeg,jpg,png,gif|image_dimension:min_width=900,min_height=600',
        'video' => 'sometimes|url'

    ];

    public function tags(){
    	return $this->belongsToMany('\App\BlogTag', 'blogs_tags_pivot', 'blog_id', 'tag_id')->withTimestamps();
    }

    public function comments(){
    	return $this->hasMany('\App\Comment', 'blog_id');
    }

    public function categories(){
    	return $this->belongsToMany('\App\BlogCategory', 'blogs_categories_pivot', 'blog_id', 'category_id')->withTimestamps();
    }

    public function users(){
        return $this->belongsTo('App\Users', 'user_id');
    }
}
