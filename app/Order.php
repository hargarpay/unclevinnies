<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    protected $fillable = ['items', 'user_id', 'ref_no', 'payment_channel'];

    public static $rules = [
    'fullname' => 'required',
    'email' => 'required|email',
    'address' => 'required',
    // 'phone' => 'required',
    'items' =>'required|integer|min:1',
    'ref_no' => 'required'
    ];
}
