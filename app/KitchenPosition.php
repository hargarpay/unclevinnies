<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KitchenPosition extends Model
{
    protected $table = 'kitchen_positions';

    protected $fillable = ['name'];
}
