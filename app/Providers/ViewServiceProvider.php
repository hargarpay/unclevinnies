<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repositories\SliderRepositoryInterface;

use App\Repositories\CategoryRepositoryInterface;

use App\Repositories\MenuRepositoryInterface;

use App\Repositories\KitchenPositionRepositoryInterface;

use App\Repositories\KitchenRankRepositoryInterface;

use App\Repositories\KitchenChefRepositoryInterface;

use App\BlogTag;

use App\BlogCategory;

use App\Blog;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(SliderRepositoryInterface $slider, CategoryRepositoryInterface $category, MenuRepositoryInterface $menu, KitchenPositionRepositoryInterface $kitchen_position,
        KitchenRankRepositoryInterface $kitchen_rank, KitchenChefRepositoryInterface $kitchen_chef)
    {
    
        $slider_share = $slider->getAll();
        $category_share = $category->getAll();
        $menu_share = $menu->getAll();
        $kitchen_position_share = $kitchen_position->getAll();
        $kitchen_rank_share = $kitchen_rank->getAll();
        $kitchen_chef_share = $kitchen_chef->getAll();
        view()->share('slider_share', $slider_share);
        view()->share('monitor', 0);
        view()->share('category_share', $category_share);
        view()->share('menu_share', $menu_share);
        view()->share('kitchen_position_share', $kitchen_position_share);
        view()->share('kitchen_rank_share', $kitchen_rank_share);
        view()->share('kitchen_chef_share', $kitchen_chef_share);
        view()->share('blog_category_share', BlogCategory::all());
        view()->share('blog_tag_share', BlogTag::all());
        view()->share('blog_share', Blog::all());
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
