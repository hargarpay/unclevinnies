<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class BackendServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\SliderRepositoryInterface',
         'App\Repositories\DbSliderRepository'
         );
        $this->app->bind('App\Repositories\CategoryRepositoryInterface',
         'App\Repositories\DbCategoryRepository'
         );
        $this->app->bind('App\Repositories\MenuRepositoryInterface',
         'App\Repositories\DbMenuRepository'
         );
        $this->app->bind('App\Repositories\KitchenRankRepositoryInterface',
         'App\Repositories\DbKitchenRankRepository'
         );
        $this->app->bind('App\Repositories\KitchenChefRepositoryInterface',
         'App\Repositories\DbKitchenChefRepository'
         );
        $this->app->bind('App\Repositories\KitchenPositionRepositoryInterface',
         'App\Repositories\DbKitchenPositionRepository'
         );
    }
}
