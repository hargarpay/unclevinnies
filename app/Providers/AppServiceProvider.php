<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Validator;

use Input;

use App\Blog;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Validator::extend('allowexts', function($attribute, $value, $parameters, $validator){

            if(empty(Input::hasFile($attribute))) return true;

            $count = 0;
            if(is_array(Input::file($attribute))){
                $images = Input::file($attribute);
            }else{
                $images = array(Input::file($attribute));
            }

            foreach ($images as $image) {
                
                $filename = $image->getClientOriginalName();

                if(!in_array($filename, explode(',',Input::get('removeImages')))){

                    $ext = strtolower(substr($filename,strrpos($filename, '.') + 1));

                    if(!in_array($ext, $parameters)){
                        return false;
                    }
                    $count++;
                }

                
            }

            return true;
                


        });



        Validator::extend('min_array', function($attribute, $value, $parameters, $validator){


            $count = 0;

            $images = Input::file($attribute);

            foreach ($images as $image) {
                
                $filename = $image->getClientOriginalName();

                if(!in_array($filename, explode(',',Input::get('removeImages')))){
                    $count++;
                }

                
            }

            return $count >= $parameters[0];
                


        });

        Validator::extend('image_dimension', function($attribute, $value, $parameters, $validator)
                {
                    // dd(Input::file($attribute)[0]->getClientOriginalName());
                    if(empty(Input::hasFile($attribute))) return true;
                    if(is_array(Input::file($attribute))){
                        $images = Input::file($attribute);
                    }else{
                        $images = array(Input::file($attribute));
                    }
                    foreach($images as $image){

                        $file = $image->getRealPath();
                         // dd($file);
                        $image_info = getimagesize($file);

                        $image_width = $image_info[0];
                        $image_height = $image_info[1];

                        foreach ($parameters as $parameter) {
                            $parameter_data = explode('=', $parameter);
                            // dd($parameter_data[1].' '.$image_width);
                            switch ($parameter_data[0]) {
                                case 'width':
                                    $status = ( $parameter_data[1] == $image_width );
                                break;

                                case 'max_width':
                                    $status = ( $parameter_data[1] >= $image_width );
                                break;

                                case 'min_width':
                                    $status = ( $parameter_data[1] <= $image_width );
                                break;

                                case 'height':
                                    $status = ( $parameter_data[1] == $image_height );
                                break;

                                case 'max_height':
                                    $status = ( $parameter_data[1] >= $image_height );
                                break;

                                case 'min_height':
                                    $status = ( $parameter_data[1] <= $image_height );
                                break;
                                
                                default:
                                     $status =  false;
                                break;
                            }

                            if(!$status){
                                return $status;
                            }
                        }
                    }
                    return true;
                });

        Validator::extend('blog_type_placeholder', function($attribute, $value, $parameters, $validator){

            switch ($value) {
                        case 'image':
                        case 'slider':
                               return Input::hasFile($value);
                        break;
                        case 'video_url':
                                return !empty(Input::get($value));
                        break;
                        default:
                                return 'Something is wrong';
                        break;
                    }

        });

        Validator::extend('blog_type_placeholder_sometimes', function($attribute, $value, $parameters, $validator){

            if($value == Blog::findOrFail(Input::get('blog_id'))->type){
                return true;
            }

            switch ($value) {
                        case 'image':
                        case 'slider':
                               return Input::hasFile($value);
                        break;
                        case 'video_url':
                                return !empty(Input::get($value));
                        break;
                        default:
                                return 'Something is wrong';
                        break;
                    }

        });

        Validator::extend('min_slider_image', function($attribute, $value, $parameters, $validator){

            $blog_placeholder = Blog::findOrFail(Input::get($parameters[0]))->placeholder;

            $slider_images_count = count(json_decode($blog_placeholder, true));

            $removed_images_count = count(explode(',', $value));

            // dd('Slider Count: '.$slider_images_count.', Remove Images: '.$removed_images_count.', Atleast Images: '.$parameters[1]);

            return ($slider_images_count - $removed_images_count) >= $parameters[1];

        });

        Validator::replacer('allowexts', function($message, $attribute, $rule, $parameters) {

                if(is_array(Input::file($attribute))){
                    $images = Input::file($attribute);
                }else{
                    $images = array(Input::file($attribute));
                }

                $message = '';

                foreach ($images as $image) {
                
                    $filename = $image->getClientOriginalName();

                    $ext = strtolower(substr($filename,strrpos($filename, '.') + 1));

                    if(!in_array($ext, $parameters)){
                        $message .= $image->getClientOriginalName()."'s format is not ".implode(', ', $parameters).'<br>';
                    }
                    
                }
                return  $message;
                });

        Validator::replacer('image_dimension', function($message, $attribute, $rule, $parameters) {

            if(is_array(Input::file($attribute))){
                $images = Input::file($attribute);
            }else{
                $images = array(Input::file($attribute));
            }
            $error_msg = '';
            foreach ($images as $image) {

                $file = $image->getRealPath();
                $image_info = getimagesize($file);

                $image_width = $image_info[0];

                $image_height = $image_info[1];

                $display_error = '';

                $start_msg = "The ".$image->getClientOriginalName()."'s ";
                foreach ($parameters as $parameter) {
                        $parameter_data = explode('=', $parameter);
                        switch ($parameter_data[0]) {
                            case 'width':
                                 if($parameter_data[1] != $image_width){
                                    $display_error .= "width must be exactly ".$parameter_data[1]."px and ";
                                 }
                            break;

                            case 'max_width':
                                if($parameter_data[1] < $image_width){
                                    $display_error .= "width must not be greater than ".$parameter_data[1]."px and ";
                                }
                            break;

                            case 'min_width':
                                if($parameter_data[1] > $image_width){
                                    $display_error .= "width must not be less than ".$parameter_data[1]."px and ";
                                }
                            break;

                            case 'height':
                                 if($parameter_data[1] != $image_height){
                                    $display_error .= "height must be exactly ".$parameter_data[1]."px and ";
                                 }
                            break;

                            case 'max_height':
                                if($parameter_data[1] < $image_height){
                                    $display_error .= "height must not be greater than ".$parameter_data[1]."px and ";
                                }
                            break;

                            case 'min_height':
                                if($parameter_data[1] > $image_height){
                                    $display_error .= "height must not be less than ".$parameter_data[1]."px and ";
                                }
                            break;
                            
                            default:
                                 return $display_error .= 'Special the dimension rules using this format => image_dimension:width=200,height=345';
                            break;
                        }
                    }
                    if(!empty($display_error)){
                        $error_msg .= $start_msg.rtrim($display_error, ' and ').'<br>';
                    }
                    
                }

                return rtrim($error_msg, '<br>');
            });;


        Validator::replacer('blog_type_placeholder', function($message, $attribute, $rule, $parameters) {

                     switch (Input::get($attribute)) {
                        case 'image':
                            return Input::hasFile(Input::get($attribute)) ? '' : 'The image thumbnail is required';
                        break;
                        case 'slider':
                               return Input::hasFile(Input::get($attribute)) ? '' : 'The slider images are required';
                        break;
                        case 'video_url':
                                return empty(Input::get(Input::get($attribute))) ? 'The video URL is required' : '';
                        break;
                        default:
                                return 'Something is wrong';
                        break;
                    }
                });


        Validator::replacer('min_array', function($message, $attribute, $rule, $parameters){
            return ucwords($attribute).' must container atleast '.$parameters[0].' images';
        });

        Validator::replacer('min_slider_image', function($message, $attribute, $rule, $parameters){

            return 'Slider Imges must container atleast '.$parameters[1].' images';

        });

        Validator::replacer('blog_type_placeholder_sometimes', function($message, $attribute, $rule, $parameters) {

                     switch (Input::get($attribute)) {
                        case 'image':
                            return Input::hasFile(Input::get($attribute)) ? '' : 'The image thumbnail is required';
                        break;
                        case 'slider':
                               return Input::hasFile(Input::get($attribute)) ? '' : 'The slider images are required';
                        break;
                        case 'video_url':
                                return empty(Input::get(Input::get($attribute))) ? 'The video URL is required' : '';
                        break;
                        default:
                                return 'Something is wrong';
                        break;
                    }
                });


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
