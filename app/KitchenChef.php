<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KitchenChef extends Model
{
    protected $table ='kitchen_chefs';

    protected $fillable = ['name','kitchen_position_id','kitchen_rank_id','excerpt'];

    public static $rules = [
    	'kitchen_position' => 'required|integer',
    	'kitchen_rank' => 'required|integer',
    	'name' => 'required',
    	'thumbnail' => 'required|image|mimes:jpeg,jpg,png,gif|image_dimension:min_width=650,min_height=563',
    	'excerpt' => 'required|min:15'
    ];

    public function kitchen_position(){
    	return $this->belongsTo('App\KitchenPosition', 'kitchen_position_id');
    }

    public function kitchen_rank(){
    	return $this->belongsTo('App\KitchenRank', 'kitchen_rank_id');
    }
}
