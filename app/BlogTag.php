<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogTag extends Model
{
    protected $table = 'blog_tags';

    protected $fillable = ['tag'];


    public function blogs(){
    	return $this->belongsToMany('\App\Blog', 'blogs_tags_pivot', 'tag_id', 'blog_id')->withTimestamps();
    }
}
