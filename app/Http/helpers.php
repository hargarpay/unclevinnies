<?php

use Illuminate\Support\Facades\DB;

use Illuminate\Pagination\LengthAwarePaginator;

use Illuminate\Pagination\Paginator;

use App\Category;

use App\BlogCategory;

use App\BlogTag;

use App\BlogComment;

use App\Order;

 function paginate($items,$perPage = 5)
    {
        $pageStart = \Request::get('page', 1);

        // Start displaying items from this number;
        $offSet = ($pageStart * $perPage) - $perPage; 

        // Get only the items you need using array_slice
        $itemsForCurrentPage = array_slice($items, $offSet, $perPage, true);

        return new LengthAwarePaginator($itemsForCurrentPage, count($items), $perPage,Paginator::resolveCurrentPage(), array('path' => Paginator::resolveCurrentPath()));
    }


function fetchDataTable($table, $data, $perPage = 5){

    $sql = "SELECT * FROM `".$table."`";

    $sql_where = ' WHERE';

    $sql_where_clause = '';

    $all_empty = true;

    foreach ($data as $key => $value) {
        if(!empty($value)){
            if($key != 'page')
            $sql_where_clause .= " `$key` = '$value' AND";

            $all_empty = false;
        }
    }

    if(!$all_empty){
        $sql .= $sql_where.$sql_where_clause;
    }

    $sql = rtrim($sql, ' AND');

    $items = DB::select($sql);


    if($perPage == 0){

        return collect($items);

    }else{
       return paginate($items, $perPage);  
    }

}

function getMenuByCategoryID($cat_id){
    $category_menu = Category::find($cat_id)->menus;
    return $category_menu;
}

function getCartMenuID($carts){
    $array_cart_menu = array();
    if(count($carts) > 0){
        foreach ($carts as $cart_menu) {
            $array_cart_menu[] = array_get($cart_menu, 'id');
        }
    }
 return $array_cart_menu;
}

function getBlogByCategoryID($category_id){

    $blog_cat = BlogCategory::findOrFail($category_id);

    return $blog_cat->blogs;

}

function getBlogByTagID($category_id){

    $blog_cat = BlogTag::findOrFail($category_id);

    return $blog_cat->blogs;

}

// Comment Multiple level Magic Function
function getComments($blog_comment) {

    echo '<div class="media blog-reply">
            <div class="media-left">
              <a href="#.">
                <img alt="Bianca Reid" src="'.url($blog_comment->user->passport).'">
              </a>
            </div>
            <div class="media-body">
              <h4>'.$blog_comment->user->fullname.'</h4>
              <span>'.\Carbon\Carbon::parse($blog_comment->created_at)->format('jS F, Y').'</span>
              <p class="no-margin">'.$blog_comment->comment.'</p>
              <a class="reply" href="#reply-form" data-comment-id="'.$blog_comment->id.'">Reply</a>';
 $blog_sub_comments = BlogComment::where('parent_id', '=', $blog_comment->id)
                                ->get();
  if($blog_sub_comments->count() > 0){
      foreach ($blog_sub_comments as $blog_sub_comment) {
          getComments($blog_sub_comment);
      }
  }
 echo "</div>
          </div>";
}

function getUniqueRef($ref_no = ''){

    if($ref_no == ''){
      $ref_no = 'unclevinnies_'.rand(1,99) . substr(uniqid(), 0,10);
    }

    if(Order::where('ref_no', $ref_no)->count() != 0){
        getUniqueRef();
    }else{
      return $ref_no;
    }

}
?>