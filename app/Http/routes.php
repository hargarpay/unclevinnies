<?php

Route::pattern('id','[0-9]+');
Route::get('/', ['as' => 'home','uses' => 'HomeController@index']);
Route::get('/gallery', ['as' => 'gallery','uses' => 'HomeController@gallery']);
Route::get('/menu-list', ['as' => 'menus','uses' => 'HomeController@menu']);
Route::get('/menu/{id}/{slug}', ['as' => 'view.single.menu','uses' => 'HomeController@viewSingleMenu']);
Route::get('/about-us', ['as' => 'about-us','uses' => 'HomeController@aboutUs']);
Route::get('/location', ['as' => 'location','uses' => 'HomeController@location']);
Route::get('/food', ['as' => 'food','uses' => 'HomeController@food']);
Route::get('/order/page', ['as' => 'order.page','uses' => 'HomeController@orderPage']);
Route::get('/order/checkout', ['as' => 'order.checkout','uses' => 'HomeController@orderCheckout']);
Route::post('/order/checkout', ['as' => 'order.checkout.submit','uses' => 'OrderController@nakeOrder']);
Route::get('/paystack/callback', ['as' => 'paystack.callback', 'uses' => 'OrderController@handlePaystackGatewayCallback']);
Route::group(['prefix' => 'blogs'], function(){
	Route::pattern('id','[0-9]+');
	// Route::pattern('slug', '[a-zA-Z0-9]')
	Route::get('/', ['as' => 'view.blogs', 'uses' => 'HomeController@viewBlogs']);
	Route::get('view/{id}/{slug}', ['as' => 'view.single.blog', 'uses' => 'HomeController@viewSingleBlog']);
});

Route::group(['prefix' => 'emails'], function(){
	Route::get('/menu-order', ['as' => 'email.menuorder', 'uses' => 'MailController@menuOrder']);
	Route::get('/register', ['as' => 'email.register', 'uses' => 'MailController@register']);
});

Route::group(['prefix' => 'do-ajax'], function(){
	Route::post('/add-to-cart', ['as' => 'add.to.cart', 'uses' => 'MenuController@addToCart']);
	Route::post('/remove-menu-from-cart', ['as' => 'remove.menu.from.cart', 'uses' => 'MenuController@removeMenuFromCart']);
	Route::post('/update-menu-qty-cart', ['as' => 'update.menu.qty.cart', 'uses' => 'MenuController@updateMenuQtyCart']);
	Route::get('/get-cart', ['as' => 'get.cart', 'uses' => 'MenuController@getCart']);
});
Route::group(['prefix' => 'dashboard'], function(){

	Route::get('/', ['as' => 'dashboard', 'uses' => 'DashboardController@view']);

	Route::group(['prefix' => 'slider'], function(){
		Route::get('view', ['as' => 'view.slider', 'uses' => 'SliderController@view']);
		Route::get('create', ['as' => 'create.slider', 'uses' => 'SliderController@create']);
		Route::post('create', ['as' => 'register.slider', 'uses' => 'SliderController@register']);
		Route::get('{id}/edit', ['as' => 'edit.slider', 'uses' => 'SliderController@edit']);
		Route::put('{id}/edit', ['as' => 'update.slider', 'uses' => 'SliderController@update']);
		Route::put('{id}/publish', ['as' => 'publish.slider', 'uses' => 'SliderController@publish']);
		Route::put('{id}/draft', ['as' => 'draft.slider', 'uses' => 'SliderController@draft']);
		Route::delete('{id}/delete', ['as' => 'delete.slider', 'uses' => 'SliderController@delete']);
	});

	Route::group(['prefix' => 'category'], function(){
		Route::get('view', ['as' => 'view.category', 'uses' => 'CategoryController@view']);
		Route::get('create', ['as' => 'create.category', 'uses' => 'CategoryController@create']);
		Route::post('create', ['as' => 'register.category', 'uses' => 'CategoryController@register']);
		Route::get('{id}/edit', ['as' => 'edit.category', 'uses' => 'CategoryController@edit']);
		Route::put('{id}/edit', ['as' => 'update.category', 'uses' => 'CategoryController@update']);
		Route::put('{id}/publish', ['as' => 'publish.category', 'uses' => 'CategoryController@publish']);
		Route::put('{id}/draft', ['as' => 'draft.category', 'uses' => 'CategoryController@draft']);
		Route::delete('{id}/delete', ['as' => 'delete.category', 'uses' => 'CategoryController@delete']);
	});

	Route::group(['prefix' => 'menu'], function(){
		Route::get('view', ['as' => 'view.menu', 'uses' => 'MenuController@view']);
		Route::get('create', ['as' => 'create.menu', 'uses' => 'MenuController@create']);
		Route::post('create', ['as' => 'register.menu', 'uses' => 'MenuController@register']);
		Route::get('{id}/edit', ['as' => 'edit.menu', 'uses' => 'MenuController@edit']);
		Route::put('{id}/edit', ['as' => 'update.menu', 'uses' => 'MenuController@update']);
		Route::put('{id}/publish', ['as' => 'publish.menu', 'uses' => 'MenuController@publish']);
		Route::put('{id}/draft', ['as' => 'draft.menu', 'uses' => 'MenuController@draft']);
		Route::put('{id}/feature', ['as' => 'feature.menu', 'uses' => 'MenuController@feature']);
		Route::put('{id}/unfeature', ['as' => 'unfeature.menu', 'uses' => 'MenuController@unfeature']);
		Route::delete('{id}/delete', ['as' => 'delete.menu', 'uses' => 'MenuController@delete']);
	});

	Route::group(['prefix' => 'blog-category'], function(){
		Route::get('view', ['as' => 'view.blog.category', 'uses' => 'BlogCategoryController@view']);
		Route::get('create', ['as' => 'create.blog.category', 'uses' => 'BlogCategoryController@create']);
		Route::post('create', ['as' => 'register.blog.category', 'uses' => 'BlogCategoryController@register']);
		Route::get('{id}/edit', ['as' => 'edit.blog.category', 'uses' => 'BlogCategoryController@edit']);
		Route::put('{id}/edit', ['as' => 'update.blog.category', 'uses' => 'BlogCategoryController@update']);
		Route::put('{id}/publish', ['as' => 'publish.blog.category', 'uses' => 'BlogCategoryController@publish']);
		Route::put('{id}/draft', ['as' => 'draft.blog.category', 'uses' => 'BlogCategoryController@draft']);
		Route::delete('{id}/delete', ['as' => 'delete.blog.category', 'uses' => 'BlogCategoryController@delete']);
	});

	Route::group(['prefix' => 'blog-tag'], function(){
		Route::get('view', ['as' => 'view.blog.tag', 'uses' => 'BlogTagController@view']);
		Route::get('create', ['as' => 'create.blog.tag', 'uses' => 'BlogTagController@create']);
		Route::post('create', ['as' => 'register.blog.tag', 'uses' => 'BlogTagController@register']);
		Route::get('{id}/edit', ['as' => 'edit.blog.tag', 'uses' => 'BlogTagController@edit']);
		Route::put('{id}/edit', ['as' => 'update.blog.tag', 'uses' => 'BlogTagController@update']);
		Route::put('{id}/publish', ['as' => 'publish.blog.tag', 'uses' => 'BlogTagController@publish']);
		Route::put('{id}/draft', ['as' => 'draft.blog.tag', 'uses' => 'BlogTagController@draft']);
		Route::delete('{id}/delete', ['as' => 'delete.blog.tag', 'uses' => 'BlogTagController@delete']);
	});

	Route::group(['prefix' => 'blog'], function(){
		Route::get('view', ['as' => 'view.blog', 'uses' => 'BlogController@view']);
		Route::get('create', ['as' => 'create.blog', 'uses' => 'BlogController@create']);
		Route::post('create', ['as' => 'register.blog', 'uses' => 'BlogController@register']);
		Route::get('{id}/edit', ['as' => 'edit.blog', 'uses' => 'BlogController@edit']);
		Route::put('{id}/edit', ['as' => 'update.blog', 'uses' => 'BlogController@update']);
		Route::get('{id}/slider', ['as' => 'slider.edit.blog', 'uses' => 'BlogController@sliderEdit']);
		Route::put('{id}/slider', ['as' => 'slider.update.blog', 'uses' => 'BlogController@sliderUpdate']);
		Route::put('{id}/publish', ['as' => 'publish.blog', 'uses' => 'BlogController@publish']);
		Route::put('{id}/draft', ['as' => 'draft.blog', 'uses' => 'BlogController@draft']);
		Route::delete('{id}/delete', ['as' => 'delete.blog', 'uses' => 'BlogController@delete']);
	});

	Route::group(['prefix' => 'blog-comment'], function(){
		Route::pattern('id','[0-9]+');
		Route::pattern('comment_id','[0-9]+');
		Route::get('{id}/coment', ['as' => 'get.comment.blog', 'uses' => 'HomeController@viewSingleBlog']);
		Route::post('{id}/coment', ['as' => 'comment.blog', 'uses' => 'BlogCommentController@comment']);
		Route::post('{id}/{coment_id}/edit', ['as' => 'update.blog.comment', 'uses' => 'BlogCommentController@update']);
		Route::post('{id}/{coment_id}/like', ['as' => 'like.blog.comment', 'uses' => 'BlogCommentController@like']);
		Route::post('{id}/{coment_id}/unlike', ['as' => 'unlike.blog.comment', 'uses' => 'BlogCommentController@unlike']);
		Route::delete('{id}/{coment_id}/comment', ['as' => 'delete.blog.comment', 'uses' => 'BlogCommentController@delete']);
	});



	Route::group(['prefix' => 'kitchen'], function(){
		Route::group(['prefix' => 'chef'],  function(){

			Route::get('view', ['as' => 'view.kitchen.chef', 'uses' => 'KitchenChefController@view']);
			Route::get('create', ['as' => 'create.kitchen.chef', 'uses' => 'KitchenChefController@create']);
			Route::post('create', ['as' => 'register.kitchen.chef', 'uses' => 'KitchenChefController@register']);
			Route::get('{id}/edit', ['as' => 'edit.kitchen.chef', 'uses' => 'KitchenChefController@edit']);
			Route::put('{id}/edit', ['as' => 'update.kitchen.chef', 'uses' => 'KitchenChefController@update']);
			Route::delete('{id}/delete', ['as' => 'delete.kitchen.chef', 'uses' => 'KitchenChefController@delete']);
		});

		Route::group(['prefix' => 'position'],  function(){

			Route::get('view', ['as' => 'view.kitchen.position', 'uses' => 'KitchenPositionController@view']);
			Route::get('create', ['as' => 'create.kitchen.position', 'uses' => 'KitchenPositionController@create']);
			Route::post('create', ['as' => 'register.kitchen.position', 'uses' => 'KitchenPositionController@register']);
			Route::get('{id}/edit', ['as' => 'edit.kitchen.position', 'uses' => 'KitchenPositionController@edit']);
			Route::put('{id}/edit', ['as' => 'update.kitchen.position', 'uses' => 'KitchenPositionController@update']);
			Route::delete('{id}/delete', ['as' => 'delete.kitchen.position', 'uses' => 'KitchenPositionController@delete']);
		});


		Route::group(['prefix' => 'rank'],  function(){

			Route::get('view', ['as' => 'view.kitchen.rank', 'uses' => 'KitchenRankController@view']);
			Route::get('create', ['as' => 'create.kitchen.rank', 'uses' => 'KitchenRankController@create']);
			Route::post('create', ['as' => 'register.kitchen.rank', 'uses' => 'KitchenRankController@register']);
			Route::get('{id}/edit', ['as' => 'edit.kitchen.rank', 'uses' => 'KitchenRankController@edit']);
			Route::put('{id}/edit', ['as' => 'update.kitchen.rank', 'uses' => 'KitchenRankController@update']);
			Route::delete('{id}/delete', ['as' => 'delete.kitchen.rank', 'uses' => 'KitchenRankController@delete']);
		});

	});
});

Route::auth();

Route::get('/home', 'HomeController@index');
