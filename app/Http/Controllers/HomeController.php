<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\Repositories\MenuRepositoryInterface;

use App\Repositories\CategoryRepositoryInterface;

use App\Repositories\KitchenPositionRepositoryInterface;

use App\Repositories\KitchenChefRepositoryInterface;

use App\Repositories\SliderRepositoryInterface;

use Cart;

use App\Blog;

use App\BlogTag;

use App\BlogCategory;

use App\BlogComment;

class HomeController extends Controller
{
    protected $menu;

    protected $category;

    protected $kitchen_position;

    protected $kitchen_chef;

    protected $slider;

    public function __construct(MenuRepositoryInterface $menu, CategoryRepositoryInterface $category, KitchenPositionRepositoryInterface $kitchen_position, KitchenChefRepositoryInterface $kitchen_chef, SliderRepositoryInterface $slider)
    {
        //$this->middleware('auth');
        $this->menu = $menu;

        $this->category = $category;

        $this->kitchen_chef = $kitchen_chef;

        $this->kitchen_position = $kitchen_position;

        $this->slider = $slider;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'HomePage';

        $categories = $this->category->setCategory()
                            ->where('published', 1)
                            ->get();
        $menus = $this->menu->setMenu()
                            ->where('published', 1)
                            ->get();

        $slider_publish_shared = $this->slider->setSlider()
                        ->where('published', 1)
                        ->get();

        $feature_menu = $this->menu->setMenu()
                            ->where('featured', 1)
                            ->limit(4)
                            ->get();

        $limit_menus = $this->menu->setMenu()
                            ->where('published', 1)
                            ->limit(9)
                            ->get();


        $kitchen_chefs = $this->kitchen_chef->setKitchenChef()
                                            ->limit(6)
                                            ->get();
        //dd($slider_publish_shared);

        return view('homepage',  compact('title', 'categories', 'menus', 'feature_menu', 'kitchen_chefs', 'slider_publish_shared', 'limit_menus'));
    }

    public function gallery(){
        $title = 'Menu Gallery';
        $categories = $this->category->setCategory()
                            ->where('published', 1)
                            ->get();
        $menus = $this->menu->setMenu()
                            ->where('published', 1)
                            ->get();

        return view('gallery',  compact('title', 'categories', 'menus'));
    }

    public function menu(){
        $title = 'View Menu';
        $categories = $this->category->setCategory()
                            ->where('published', 1)
                            ->get();

        return view('menu',  compact('title', 'categories', 'menus'));
    }

    public function aboutUs(){
        $title = 'About Us';

        $kitchen_chefs = $this->kitchen_chef->getAll();

        $kitchen_positions = $this->kitchen_position->getAll();

        return view('about-us', compact('title', 'kitchen_chefs', 'kitchen_positions'));
    }

    public function location(){
        $title = 'Location';

        return view('location', compact('title'));
    }

    public function orderPage(){
        $title = 'Order Now';

        $categories = $this->category->setCategory()
                            ->where('published', 1)
                            ->get();

        $cart_menu_ids = getCartMenuID(Cart::contents(true));

        return view('order-page',  compact('title', 'categories', 'cart_menu_ids'));
    }

    public function food(){
        $title = 'Food';

        $categories = $this->category->setCategory()
                            ->where('published', 1)
                            ->get();

        $limit_categories = $this->category->setCategory()
                            ->where('published', 1)
                            ->limit(6)
                            ->get();

        $menus = $this->menu->setMenu()
                            ->where('published', 1)
                            ->get();

        $limit_menus = $this->menu->setMenu()
                            ->where('published', 1)
                            ->limit(7)
                            ->get();

        $feature_menus = $this->menu->setMenu()
                            ->where('featured', 1)
                            ->limit(4)
                            ->get();

        return view('food', compact('title', 'categories', 'limit_categories', 'menus', 'feature_menus', 'limit_menus'));
    }

    public function orderCheckout(){

        return view('checkout');
    }

    public function viewBlogs(){
        $title = 'View Blog';

        $tags = BlogTag::where('published', 1)
                        ->get();

        $categories = BlogCategory::where('published', 1)
                                ->get();

        $blogs = Blog::where('published', 1)
                    ->paginate(3);

        $latest_blogs = Blog::where('published', 1)
                            ->orderBy('created_at', 'desc')
                            ->where('type', '!=', 'video_url')
                            ->limit(2)
                            ->get();

        $popular_blogs = Blog::where('published', 1)
                            ->orderBy('views', 'desc')
                            ->where('type', '!=', 'video_url')
                            ->limit(4)
                            ->get();

        $blog_sidebar_comments = BlogComment::orderBy('created_at', 'desc')
                                    ->limit(4)
                                    ->get();

        return view('blog', compact('title', 'blogs', 'tags', 'categories', 'latest_blogs', 'popular_blogs', 'blog_sidebar_comments'));
    }

    public function viewSingleBlog($id){

        $blog = Blog::findOrFail($id);

        $title = $blog->title;

        $blog_tags = '';

        $categories = BlogCategory::where('published', 1)
                                ->get();

        $tags = BlogTag::where('published', 1)
                        ->get();

        $latest_blogs = Blog::where('published', 1)
                            ->orderBy('created_at', 'desc')
                            ->where('type', '!=', 'video_url')
                            ->limit(4)
                            ->get();
        $popular_blogs = Blog::where('published', 1)
                            ->orderBy('views', 'desc')
                            ->where('type', '!=', 'video_url')
                            ->limit(4)
                            ->get();

        $blog_comments = BlogComment::where('parent_id', 0)
                                    ->where('blog_id', $id)
                                    ->orderBy('created_at', 'desc')
                                    ->get();

        $blog_sidebar_comments = BlogComment::orderBy('created_at', 'desc')
                                    ->limit(4)
                                    ->get();
                                    
        $all_blog_comments = BlogComment::where('blog_id', $id)
                                    ->orderBy('created_at', 'desc')
                                    ->get();

        if($blog->tags->count() > 0){
            foreach ($blog->tags as $tag) {
                $blog_tags .= $tag->tag.', ';
            }
        }

        $blog->views = $blog->views + 1;

        $blog->save();

        return view('blog-details', compact('title', 'blog', 'categories', 'tags', 'latest_blogs', 'blog_tags', 'popular_blogs', 'blog_comments', 'blog_sidebar_comments', 'all_blog_comments'));
    }

    function viewSingleMenu($id){
        $menu = $this->menu->find($id);
        $title = $menu->title;

        $categories = $this->category->setCategory()
                            ->where('published', 1)
                            ->get();

        $cart_menu_ids = getCartMenuID(Cart::contents(true));

        return view('view-single-menu', compact('menu', 'title', 'categories', 'cart_menu_ids'));


    }
}
