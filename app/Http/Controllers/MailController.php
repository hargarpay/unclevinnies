<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Cart;

use App\Menu;

class MailController extends Controller
{
    public function __construct(){

    }

    public function menuOrder(){
    	$user = new \stdClass;
    	$user->fullname = 'Odewale Ifeoluwa';
    	$user->email = 'ifeoluwa.odewale@gmail.com';
    	$user->phone = '08056671313';
    	$user->address = '6, Jeminatu Buraimoh Street, Surulere Lagos';
    	$items = Cart::contents();
    	$grand_total = Cart::total();
    	$latest_menu = Menu::orderBy('created_at', 'desc')
    						->limit(3)
    						->get();
    	return view('emails.menuorder', compact('items', 'latest_menu', 'grand_total','user'));
    }

    public function register(){

        $user = new \stdClass;
        $user->fullname = 'Odewale Ifeoluwa';
        $user->email = 'ifeoluwa.odewale@gmail.com';
        $user->phone = '08056671313';
        $user->address = '6, Jeminatu Buraimoh Street, Surulere Lagos';
        $user->password = 'password';
        $promo_menu = Menu::orderBy('amount', 'asc')
                            ->limit(3)
                            ->get();;
        $latest_menu = Menu::orderBy('created_at', 'desc')
                            ->limit(3)
                            ->get();
        return view('emails.register', compact('latest_menu', 'promo_menu','user'));

    }
}
