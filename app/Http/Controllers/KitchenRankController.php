<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Repositories\KitchenRankRepositoryInterface;

use Validator;

class KitchenRankController extends Controller
{
    protected $kitchen_rank;

    public function __construct(KitchenRankRepositoryInterface $kitchen_rank){

        $this->middleware('auth');
        $this->middleware('admin');

    	$this->kitchen_rank = $kitchen_rank;


    }

    public function view(){
    	$title = 'View Kitchen Rank';

    	$kitchen_ranks = $this->kitchen_rank->setKitchenRank()
    						->paginate(30);

    	return view('dashboard.kitchen-rank.view', compact('title', 'kitchen_ranks'));
    }

    public function create(){
    	$title = 'Create Kitchen Rank';

    	return view('dashboard.kitchen-rank.create', compact('title'));

    }

    public function register(Request $request){
    	$setKitchenRank = $this->kitchen_rank->setKitchenRank();

    	$rule = [
    		'name' => 'required|unique:kitchen_ranks,name'
    	];

    	$validate = Validator::make($request->all(), $rule);

    	if($validate->passes()){

    		$kitchen_rank = $setKitchenRank->create([
    				'name' => $request->input('name')
    		]);

    		flash('You have successfully created a kitchen rank')->success();

            return redirect()->back();


    	}

    	flash('Something is wrong')->error();

    	return redirect()->back()
    					->withErrors($validate)
    					->withInput();
    }

    public function edit($id){

    	$title = 'Edit Kitchen Rank';

    	$kitchen_rank = $this->kitchen_rank->find($id);

    	return view('dashboard.kitchen-rank.edit', compact('title', 'kitchen_rank'));
    }

    public function update($id, Request $request){

    	$setKitchenRank = $this->kitchen_rank->setKitchenRank();

		$rules = [
    		'name' => 'required|unique:kitchen_ranks,name,'.$id
    	];	

    	$validate = Validator::make($request->all(), $rules);

    	if($validate->passes()){

    		$kitchen_rank = $this->kitchen_rank->find($id);

    		$kitchen_rank->update([
    				'name' => $request->input('name')
    		]);

            flash('You have successfully updated a kitchen rank')->success();

            return redirect()->back();

    	}
    }

    public function delete($id){
    	$kitchen_rank = $this->kitchen_rank->find($id);
    	$kitchen_rank->delete();
    	flash('You have successfully deleted the kitchen rank')->success();
    	return redirect()->back();

    }
}
