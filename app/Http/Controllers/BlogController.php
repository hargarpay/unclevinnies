<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Blog;

use App\BlogTag;

use App\BlogCategory;

use Validator;

use Image;

class BlogController extends Controller
{
    public function __construct(){

        $this->middleware('auth');
        $this->middleware('admin');

    }

    public function view(){
      $title = 'View Blogs';

      $blogs = Blog::paginate(20);

      return view('dashboard.blog.view', compact('title', 'blogs'));
    }

   	public function create(){
   		$title = 'Create Blog';

   		$tags = BlogTag::where('published', 1)
   						->get();

   		$categories = BlogCategory::where('published', 1)
   								->get();

   		$blog_types = ['image', 'slider', 'video_url'];

   		return view('dashboard.blog.create', compact('title', 'tags', 'categories', 'blog_types'));
   	}

   	public function register(Request $request){

   		$rules = Blog::$rules;

   		$validate = Validator::make($request->all(), $rules);

   		if($validate->passes()){

   			$blog = Blog::create([
   							'title' => $request->input('name'),
   							'type' => $request->input('blog_type'),
   							'description' => e($request->input('articleContent'))
   							]);

   			//Image and Slider will pass through else it uses video_url

   			if($request->hasFile($request->blog_type)){

   				if(is_array($request->file($request->blog_type))){
                    $images = $request->file($request->blog_type);
                }else{
                    $images = array($request->file($request->blog_type));
                }

                $blog_image_json = array();

                foreach ($images as $key => $image) {
                	$blog_image_path = md5(time())."-".$key.'-'.str_slug($request->name).".".$image->getClientOriginalExtension();

                    Image::make($image->getRealPath())->resize(900,600)->save(public_path() . '/images/blogs/'.$blog_image_path);

                    $blog_image_json[] = '/images/blogs/'.$blog_image_path.'?cache='.time();

                }

                $blog->placeholder = json_encode($blog_image_json);
                
   			}else{
   				$blog->placeholder = $request->input($request->blog_type);
   			}
            
            $blog->save();

            $blog->tags()->attach($request->tags);

            $blog->categories()->attach($request->categories);

            flash('You have successfully created a blog')->success();

            return redirect()->back();
   		}

   		flash('Something is wrong')->error();

   		return redirect()->back()
   						->withErrors($validate)
   						->withInput();

   	}


   	public function edit($id, Request $request){
   		$title = 'Edit Blog';

   		$tags = BlogTag::where('published', 1)
   						->get();

   		$categories = BlogCategory::where('published', 1)
   								->get();

   		$blog = Blog::findOrFail($id);

   		$tag_select = array();

   		$category_select = array();

   		if($blog->tags->count() > 0){
	   		foreach ($blog->tags as $tag) {
	   			$tag_select[] = $tag->id;
	   		}
	   	}

   		if($blog->categories->count() > 0){
	   		foreach($blog->categories as $category) {
	   			$category_select[] = $category->id;
	   		}
   		}

   		$blog_types = ['image', 'slider', 'video_url'];

   		return view('dashboard.blog.edit', compact('title', 'tags', 'categories', 'blog_types', 'blog', 'tag_select','category_select' ));
   	}


   	public function update($id, Request $request){

   		$rules = Blog::$rules;

      $rules['blog_type'] = ['required', 'regex:/(image|video_url|slider)/', 'blog_type_placeholder_sometimes'];

   		$validate = Validator::make($request->all(), $rules);

   		if($validate->passes()){

   			$blog = Blog::findOrFail($id);

        $blog_former_type = $blog->type;

   			$blog->update([
   							'title' => $request->input('name'),
   							'type' => $request->input('blog_type'),
   							'description' => e($request->input('articleContent'))
   							]);

   			//Image and Slider will pass through else it uses video_url

   			if($request->hasFile($request->blog_type)){

   				     if($request->blog_type == 'image'){
                    $images = array($request->file($request->blog_type));
                    $blog_image_json = array();
                }else{
                    $images = $request->file($request->blog_type);
                    if($blog_former_type  != 'video_url'){
                       $blog_image_json = json_decode($blog->placeholder, true);
                    }else{
                      $blog_image_json = array();
                    }
                }

                foreach ($images as $key => $image) {
                	$blog_image_path = md5(time())."-".$key.'-'.str_slug($request->name).".".$image->getClientOriginalExtension();

                    Image::make($image->getRealPath())->resize(900,600)->save(public_path() . '/images/blogs/'.$blog_image_path);

                    $blog_image_json[] = '/images/blogs/'.$blog_image_path.'?cache='.time();

                }

                $blog->placeholder = json_encode($blog_image_json);
                
   			}elseif($request->blog_type == 'video_url'){
   				   $blog->placeholder = $request->input($request->blog_type);
   			}
            
            $blog->save();

            $blog->tags()->sync($request->tags);

            $blog->categories()->sync($request->categories);

            flash('You have successfully update a blog')->success();

            return redirect()->back();
   		}

   		flash('Something is wrong')->error();

   		return redirect()->back()
   						->withErrors($validate)
   						->withInput();

   	}

    public function publish($id){
      $tag = Blog::findOrFail($id);
      $tag->published = 1;
      $tag->save();
      flash('You have successfully published the blog')->success();
      return redirect()->back();
    }


    public function draft($id){
      $tag = Blog::findOrFail($id);
      $tag->published = 0;
      $tag->save();
      flash('You have successfully drafted the blog')->success();
      return redirect()->back();
    }

    public function delete($id){
      $tag = Blog::findOrFail($id);
      $tag->delete();
      flash('You have successfully deleted the blog')->success();
      return redirect()->back();
    }

    function sliderEdit($id){

      $title = 'Edit Slider';

      $blog = Blog::findOrFail($id);

      return view('dashboard.blog.edit-slider', compact('title', 'blog'));

    }

    function sliderUpdate($id, Request $request){
      
      $validate = Validator::make($request->all(), ['removeImages' => 'min_slider_image:blog_id,2']);

      if($validate->passes()){
        $blog = Blog::findOrFail($id);

        $remove_images = explode(',', $request->input('removeImages'));

        $json_slider_image = array();

        foreach (json_decode($blog->placeholder, true) as $image_src) {
            if(!in_array($image_src, $remove_images)){
              $json_slider_image[] = $image_src;
            }else{
              @unlink( public_path(explode('?', str_replace('/', '\\', ltrim($image_src, '/')))[0] ));
            }
        }
        $blog->placeholder = json_encode($json_slider_image);

        $blog->save();

        return redirect()->route('view.blog');

      }
      flash('Something is wrong')->error();
      return redirect()->back()
                      ->withErrors($validate)
                      ->withInput();
    }
}
