<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use Auth;

use App\Menu;

use App\Events\Register;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    protected $rules = [
            'fullname' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'address' => 'required',
            'phone' => 'required:min:7'
        ];

    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, $this->rules);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'fullname' => $data['fullname'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'address' => $data['address'],
            'phone' => $data['phone']
        ]);
    }

     protected function validateLogin(Request $request)
    {
        $this->validate($request, [
                'email' => 'required',
                'password' => 'required'
            ]);
    }

    public function register(Request $request)
    {
        $validator = $this->validator($request->all());


        if ($validator->fails()) {
            flash('Something is wrong')->error();

            $this->throwValidationException(
                $request, $validator
            );
        }


        Auth::guard($this->getGuard())->login($this->create($request->all()));

        $user = new \stdClass;
        $user->fullname = $request->input('fullname');
        $user->email = $request->input('email');
        $user->phone = $request->input('phone');
        $user->address = $request->input('address');
        $user->password = $request->input('password');



        $promo_menu = Menu::orderBy('amount', 'asc')
                            ->limit(3)
                            ->get();;
        $latest_menu = Menu::orderBy('created_at', 'desc')
                            ->limit(3)
                            ->get();

        event(new Register($user, $promo_menu, $latest_menu));

        flash('You have successfully register')->success();


        return redirect($this->redirectPath());
    }


    public function login(Request $request){

        $this->validateLogin($request);

        $throttles = $this->isUsingThrottlesLoginsTrait();
        
        if ($throttles && $lockedOut = $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')]))
        {
            flash('You have successfully login')->success();
            return $this->handleUserWasAuthenticated($request, $throttles);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles && ! $lockedOut) {
            $this->incrementLoginAttempts($request);
        }

        flash('Sorry, something wrong')->error();

        return $this->sendFailedLoginResponse($request)->withInput();

     }

     public function logout()
    {
        flash('You have successfully logout')->success();
        Auth::logout();
        return redirect()->to('/login');
    }
}


