<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\BlogCategory;

use Validator;

use Image;

class BlogCategoryController extends Controller
{
    public function __construct(){

        $this->middleware('auth');
        $this->middleware('admin');

    }

    public function view(){
    	$title = 'View Blog Category';

    	$categories = BlogCategory::paginate(10);

    	return view('dashboard.category-blog.view', compact('categories', 'title'));
    }

    public function create(){
    	$title = "Create Blog Category";
    	return view('dashboard.category-blog.create', compact('title'));
    }

    public function register(Request $request){
    	$validate = Validator::make($request->all(), [
                    'category' => 'required|unique:blog_categories,category',
                    'thumbnail' => 'required|image|mimes:jpeg,jpg,png,gif|image_dimension:min_width=1920,min_height=450']);

    	if($validate->passes()){
    			$category = BlogCategory::create([
    					'category' => $request->input('category')
    				]);

                if($request->hasFile('thumbnail')){

                    $image = $request->file('thumbnail');

                    $category_menu_path = md5(time())."-".str_slug($request->category).".".$image->getClientOriginalExtension();

                    Image::make($image->getRealPath())->resize(1920,450)->save(public_path() . '/images/blog-categories-banners/'.$category_menu_path);

                    $category->banner = '/images/blog-categories-banners/'.$category_menu_path.'?cache='.time();

                    $category->save();

                }
            flash('You have successfully created a Blog Category')->success();

            return redirect()->route('view.blog.category');

    	}

    	flash('Something is wrong')->error();

    	return redirect()->back()
    					->withErrors($validate)
    					->withInput();
    }

     public function edit($id){

    	$title = 'Edit Category';

    	$category = BlogCategory::findOrFail($id);

    	return view('dashboard.category-blog.edit', compact('category', 'title'));
    }

    public function update($id, Request $request){

    	$validate = Validator::make($request->all(), [
            'category' => 'required|unique:blog_categories,category,'.$id,
            'thumbnail' => 'sometimes|image|mimes:jpeg,jpg,png,gif|image_dimension:min_width=900,min_height=337'
            ]);

    	if($validate->passes()){

    			$category = BlogCategory::findOrFail($id);

    			$category->category = $request->input('category');

                 if($request->hasFile('thumbnail')){

                    @unlink( public_path(explode('?', str_replace('/', '\\', ltrim($category->banner, '/')))[0] ));

                    $image = $request->file('thumbnail');

                    $category_menu_path = md5(time())."-".str_slug($request->category).".".$image->getClientOriginalExtension();

                    Image::make($image->getRealPath())->resize(900,337)->save(public_path() . '/images/blog-categories-banners/'.$category_menu_path);

                    $category->banner = '/images/blog-categories-banners/'.$category_menu_path.'?cache='.time();
                }

    			$category->save();


    			flash('You have successfully updated blog category')->success();

    			return redirect()->route('view.blog.category');
    	}

    	flash('Something is wrong')->error();

    	return redirect()->back()
    					->withErrors($validate)
    					->withInput();
    }

     public function publish($id){
    	$category = BlogCategory::findOrFail($id);
    	$category->published = 1;
    	$category->save();
    	flash('You have successfully published the category')->success();
    	return redirect()->back();
    }


    public function draft($id){
    	$category = BlogCategory::findOrFail($id);
    	$category->published = 0;
    	$category->save();
    	flash('You have successfully drafted the category')->success();
    	return redirect()->back();
    }

    public function delete($id){
    	$category = BlogCategory::findOrFail($id);
    	$category->delete();
    	flash('You have successfully deleted the category')->success();
    	return redirect()->back();

    }
}
