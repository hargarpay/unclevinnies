<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Events\Register;

use App\Events\MenuOrder;

use App\Order;

use App\User;

use App\Menu;

use Cart;

use Validator;

use Hash;

class OrderController extends Controller
{
    public function __construct(){

    }

    public function nakeOrder(Request $request){
        $order = new Order;

    	$user = new User;

    	$validate = Validator::make($request->all(), $order::$rules);

    	if($validate->passes()){

            $latest_menu = Menu::orderBy('created_at', 'desc')
                                    ->limit(3)
                                    ->get();

            $user_info = new \stdClass;

            $user_info->fullname = $request->input('fullname');
            $user_info->email = $request->input('email');
            $user_info->phone = $request->input('phone');
            $user_info->address = $request->input('address');

            $user->where('email', $request->input('email'))->first();

            if($user->where('email', $request->input('email'))->count() == 0){
               $user->create([
                    'fullname' => $request->input('fullname'),
                    'email' => $request->input('email'),
                    'address' => $request->input('address'),
                    'phone' => $request->input('phone'),
                    'password' => Hash::make('password')
                ]);

                $user_info->password = 'password';

                $promo_menu = Menu::orderBy('amount', 'asc')
                                    ->limit(3)
                                    ->get();

    			event(new Register($user_info, $promo_menu, $latest_menu));
    		}

            if(strtolower($request->input('payment_type')) != 'cash'){
                return $this->paystackRedirectToGateway($request);
            }

            return $this->postSuccess($request);
            

    	}

    	flash('Simething is wrong')->error();
    	return redirect()->back()
    					->withErrors($validate)
    					->withInput();
    }


    public function paystackRedirectToGateway(Request $request){

        $paystack = new \Yabacon\Paystack('test' == getenv('PAYSTACK_MODE') ? getenv('PAYSTACK_TEST_SECRET_KEY') : getenv('PAYSTACK_LIVE_SECRET_KEY'));

        //Building metadata :: More information about the transaction
        $builder = new \Yabacon\Paystack\MetadataBuilder();
        $builder->withCustomField('Fullname', $request->input('fullname'));
        $builder->withCustomField('Phone Number', $request->input('phone'));
        $metadata = $builder->build();

        try
        {
          $tranx = $paystack->transaction->initialize([
            'amount'=>  Cart::total() * 100,       // in kobo
            'email'=> $request->input('email'),         // unique to customers
            'reference' => $request->input('ref_no'), // unique to transactions
            'first_name' => $request->input('fullname'),
            'callback_url' => route('paystack.callback'),
            'metadata' => $metadata,
          ]);
        } catch(\Yabacon\Paystack\Exception\ApiException $e){
          print_r($e->getResponseObject());
          die($e->getMessage());
        }

        // dd($tranx);

         return redirect($tranx->data->authorization_url);

    }

    /**
     * Handles Paystack callbacks
     * @params Request
     * @return Respone
     */
    public function handlePaystackGatewayCallback(){
        $request = new Request;

        $reference = isset($_GET['reference']) ? $_GET['reference'] : '';
        if(!$reference){
          die('No reference supplied');
        }

        $paystack = new \Yabacon\Paystack('test' == getenv('PAYSTACK_MODE') ? getenv('PAYSTACK_TEST_SECRET_KEY') : getenv('PAYSTACK_LIVE_SECRET_KEY'));
        try
        {
          // verify using the library
          $tranx = $paystack->transaction->verify([
            'reference'=> $reference, // unique to transactions
          ]);
        } catch(\Yabacon\Paystack\Exception\ApiException $e){
          print_r($e->getResponseObject());
          die($e->getMessage());
        }
        //Verify the success of the transaction
        if ('success' === $tranx->data->status) {
            //Creating request data
            $request->merge(['email' => $tranx->data->customer->email,
                            'amount' => (($tranx->data->amount) / 100),
                            'payment_type' => 'Paystack',
                            'ref_no' => $tranx->data->reference]);
            //Converting the metadata to request variable
            $more_details = $tranx->data->metadata;

            foreach($more_details->custom_fields as $custom_field){
                $request->merge([str_replace(' ', '_', strtolower($custom_field->variable_name)) => $custom_field->value]);
            }
            // message for the newsletter
            return $this->postSuccess($request);
        }else{
            flash('Transaction failed. Please try again')->error();
            return redirect()->route("order.checkout");
        }

    }

    protected function postSuccess(Request $request){

            $order = new Order;

            $user = new User;

            $user_info = new \stdClass;

            $user_info->fullname = $request->input('fullname');
            $user_info->email = $request->input('email');
            $user_info->phone = $request->input('phone');
            $user_info->address = $request->input('address');

            $latest_menu = Menu::orderBy('created_at', 'desc')
                                    ->limit(3)
                                    ->get();

            $user_info->delivery = 'On the way';
            $user_info->payment_method =  $request->input('payment_type');
            $items = Cart::contents(true);
            $grand_total = Cart::total();

            $user_by_mail = $user->where('email', $request->input('email'))->first();

            $order->create([
                    'items' => json_encode($items),
                    'ref_no' => $request->input('ref_no'),
                    'user_id' => $user_by_mail->id,
                    'payment_channel' => $request->input('payment_type')
                ]);

            event(new MenuOrder($user_info, $items, $grand_total, $latest_menu));

            flash('You have successfully make your order')->success();

            Cart::destroy();
            
            return redirect()->route('order.page');
    }
}
