<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Repositories\KitchenPositionRepositoryInterface;

use Validator;

class KitchenPositionController extends Controller
{
	protected $kitchen_position;

    public function __construct(KitchenPositionRepositoryInterface $kitchen_position){

        $this->middleware('auth');
        $this->middleware('admin');

    	$this->kitchen_position = $kitchen_position;


    }

    public function view(){
    	$title = 'View Kitchen Position';

    	$kitchen_positions = $this->kitchen_position->setKitchenPosition()
    						->paginate(30);

    	return view('dashboard.kitchen-position.view', compact('title', 'kitchen_positions'));
    }

    public function create(){
    	$title = 'Create Kitchen Position';

    	return view('dashboard.kitchen-position.create', compact('title'));

    }

    public function register(Request $request){
    	$setKitchenPosition = $this->kitchen_position->setKitchenPosition();

    	$rule = [
    		'name' => 'required|unique:kitchen_positions,name'
    	];

    	$validate = Validator::make($request->all(), $rule);

    	if($validate->passes()){

    		$kitchen_position = $setKitchenPosition->create([
    				'name' => $request->input('name')
    		]);

    		flash('You have successfully created a kitchen position')->success();

            return redirect()->back();


    	}

    	flash('Something is wrong')->error();

    	return redirect()->back()
    					->withErrors($validate)
    					->withInput();
    }

    public function edit($id){

    	$title = 'Edit Kitchen Position';

    	$kitchen_position = $this->kitchen_position->find($id);

    	return view('dashboard.kitchen-position.edit', compact('title', 'kitchen_position'));
    }

    public function update($id, Request $request){

    	$setKitchenPosition = $this->kitchen_position->setKitchenPosition();

		$rules = [
    		'name' => 'required|unique:kitchen_positions,name,'.$id
    	];	

    	$validate = Validator::make($request->all(), $rules);

    	if($validate->passes()){

    		$kitchen_position = $this->kitchen_position->find($id);

    		$kitchen_position->update([
    				'name' => $request->input('name')
    		]);

            flash('You have successfully updated a kitchen position')->success();

            return redirect()->back();

    	}
    }

    public function delete($id){
    	$kitchen_position = $this->kitchen_position->find($id);
    	$kitchen_position->delete();
    	flash('You have successfully deleted the kitchen position')->success();
    	return redirect()->back();

    }
}
