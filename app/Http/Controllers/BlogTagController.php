<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\BlogTag;

use Validator;

class BlogTagController extends Controller
{
    public function __construct(){

        $this->middleware('auth');
        $this->middleware('admin');
    }
    public function view(){
    	$title = 'View Blog Tag';

    	$tags = BlogTag::paginate(10);

    	return view('dashboard.tag-blog.view', compact('tags', 'title'));
    }

    public function create(){
    	$title = "Create Blog Tag";
    	return view('dashboard.tag-blog.create', compact('title'));
    }

    public function register(Request $request){
    	$validate = Validator::make($request->all(), [
                    'tag' => 'required|unique:blog_tags,tag'
                    ]);

    	if($validate->passes()){
    			$tag = BlogTag::create([
    					'tag' => $request->input('tag')
    				]);

            flash('You have successfully created a Blog Tag')->success();

            return redirect()->route('view.blog.tag');

    	}

    	flash('Something is wrong')->error();

    	return redirect()->back()
    					->withErrors($validate)
    					->withInput();
    }

     public function edit($id){

    	$title = 'Edit Tag';

    	$tag = BlogTag::findOrFail($id);

    	return view('dashboard.tag-blog.edit', compact('tag', 'title'));
    }

    public function update($id, Request $request){

    	$validate = Validator::make($request->all(), [
            'tag' => 'required|unique:blog_tags,tag,'.$id
            ]);

    	if($validate->passes()){

    			$tag = BlogTag::findOrFail($id);

    			$tag->tag = $request->input('tag');

    			$tag->save();


    			flash('You have successfully updated tag menu')->success();

    			return redirect()->route('view.blog.tag');
    	}

    	flash('Something is wrong')->error();

    	return redirect()->back()
    					->withErrors($validate)
    					->withInput();
    }

     public function publish($id){
    	$tag = BlogTag::findOrFail($id);
    	$tag->published = 1;
    	$tag->save();
    	flash('You have successfully published the tag')->success();
    	return redirect()->back();
    }


    public function draft($id){
    	$tag = BlogTag::findOrFail($id);
    	$tag->published = 0;
    	$tag->save();
    	flash('You have successfully drafted the tag')->success();
    	return redirect()->back();
    }

    public function delete($id){
    	$tag = BlogTag::findOrFail($id);
    	$tag->delete();
    	flash('You have successfully deleted the tag')->success();
    	return redirect()->back();

    }
}
