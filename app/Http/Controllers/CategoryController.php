<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Repositories\CategoryRepositoryInterface;

use Validator;

use Image;

class CategoryController extends Controller
{
    protected $category;

    public function __construct(CategoryRepositoryInterface $category){

        $this->middleware('auth');
        $this->middleware('admin');

    	$this->category = $category;

    }

    public function view(){

    	$title = 'View Categories';

    	$categories = $this->category->setCategory()
    								->paginate(10);

    	return view('dashboard.category-menu.view', compact('categories', 'title'));
    }

    public function create(){

    	$title = 'Create Category';

    	return view('dashboard.category-menu.create', compact('title'));
    }

    public function register(Request $request){

    	$validate = Validator::make($request->all(), [
                    'category' => 'required|unique:categories,name',
                    'thumbnail' => 'required|image|mimes:jpeg,jpg,png,gif|image_dimension:min_width=900,min_height=337']);

    	if($validate->passes()){
    			$category = $this->category->setCategory()->create([
    					'name' => $request->input('category')
    				]);
    			$category->class = 'menu-'.$category->id;

                if($request->hasFile('thumbnail')){

                    $image = $request->file('thumbnail');

                    $category_menu_path = md5(time())."-".str_slug($request->category).".".$image->getClientOriginalExtension();

                    Image::make($image->getRealPath())->resize(900,337)->save(public_path() . '/images/categories-banners/'.$category_menu_path);

                    $category->banner = '/images/categories-banners/'.$category_menu_path.'?cache='.time();

                    $category->save();

                    flash('You have successfully created a Category Menu')->success();

                    return redirect()->route('view.category');
                }

    	}

    	flash('Something is wrong')->error();

    	return redirect()->back()
    					->withErrors($validate)
    					->withInput();
    	
    }

    public function edit($id){

    	$title = 'Edit Category';

    	$category = $this->category->find($id);

    	return view('dashboard.category-menu.edit', compact('category', 'title'));
    }

    public function update($id, Request $request){

    	$validate = Validator::make($request->all(), [
            'category' => 'required|unique:categories,name,'.$id,
            'thumbnail' => 'sometimes|image|mimes:jpeg,jpg,png,gif|image_dimension:min_width=900,min_height=337'
            ]);

    	if($validate->passes()){

    			$category = $this->category->find($id);

    			$category->name = $request->input('category');

                 if($request->hasFile('thumbnail')){

                    @unlink( public_path(explode('?', str_replace('/', '\\', ltrim($category->banner, '/')))[0] ));

                    $image = $request->file('thumbnail');

                    $category_menu_path = md5(time())."-".str_slug($request->category).".".$image->getClientOriginalExtension();

                    Image::make($image->getRealPath())->resize(900,337)->save(public_path() . '/images/categories-banners/'.$category_menu_path);

                    $category->banner = '/images/categories-banners/'.$category_menu_path.'?cache='.time();
                }

    			$category->save();


    			flash('You have successfully updated category menu')->success();

    			return redirect()->route('view.category');
    	}

    	flash('Something is wrong')->error();

    	return redirect()->back()
    					->withErrors($validate)
    					->withInput();
    }

     public function publish($id){
    	$category = $this->category->find($id);
    	$category->published = 1;
    	$category->save();
    	flash('You have successfully published the category')->success();
    	return redirect()->back();
    }


    public function draft($id){
    	$category = $this->category->find($id);
    	$category->published = 0;
    	$category->save();
    	flash('You have successfully drafted the category')->success();
    	return redirect()->back();
    }

    public function delete($id){
    	$category = $this->category->find($id);
    	$category->delete();
    	flash('You have successfully deleted the category')->success();
    	return redirect()->back();

    }
}
