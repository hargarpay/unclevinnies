<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Repositories\KitchenChefRepositoryInterface;

use App\Repositories\KitchenPositionRepositoryInterface;

use App\Repositories\KitchenRankRepositoryInterface;

use Validator;

use Image;

class KitchenChefController extends Controller
{
    protected $kitchen_chef;

    protected $kitchen_position;

    protected $kitchen_rank;

    public function __construct(KitchenChefRepositoryInterface $kitchen_chef, KitchenPositionRepositoryInterface $kitchen_position, KitchenRankRepositoryInterface $kitchen_rank){

        $this->middleware('auth');
        $this->middleware('admin');

    	$this->kitchen_chef = $kitchen_chef;

    	$this->kitchen_position = $kitchen_position;

    	$this->kitchen_rank = $kitchen_rank;


    }

    public function view(){
    	$title = 'View Kitchen Chef';

    	$kitchen_chefs = $this->kitchen_chef->setKitchenChef()
    						->paginate(30);

    	return view('dashboard.kitchen-chef.view', compact('title', 'kitchen_chefs'));
    }

    public function create(){
    	$title = 'Create Kitchen Chef';

    	$kitchen_positions = $this->kitchen_position->getAll();

    	$kitchen_ranks = $this->kitchen_rank->getAll();

    	return view('dashboard.kitchen-chef.create', compact('title', 'kitchen_positions', 'kitchen_ranks'));

    }

    public function register(Request $request){

    	$setKitchenChef = $this->kitchen_chef->setKitchenChef();

    	$validate = Validator::make($request->all(), $setKitchenChef::$rules);

    	if($validate->passes()){

    		$kitchen_chef = $setKitchenChef->create([
    				'name' => $request->input('name'),
    				'kitchen_position_id' => $request->input('kitchen_position'),
    				'kitchen_rank_id' => $request->input('kitchen_rank'),
    				'excerpt' => $request->input('excerpt')
    		]);

    		if($request->hasFile('thumbnail')){

                $image = $request->file('thumbnail');
                $kitchen_chef_path = md5(time())."-".$request->name.".".$image->getClientOriginalExtension();

                Image::make($image->getRealPath())->resize(650,563)->save(public_path() . '/images/kitchen_chefs/'.$kitchen_chef_path);

                $kitchen_chef->thumbnail = '/images/kitchen_chefs/'.$kitchen_chef_path.'?cache='.time();

                $kitchen_chef->save();

                flash('You have successfully created a kitchen chef')->success();

                return redirect()->back();
    		}


    	}

    	flash('Something is wrong')->error();

    	return redirect()->back()
    					->withErrors($validate)
    					->withInput();
    }

    public function edit($id){

    	$title = 'Edit Kitchen Chef';

    	$kitchen_positions = $this->kitchen_position->getAll();

    	$kitchen_ranks = $this->kitchen_rank->getAll();

    	$kitchen_chef = $this->kitchen_chef->find($id);

    	return view('dashboard.kitchen-chef.edit', compact('title', 'kitchen_chef', 'kitchen_positions', 'kitchen_ranks'));
    }

    public function update($id, Request $request){

    	$setKitchenChef = $this->kitchen_chef->setKitchenChef();

		$setKitchenChef::$rules['thumbnail'] = 'sometimes|image|mimes:jpeg,jpg,png,gif|image_dimension:min_width=650,min_height=563';

    	$validate = Validator::make($request->all(), $setKitchenChef::$rules);

    	if($validate->passes()){

    		$kitchen_chef = $this->kitchen_chef->find($id);

    		$kitchen_chef->update([
    				'name' => $request->input('name'),
    				'kitchen_position_id' => $request->input('kitchen_position'),
    				'kitchen_rank_id' => $request->input('kitchen_rank'),
    				'excerpt' => $request->input('excerpt')
    		]);

    		if($request->hasFile('thumbnail')){
    			@unlink( public_path(explode('?', str_replace('/', '\\', ltrim($kitchen_chef->thumbnail, '/')))[0] ));

                $image = $request->file('thumbnail');
                $kitchen_chef_path = md5(time())."-".$request->name.".".$image->getClientOriginalExtension();

                Image::make($image->getRealPath())->resize(650,563)->save(public_path() . '/images/kitchen_chefs/'.$kitchen_chef_path);

                $kitchen_chef->thumbnail = '/images/kitchen_chefs/'.$kitchen_chef_path.'?cache='.time();

                $kitchen_chef->save();

    		}

            flash('You have successfully updated a kitchen chef')->success();

            return redirect()->back();

    	}
    }

    public function delete($id){
    	$kitchen_chef = $this->kitchen_chef->find($id);
    	$kitchen_chef->delete();
    	flash('You have successfully deleted the kitchen chef')->success();
    	return redirect()->back();

    }
}
