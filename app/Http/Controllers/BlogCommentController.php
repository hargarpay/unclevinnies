<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Validator;

use App\BlogComment;

class BlogCommentController extends Controller
{
    public function __construct(){
    		$this->middleware('auth');
    }

    public function comment($id, Request $request){

    	$validate = Validator::make($request->all(), [
    			'comment' => 'required|min:2'
    		]);

    	if($validate->passes()){
    		BlogComment::create([
    				'comment' => $request->comment,
    				'parent_id' => $request->parent_id,
    				'blog_id' => $request->blog_id,
    				'user_id' => auth()->id()
    			]);

    		flash('Successfully Added Comment to the post')->success();

    		return redirect()->back();
    	}

    	flash('Something is wrong')->error();

    	return redirect()->back()
    					->withErrors($validate)
    					->withInput();


    }
}
