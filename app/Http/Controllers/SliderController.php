<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Repositories\SliderRepositoryInterface;

use Validator;

use Image;

class SliderController extends Controller
{
    protected $slider;

    public function __construct(SliderRepositoryInterface $slider){

        $this->middleware('auth');
        $this->middleware('admin');
    	$this->slider = $slider;
    }

    public function view(){

    	$title = "View Sliders";

    	$sliders = $this->slider->setSlider()
    								->paginate(20);

    	return view('sliders.view', compact('title', 'sliders'));

    }


    public function create(){
    	$title = 'Create Slider';
    	$positions = array('left' => 'Left', 'right' => 'Right', 'center' => 'Center');
    	$actions = array(
    			'order-form' => 'Order Now',
    			'cheffs' => 'Meet our Cheffs',
    			'facts' => 'See Facts',
    			'gallery' => 'See Our Gallery',
    			'services' => 'Learn More on our Services',
    			'specialities' => 'Learn More on our Specialities'
    		);
    	return view('sliders.create', compact('positions', 'actions', 'title'));
    }

    public function register(Request $request){
    	$setSlider = $this->slider->setSlider();

    	$validate = Validator::make($request->all(), $setSlider::$rules);

    	if($validate->passes()){

    		//dd($request->all());

    		$slider = $setSlider->create([
    				'small_title' => strtoupper($request->input('small_title')),
    				'major_title' => strtoupper($request->input('major_title')),
    				'description' => $request->input('description'),
    				'tab_title' => strtoupper($request->input('tab_title')),
    				'tab_description' => $request->input('tab_description'),
    				'actions' => json_encode($request->input('actions')),
    				'position' => $request->input('position')
    		]);

    		if(!empty($request->input('color_title')))
    		{
    			$slider->color_title = strtoupper($request->input('color_title'));
    		}
    		if($request->hasFile('banner')){

                $image = $request->file('banner');
                $banner_path = md5(time())."-".$request->small_title.".".$image->getClientOriginalExtension();

                Image::make($image->getRealPath())->resize(1920,1098)->save(public_path() . '/images/banners/'.$banner_path);

                $slider->banner = '/images/banners/'.$banner_path.'?cache='.time();

                $slider->save();

                flash('You have successfully created a slider')->success();

                return redirect()->back();
    		}
    	}

    	flash('Something is wrong')->error();

    	return redirect()->back()
    					->withErrors($validate)
    					->withInput();

    }


    public function edit($id){

    	$title = 'Edit Slider';

    	$slider = $this->slider->find($id);

        //dd($slider->actions);

        if('null' == $slider->actions){
            $slider_actions = array();
        }else{
    	   $slider_actions = json_decode($slider->actions, true);
        }


    	$positions = array('left' => 'Left', 'right' => 'Right', 'center' => 'Center');
    	$actions = array(
    			'order-form' => 'Order Now',
    			'cheffs' => 'Meet our Cheffs',
    			'facts' => 'See Facts',
    			'gallery' => 'See Our Gallery',
    			'services' => 'Learn More on our Services',
    			'specialities' => 'Learn More on our Specialities'
    		);

    	return view('sliders.edit', compact('slider', 'slider_actions', 'title', 'positions', 'actions'));

    }

    public function update($id, Request $request){

    	$setSlider = $this->slider->setSlider();

    	$setSlider::$rules['banner'] = 'sometimes|image|mimes:jpeg,jpg,png,gif|image_dimension:min_width=1920,min_height=1098';

    	$validate = Validator::make($request->all(), $setSlider::$rules);

    	if($validate->passes()){

    		$slider = $this->slider->find($id);

    		$slider->small_title = $request->input('small_title');
    		$slider->major_title = $request->input('major_title');
    		if(!empty($request->input('color_title'))){

    			$slider->color_title = $request->input('color_title');
    		}
    		$slider->description = $request->input('description');
    		$slider->actions = json_encode($request->input('actions'));
    		$slider->tab_title = $request->input('tab_title');
    		$slider->tab_description = $request->input('tab_description');
            $slider->position = $request->input('position');

    		if ($request->hasFile('banner'))
            {
                @unlink( public_path(explode('?', str_replace('/', '\\', ltrim($slider->banner, '/')))[0] ));

                $image = $request->file('banner');

                $banner_path = md5(time())."-".$request->small_title.".".$image->getClientOriginalExtension();

                Image::make($image->getRealPath())->resize(1920,1098)->save(public_path() . '/images/banners/'.$banner_path);

                $slider->banner = '/images/banners/'.$banner_path.'?cache='.time();


            }

            $slider->save();

            flash('You have successfully updated the slider')->success();

            return redirect()->back();

    	}

    	flash('Something is wrong')->error();

    	return redirect()->back()
    					->withErrors($validate)
    					->withInput();
    }

    public function publish($id){
    	$slider = $this->slider->find($id);
    	$slider->published = 1;
    	$slider->save();
    	flash('You have successfully published the slider')->success();
    	return redirect()->back();
    }


    public function draft($id){
    	$slider = $this->slider->find($id);
    	$slider->published = 0;
    	$slider->save();
    	flash('You have successfully drafted the slider')->success();
    	return redirect()->back();
    }

    public function delete($id){
    	$slider = $this->slider->find($id);
    	$slider->delete();
    	flash('You have successfully deleted the slider')->success();
    	return redirect()->back();

    }


}
