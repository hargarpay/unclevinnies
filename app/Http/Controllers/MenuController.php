<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Repositories\MenuRepositoryInterface;

use App\Repositories\CategoryRepositoryInterface;

use Validator;

use Image;

use Cart;

class MenuController extends Controller
{
    protected $menu;

    protected $category;

    public function __construct(MenuRepositoryInterface $menu, CategoryRepositoryInterface $category){

        $this->middleware('auth', ['except' => ['addToCart','removeMenuFromCart', 'updateMenuQtyCart']]);
        $this->middleware('admin', ['except' => ['addToCart','removeMenuFromCart', 'updateMenuQtyCart']]);

    	$this->menu = $menu;

    	$this->category = $category;

    }

    public function addToCart(Request $request){

        if($request->ajax()){
            $menu_id = $request->input('menuID');
            $menu_qty = $request->input('menuQty');

             // echo $menu_id.'   '.$menu_qty;

            $menu = $this->menu->find($menu_id);


            Cart::insert(array(
                    'id' => $menu->id,
                    'name' => $menu->title,
                    'quantity' => $menu_qty,
                    'image' => url($menu->thumbnail),
                    'price' => $menu->amount
                ));

            return response()->json([
                'cart' => Cart::totalItems(true)
                ]);
        }


    }

    public function removeMenuFromCart(Request $request){

          if($request->ajax()){

            $menu_id = $request->input('menuID');

            foreach (Cart::contents() as $cart_menu) {

                if($cart_menu->id == $menu_id){
                    $cart_menu->remove();
                    break;
                }
            }

            return response()->json([
                'cart' => Cart::total(false)
                ]);
         }
    }

    public function updateMenuQtyCart(Request $request){

        if($request->ajax()){

            $menu_id = $request->input('menuID');

            $menu_qty = $request->input('menuQty');

            foreach (Cart::contents() as $cart_menu) {

                if($cart_menu->id == $menu_id){
                    $cart_menu->quantity = (int) $menu_qty;
                    break;
                }
            }

            return response()->json([
                'cart' => Cart::total(false),
                'menu_qty' => $menu_qty
                ]);
         }
    }

    public function getCart(){
        dd(\Cart::contents());

    }

    public function view(){
    	$title = 'View Menu';

    	$menus = $this->menu->setMenu()
    						->paginate(30);

    	return view('dashboard.menu.view', compact('title', 'menus'));
    }

    public function create(){
    	$title = 'Create menu';

    	$categories = $this->category->getAll();

    	return view('dashboard.menu.create', compact('title', 'categories'));

    }

    public function register(Request $request){
    	$setMenu = $this->menu->setMenu();

    	$validate = Validator::make($request->all(), $setMenu::$rules);

    	if($validate->passes()){

    		$menu = $setMenu->create([
    				'title' => $request->input('title'),
    				'description' => $request->input('description'),
    				'category_id' => $request->input('category'),
    				'amount' => $request->input('amount')
    		]);

    		if($request->hasFile('thumbnail')){

                $image = $request->file('thumbnail');
                $menu_path = md5(time())."-".$request->title.".".$image->getClientOriginalExtension();

                Image::make($image->getRealPath())->resize(650,566)->save(public_path() . '/images/menus/'.$menu_path);

                $menu->thumbnail = '/images/menus/'.$menu_path.'?cache='.time();

                $menu->save();

                flash('You have successfully created a menu')->success();

                return redirect()->back();
    		}
    	}

    	flash('Something is wrong')->error();

    	return redirect()->back()
    					->withErrors($validate)
    					->withInput();
    }

    public function edit($id){

    	$title = 'Edit menu';

    	$categories = $this->category->getAll();

    	$menu = $this->menu->find($id);

    	return view('dashboard.menu.edit', compact('title', 'categories', 'menu'));
    }

    public function update($id, Request $request){

    	$setMenu = $this->menu->setMenu();

		$setMenu::$rules['thumbnail'] = 'sometimes|image|mimes:jpeg,jpg,png,gif|image_dimension:min_width=650,min_height=566'; 	

    	$validate = Validator::make($request->all(), $setMenu::$rules);

    	if($validate->passes()){

    		$menu = $this->menu->find($id);

    		$menu->update([
    				'title' => $request->input('title'),
    				'description' => $request->input('description'),
    				'category_id' => $request->input('category'),
    				'amount' => $request->input('amount')
    		]);

    		if($request->hasFile('thumbnail')){

    			@unlink( public_path(explode('?', str_replace('/', '\\', ltrim($menu->thumbnail, '/')))[0] ));

                $image = $request->file('thumbnail');
                $menu_path = md5(time())."-".$request->title.".".$image->getClientOriginalExtension();

                Image::make($image->getRealPath())->resize(650,566)->save(public_path() . '/images/menus/'.$menu_path);

                $menu->thumbnail = '/images/menus/'.$menu_path.'?cache='.time();

                $menu->save();

    		}

            flash('You have successfully created a menu')->success();

            return redirect()->back();

    	}
    }

    public function publish($id){
    	$menu = $this->menu->find($id);
    	$menu->published = 1;
    	$menu->save();
    	flash('You have successfully published the menu')->success();
    	return redirect()->back();
    }


    public function draft($id){
    	$menu = $this->menu->find($id);
    	$menu->published = 0;
    	$menu->save();
    	flash('You have successfully drafted the menu')->success();
    	return redirect()->back();
    }

    public function feature($id){
    	$menu = $this->menu->find($id);
    	$menu->featured = 1;
    	$menu->save();
    	flash('You have successfully featured the menu')->success();
    	return redirect()->back();
    }

    public function unfeature($id){
    	$menu = $this->menu->find($id);
    	$menu->featured = 0;
    	$menu->save();
    	flash('You have successfully unfeatured the menu')->success();
    	return redirect()->back();
    }

    public function delete($id){
    	$menu = $this->menu->find($id);
    	$menu->delete();
    	flash('You have successfully deleted the menu')->success();
    	return redirect()->back();

    }
}
