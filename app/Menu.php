<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = "menus";

    protected $fillable = ['title', 'description', 'category_id', 'amount'];

    public static $rules = [
    						'title' => 'required',
    						'description' => 'required|min:15',
    						'amount' => 'required|integer',
    						'category' => 'required|integer',
    						'thumbnail' => 'required|image|mimes:jpeg,jpg,png,gif|image_dimension:min_width=650,min_height=566'
    						];

    public function category(){
    	return $this->belongsTo('App\Category', 'category_id');
    }
}
