<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KitchenRank extends Model
{
    protected $table = 'kitchen_ranks';

    protected $fillable = ['name'];
}
