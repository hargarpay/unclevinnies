<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "categories";

    protected $fillable = ['name', 'thumbnail'];

    public function menus(){
    	return $this->hasMany('App\Menu', 'category_id');
    }

    public function publishMenu(){
    	return  $this->menus()->where('published', 1)->get();
    }
}
