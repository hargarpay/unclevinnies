<?php

namespace App\Listeners;

use App\Events\Register;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mailers\RegisterMailer as Mailer;

class NotifyRegister
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    protected $Mailer;

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  Register  $event
     * @return void
     */
    public function handle(Register $event)
    {
        $this->mailer->sendRegister($event->user, $event->promo_menu, $event->latest_menu);
    }
}
