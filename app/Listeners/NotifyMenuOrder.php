<?php

namespace App\Listeners;

use App\Events\MenuOrder;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mailers\MenuOrderMailer as Mailer;


class NotifyMenuOrder
{
    /**
     * Create the event listener.
     *
     * @return void
     */

    protected $mailer;
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  MenuOrder  $event
     * @return void
     */
    public function handle(MenuOrder $event)
    {
        $this->mailer->sendMenuOrder($event->user,$event->items, $event->grand_total, $event->latest_menu);
    }
}
