<?php namespace App\Repositories;

interface SliderRepositoryInterface{

	public function setSlider();

	public function getAll();

	public function find($id);

} 