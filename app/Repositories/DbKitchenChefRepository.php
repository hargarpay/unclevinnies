<?php namespace App\Repositories;

use App\KitchenChef;

class DbKitchenChefRepository implements KitchenChefRepositoryInterface{

	public function setKitchenChef(){
		return new KitchenChef;
	}

	public function getAll(){

		return KitchenChef::all();

	}

	public function find($id){

		return KitchenChef::findOrFail($id);

	}
}