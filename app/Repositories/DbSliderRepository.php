<?php namespace App\Repositories;

use App\Slider;

class DbSliderRepository implements SliderRepositoryInterface{

	public function setSlider(){
		return new Slider;
	}

	public function getAll(){

		return Slider::all();

	}

	public function find($id){

		return Slider::findOrFail($id);

	}
}