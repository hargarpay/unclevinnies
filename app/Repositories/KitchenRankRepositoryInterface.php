<?php namespace App\Repositories;

interface KitchenRankRepositoryInterface{

	public function setKitchenRank();

	public function getAll();

	public function find($id);

} 