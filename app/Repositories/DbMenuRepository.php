<?php namespace App\Repositories;

use App\Menu;

class DbMenuRepository implements MenuRepositoryInterface{

	public function setMenu(){
		return new Menu;
	}

	public function getAll(){

		return Menu::all();

	}

	public function find($id){

		return Menu::findOrFail($id);

	}
}