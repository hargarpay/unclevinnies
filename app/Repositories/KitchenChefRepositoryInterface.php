<?php namespace App\Repositories;

interface KitchenChefRepositoryInterface{

	public function setKitchenChef();

	public function getAll();

	public function find($id);

} 