<?php namespace App\Repositories;

interface CategoryRepositoryInterface{

	public function setCategory();

	public function getAll();

	public function find($id);

} 