<?php namespace App\Repositories;

use App\KitchenPosition;

class DbKitchenPositionRepository implements KitchenPositionRepositoryInterface{

	public function setKitchenPosition(){
		return new KitchenPosition;
	}

	public function getAll(){

		return KitchenPosition::all();

	}

	public function find($id){

		return KitchenPosition::findOrFail($id);

	}
}