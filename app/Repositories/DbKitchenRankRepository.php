<?php namespace App\Repositories;

use App\KitchenRank;

class DbKitchenRankRepository implements KitchenRankRepositoryInterface{

	public function setKitchenRank(){
		return new KitchenRank;
	}

	public function getAll(){

		return KitchenRank::all();

	}

	public function find($id){

		return KitchenRank::findOrFail($id);

	}
}