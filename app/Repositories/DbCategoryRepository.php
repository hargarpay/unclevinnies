<?php namespace App\Repositories;

use App\Category;

class DbCategoryRepository implements CategoryRepositoryInterface{

	public function setCategory(){
		return new Category;
	}

	public function getAll(){

		return Category::all();

	}

	public function find($id){

		return Category::findOrFail($id);

	}
}