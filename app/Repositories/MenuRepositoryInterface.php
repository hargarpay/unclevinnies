<?php namespace App\Repositories;

interface MenuRepositoryInterface{

	public function setMenu();

	public function getAll();

	public function find($id);

} 