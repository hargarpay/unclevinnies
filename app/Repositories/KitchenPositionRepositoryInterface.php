<?php namespace App\Repositories;

interface KitchenPositionRepositoryInterface{

	public function setKitchenPosition();

	public function getAll();

	public function find($id);

} 