<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogComment extends Model
{
    protected $table = 'blog_comments';

    protected $fillable = ['comment', 'blog_id', 'user_id', 'parent_id'];

    public function blog(){
    	return $this->belongsTo('\App\Blog', 'blog_id');
    }

    public function user(){
    	return $this->belongsTo('App\User', 'user_id');
    }
}
