<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
   protected $table = 'sliders';

   protected $fillable = ['banner', 'small_title', 'major_title', 'color_title', 'description', 'tab_title', 'tab_description', 'actions', 'position'];

   public static $rules = [
   		'small_title' => 'required',
   		'major_title' => 'required',
   		'color_title' => 'sometimes',
   		'banner' => 'required|image|mimes:jpeg,jpg,png,gif|image_dimension:min_width=1920,min_height=1098',
   		'description' => 'required|min:20',
   		'position' => ['required', 'regex:/(left|right|center)/'],
   		'actions' => 'sometimes|array',
   		'tab_title' => 'required',
   		'tab_description' => 'required'
   ];
}
